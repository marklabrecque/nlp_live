<?php
$browser_title = "Pricing";
ob_start(); ?>
	<h2>Promotional Offer</h2>
	<p><a href="http://nicolelynn2010.wordpress.com/promotions/" title="Promotions &laquo; Nicole-Lynn Photography" target="_blank">Click here</a> to view our current list of promotions and packages.</p>
	<!-- 
	<p>Book a family photo session by November 30, 2010 and receive 25 printed holiday cards free</p> -->
<?php 
$sidebar = ob_get_contents();
ob_end_clean();

ob_start();
?>
	<h1><?php echo $browser_title ?></h1>
	
	
	<h2>Session Fees:</h2>

		<p>The session fee covers the photographer&rsquo;s time spent with you. All session fees are based on five people or less, however bigger groups are welcomed. Generally sessions take 1-2 hours depending on the session type.</p>

		<h3>Basic Session Fees:</h3>
		<table border="0" cellspacing="0" cellpadding="0" class="price-table">
			<tr class="odd"><td>Session Fee (within Calgary, AB)</td>	<td class="price">$ 50.00</td></tr>
			<tr><td>Session Fee (outside Calgary, AB)</td>				<td class="price">$ 75.00</td></tr>
			<tr class="odd"><td>Extra Person Fee</td					><td class="price">$ 5.00</td></tr>
		</table>

		<h3>Special Occasion &amp; Modelling Session Fees:</h3>
		<table border="0" cellspacing="0" cellpadding="0" class="price-table">
			<tr class="odd"><td>4 hours	(one location)</td>							<td class="price">$ 300.00</td></tr>
			<tr><td>8 hours (up to three locations)</td>							<td class="price">$ 650.00</td></tr>
			<tr class="odd"><td>Unlimited (unlimited locations within one day)</td>	<td class="price">$ 800.00</td></tr>
		</table>
	

		<h2>Proofing</h2>

		<p>Proofing is available for all sessions. This includes a preliminary book of prints of images from your session that can be used to assist you with ordering purposes.  </p>
		<table border="0" cellspacing="0" cellpadding="0" class="price-table">
			<tr class="odd"><td>4x6 Proof Set</td>							<td class="price">$ 50.00</td></tr>
			<tr><td>Additional 4x6 Proof Set</td>							<td class="price">$ 40.00</td></tr>
			<tr class="odd"><td>8x10 Proof Book (4 photos to a page)</td>	<td class="price">$ 55.00</td></tr>
		</table>
	

		<h2>Prints &amp; Products</h2>

		<h3>Digital Discs:</h3>
		<p>All digital discs consist of the edited color images as well as copies in either black &amp; white or sepia tones.</p>

		<table border="0" cellspacing="0" cellpadding="0" class="price-table">
			<tr class="odd"><td>Digital Disc of images from a Basic Session (up to 50 color images)</td><td class="price">	$ 150.00</td></tr>
			<tr><td>Additional Images (for every additional 50)</td>									<td class="price">$ 100.00</td></tr>
		</table>

		<h3>Prints:</h3>
		<p>All prints are done using a chemical process to create real photographs not inkjet prints. Prints come in a range of sizes that range in price from $2.50 to $150.00. Common sizes are listed below.</p>

		<table border="0" cellspacing="0" cellpadding="0" class="price-table">
			<tr class="odd"><td>3.5x5 Print</td><td class="price">$   2.50</td></tr>
			<tr><td>4x6 Print  </td><td class="price">$   3.00</td></tr>
			<tr class="odd"><td>5x7 Print  </td><td class="price">$   5.00</td></tr>
			<tr><td>8x10 Print </td><td class="price">$   7.50</td></tr>
			<tr class="odd"><td>11x14 Print</td><td class="price">$ 12.50</td></tr>
			<tr><td>16x20 Print</td><td class="price">$ 30.00</td></tr>
		</table>
		<h3>Cards:</h3>
		<p>All cards are custom designed using photos from your session. They come in traditional layouts, tri-folds and 50 different shapes. Cards come in packages of 25 and include envelopes. Card pricing ranges from $25.00 to $55.00.</p>

		<h3>Photobooks &amp; Portfolios:</h3>
		<p>All photobooks are custom designed using photos from your session. Photobooks come in three different sizes; 8.5x11, 11x14 and 12x12. Photobooks start at $275.00. Additional copies of books are available at a discount.</p>

		<p>All portfolios are a set of professionally bound 8x10 photos. Prices start at $250.00</p>

		<h3>Framing and Finishing:</h3>
		<p>Custom framing is available for any print size. Frames come in a wide range of colors, shapes, and sizing options. Prices start at $40.00</p>

		<p>Mounting options are also available which finish the print with a solid edge that allows it to be hung without the need for a frame. Prices start at $35.00</p>

		<p>Other finishing options are available. Please inquire for more information.</p>

		<h3>Other Custom Products:</h3>
		<p>In order to fully enhance your post-production experience we offer many other custom products. These products include Image Folios, Custom Boxes for prints, Wall Panels and Collages and Stickers.</p>


		<h2>Weddings</h2>

		<p>Since every wedding is unique packages are created on an individual basis based on the client&rsquo;s needs. Packages start at $1000.00</p>
		
		<h2>Gift Certificates</h2>

		<p>Gift Certificates are available in any denomination. Please contact us for more details.</p>


		<p>Updated February 2010</p>
		<p>Prices Subject to Change</p>
		<p>GST not included</p>
		<p>Nicole-Lynn Photography retains copyright and reserves the right to use images in any marketing, displays or advertisements.</p>

	
	
	<?php /* ?>
	
	<p>All sessions have a session fee in order to cover the photographer’s time spent creating life lasting memories with you. All session fees are based on 5 people or less, however bigger groups are welcomed. Please <a href="/contact/">e-mail</a> for a pricing quote.</p>
	<p>Basic Session fee $50 (within Calgary, AB);<br /> $75 (outside of Calgary, AB)<br />
	<span>This covers up to a 1 hour session at one location</span></p>

	<p>Due to the wide variety of session types and individual needs, all packages are customized to fit each individual client. Below are some examples of package types. Please <a href="/contact/">e-mail</a> us with any questions, or for a session quote.</p>

	<div class="package">
		<h3>Maternity &amp; Newborn</h3>
		<span class="price">($400.00)</span>
		<img src="/img/services-maternity-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>2 sessions <br />(up to a maximum of 5 people)</li>
			<li>Photo Editing</li>
			<li>Brag Book including a 4x6 proof set</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>Framed 11x14 Customized Wall </li>
			<li>Panel 3 Occasion Cards, included on Photo Disc</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Family - Basic</h3>
		<span class="price">($275.00)</span>
		<img src="/img/services-family-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 session <br />(up to a maximum of 5 people)</li>
			<li>Photo Editing</li>
			<li>Brag Book including a 4x6 proof set</li>
			<li>Photo Disc of all edited photos, including color changes</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Graduation</h3>
		<span class="price">($850.00)</span>
		<img src="/img/services-graduation-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 four hour Special Occasion Session <br />(up to 4 graduates, dates, and family)</li>
			<li>Photo Editing</li>
			<li>4 Brag Books including 4x6 proof set</li>
			<li>4 Photo Discs of all edited photos, 
			including color changes</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Model/Headshots</h3>
		<span class="price">($425.00)</span>
		<img src="/img/services-model-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>8x10 Portfolio</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Wedding - Basic</h3>
		<span class="price">($1000.00)</span>
		<img src="/img/services-wedding-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 eight hour <br />(full day) Special Occasion Session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>8x10 Portfolio</li>
			<li>Framed 11x14 Customized Wall Panel</li>
			<li>2 Occasion Cards, included on Photo Disc</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Special Event</h3>
		<span class="price">($650.00)</span>
		<img src="/img/services-special-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 four hour Special Occasion Session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>Framed 11x14 Customized Wall Panel</li>
		</ul>
	</div><!-- /package -->
	
<?php */
$content = ob_get_contents();
ob_end_clean();


include_once('../templates/two-column.php');
?>
