<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<title>Nicole-Lynn Photography</title>
	<link href="/css/landing.css" rel="stylesheet" type="text/css" media="all" />
	<!--[if lt IE 7]>
	<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/css/landing.css" type="text/css" media="all">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<link rel="stylesheet" href="/css/normalize.css" type="text/css">
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
	<style type="text/css">
		* { box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box; }
		body {  }
		.inner { width:845px; margin:0 auto; }
		.header { float:left; width:100%; text-align:center; border-bottom:1px solid #ccc; background:#fff; }
		.header .wrapper { margin:0 auto; width: 400px;}
		.header .wrapper .logo { float:left; }
		.header .wrapper .heading h2 { float:left; font-size:15px; text-transform:uppercase; font-family:Arial, sans-serif; color:#713d25; margin:67px 0 0 10px; }
		.content { float:left; width:100%; padding:50px 0; background:#c0e1d6; }
		.content .column { float:left; margin-right:25px; }
		.content .column p { width:100%; text-align:center; font-family:Arial, sans-serif; color:#713d25; }
		.content .column a { position:relative; display:block; width:100%; height:100%; color:#713d25; }
		.content .column a .mask { position:absolute; display:none; width:100%; height:400px; top:0; left:0; background:rgba(0,0,0,0.4); color:#fff; font-family:'Dancing Script', cursive; font-size:18px; text-align:center; padding-top:181px; }
		.content .column a:hover .mask { display:block; }
		.content .column.last { margin-right:0; }
		.footer { float:left; width:100%; color:#fff; text-align:center; padding-top:50px; }
		.footer a { font-family:'Dancing Script', cursive; text-decoration:none; color:#713d25; margin:0 15px 0 0; padding:0 15px 0 0; border-right:1px solid #713d25; }
		.footer a.last { border-right:none; }
	</style>
</head>
<body>
	<div class="header">
		<div class="inner">
			<div class="wrapper">
				<img class="logo" src="img/landing-logo-2.png" alt="Nicole Lynn Photography" />
				<div class="heading">
					<h2>Nicole-Lynn Photography</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<div class="column">
				<a href="/home/">
					<img src="img/landing-nicole-lynn-resized.jpg" />
					<span class="mask"></span>
				</a>
				<p>Portraits</p>
			</div>
			<div class="column">
				<a href="/weddings/">
					<img src="img/landing-weddings-resized.jpg" />
					<span class="mask"></span>
				</a>
				<p>Weddings</p>
			</div>
			<div class="column last">
				<a href="http://blushingpinkphotography.com">
					<img src="img/landing-blushing-pink-resized.jpg" />
					<span class="mask"></span>
				</a>
				<p>Blushing Pink</p>
			</div>
		</div>
		<div class="footer">
			<div class="inner">
				<a href="/contact" title="Contact Us">Contact Us</a>
				<a href="http://nicolelynn2010.wordpress.com/" title="Blog">Blog</a>
				<a href="http://nicolelynn.com/photocart" title="Client Access" class="last">Client Access</a>
			</div>
		</div>
	</div>
</body>
</html>
