
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<title>Welcome -  Nicole-Lynn Photography</title>
 		<link href="/css/screen.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lt IE 7]>
		<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
		<![endif]-->
		<link rel="stylesheet" href="/css/home.css" type="text/css" media="all">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<!-- Google Analytics script -->
		<script type="text/javascript" src="/js/ga.js"></script>
	</head>
	<body>
		<div id="container">
			<div id="header">
				<div id="logo">
					<a href="/"><img src="/img/logo.jpg" alt="Home" /></a>
				</div>
				<div id="menu">
					<ul>
						<li id="m-home" class="current first"><a href="/home/" title="Home">Home</a></li>
						<li id="m-about"><a href="/about/" title="About">About</a></li>
						<li id="m-gallery"><a href="/gallery/" title="Gallery">Gallery</a></li>
						<li id="m-pricing"><a href="/info/" title="Info">Info</a></li>
						<li id="m-contact"><a href="/contact/" title="Contact">Contact</a></li>
						<li id="m-blog"><a href="http://nicolelynn2010.wordpress.com/  " title="Blog">Blog</a></li>
						<li id="m-client-access" class="last"><a href="/photocart/" title="Client Access">Client Access</a></li>
					</ul>
				</div><!-- /menu -->
			</div><!-- /header -->
			<div id="content">
				<div id="bodytext-container" class="no-sidebar">
					<div id="bodytext">
					<!-- include Cycle plugin -->
					<script src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
					<script>!window.cycle && document.write('<script src="/js/jquery.cycle/jquery.cycle.min.js"><\/script>')</script>
					<script type="text/javascript" charset="utf-8">
						(function($){
							$(function(){
								$('#heros').cycle({
								    speed:   600,
								    timeout: 3000,
								    next:   '#heros',
								    pause:   1
								});
								$('#buckets li a').hover(
									function() {
										$(this).find('.inner-shadow').stop().fadeIn();
									},
									function(){
										$(this).find('.inner-shadow').stop().fadeOut('slow');
									}
								);
							});
						})(jQuery);
					</script>
					<div id="preamble">
					<!-- <p>Nicole-Lynn Photography is a Calgary based company with photographers that specialize in portrait, wedding and fashion photography. Our emphasis is on creating fresh and innovative portraits. Capturing life&rsquo;s moments one at a time.</p>-->
					<p>We are Calgary based portrait & wedding photographers. Our emphasis is on creating fresh and innovative portraits. Capturing life’s moments one at a time.</p>
					<a id="about-button" href="/about/">Find out more about Nicole-Lynn</a>
				</div><!-- /preamble -->
				<div id="hero-container">
					<ul id="heros">
						<li style=" background-image: url(/home/img/1.jpg)"></li>
						<li style=" background-image: url(/home/img/2.jpg)"></li>
						<li style=" background-image: url(/home/img/3.jpg)"></li>
						<li style=" background-image: url(/home/img/4.jpg)"></li>
						<li style=" background-image: url(/home/img/5.jpg)"></li>
						<li style=" background-image: url(/home/img/6.jpg)"></li>
						<li style=" background-image: url(/home/img/7.jpg)"></li>
						<li style=" background-image: url(/home/img/8.jpg)"></li>
					</ul>
					<div id="hero-box">
						<ul id="buckets">
							<li id="gallery"><a href="/gallery/"><div class="inner-shadow"></div><span>Gallery</span></a></li>
							<li id="pricing"><a href="/info/"><div class="inner-shadow"></div><span>Info</span></a></li>
							<li id="contact" class="last"><a href="/contact/"><div class="inner-shadow"></div><span>Contact</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /content -->
</div><!-- /container -->
<div id="footer">
	<div id="footer-content">
		<p id="copyright">Copyright &copy; 2013 <a href="/">Nicole-Lynn Photography</a></p>
					</div><!-- /footer-content -->
</div> <!-- /footer -->
</body>
</html>
