<?php
$browser_title = "Welcome";

$css = '<link rel="stylesheet" href="/css/home.css" type="text/css" media="all">';


ob_start();

?>


<!-- include Cycle plugin -->
<script src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script>!window.cycle && document.write('<script src="/js/jquery.cycle/jquery.cycle.min.js"><\/script>')</script>

<script type="text/javascript" charset="utf-8">

	(function($){
		$(function(){
			
			
			$('#heros').cycle({ 
			    speed:   600, 
			    timeout: 3000, 
			    next:   '#heros', 
			    pause:   1 
			});

			$('#buckets li a').hover(
				function() {
					$(this).find('.inner-shadow').stop().fadeIn();
				},
				function(){
					$(this).find('.inner-shadow').stop().fadeOut('slow');
				}
			);
		});
	})(jQuery);
</script>

		
	

	<div id="preamble">
		<p>
			Nicole-Lynn Photography is a Calgary based company with photographers that specialize in portrait, wedding and fashion photography. Our emphasis is on creating fresh and innovative portraits. Capturing life&rsquo;s moments one at a time. 
		</p>
		<a id="about-button" href="/about/">Find out more about Nicole-Lynn</a>
	</div><!-- /preamble -->
	
		<div id="hero-container">
			<ul id="heros">
				<li style=" background-image: url(img/home/rotating-01.jpg)" alt="Friends" /></li>
				<li style=" background-image: url(img/home/rotating-02.jpg)" alt="Babies" /></li>
				<li style=" background-image: url(img/home/rotating-03.jpg)" alt="Pregnancy" /></li>
				<li style=" background-image: url(img/home/rotating-04.jpg)" alt="Family" /></li>
				<li style=" background-image: url(img/home/rotating-05.jpg)" alt="Kid in bucket" /></li>
				<li style=" background-image: url(img/home/rotating-06.jpg)" alt="Wedding" /></li>
			</ul>
			<div id="hero-box">
				<ul id="buckets">
					<li id="gallery"><a href="/gallery/"><div class="inner-shadow"><img src="img/home/bucket-shadow.png" width="299" height="120" alt=""  /></div><span>Gallery</span></a></li>
					<li id="pricing"><a href="/pricing/"><div class="inner-shadow"><img src="img/home/bucket-shadow.png" width="299" height="120" alt=""  /></div><span>Prices</span></a></li>
					<li id="contact" class="last"><a href="/contact/"><div class="inner-shadow"><img src="img/home/bucket-shadow.png" width="299" height="120" alt="" /></div><span>Contact</span></a></li>
				</ul>
			</div>
		</div>
	
	
	

<?php
$content = ob_get_contents();
ob_end_clean();


include_once('templates/full-width.php');