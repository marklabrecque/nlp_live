<?php
require_once('config.inc.php');
require("class.smtp.php");
require("class.phpmailer.php");
require_once( 'EmailTemplate.class.php'); 
/**
* @package JPLIB
* @version $Id: Email.class.php,v 1.0 2004/08/05 16:38:00 joel Exp $
*/

/**
* Mailer Class
* @access public
* @package JPLIB
*/
class Mailer extends PHPMailer{
 	// Set default variables for all new objects
    var $Host     = '';
    var $Mailer   = "smtp";                         // Alternative to IsSMTP()
	var $dao;
	
	var $db_action = 'do';
	/**
	* template plage
	* @access private
	* @var string path
	*/
	var $template_page; 
	var $template_html; // String
	
	var $id; // Int
	var $html_body; // String
	var $text_body; // String
	
	var $member_id; // Int
	var $subscriber_id; // Int
	var $search_id; // Int
	var $search_sql; // String
	var $error; // String 
	var $created;// Date 
	var $modified; // Date 
	var $status;// String 
	var $track; // Boolean
	
	/**
	* Mailer constructor
	* @access public
	*/
	function Mailer(&$dao) {
		$this->dao = &$dao;
		$this->Host = Config::get('SMTP_HOST');
	}
	
	function set_error($new_value){ 
		$this->error = $new_value; 
	}
	
	function set_id($new_value){
		$this->load($new_value);
		$this->id = $new_value;
	}
	
	function set_track($new_value){
		$this->track = ($new_value == 1)?1:0;
	}
	
	function load($id=''){
		if(!empty($id)){
			$this->id = $id ;
		}
		$sql = "SELECT * 
				FROM ".Config::get('MESSAGE_TABLE')." 
				WHERE id='".$this->id."'";
		$result = $this->dao->query($sql);
		$row = $result->fetch();
		$this->html_body  = stripslashes($row['email_body']);
		$this->text_body  = stripslashes(strip_tags($row['email_body']));
		$this->Subject  = stripslashes(strip_tags($row['subject']));
		$this->created  = stripslashes(strip_tags($row['created']));
		$this->modified  = stripslashes(strip_tags($row['modified']));
		$this->status  = stripslashes(strip_tags($row['status']));
		// Get Template Information
		$this->get_template($row['template_page']);
		// Get From Information
		$this->get_from($row['member_id']);
		// Get Search Information
		$this->get_search($row['search_id']);
	}
	
	//  GET FROM FOLDER
	function get_template($page){
		$this->template_page  = $page;
		$et_obj = new EmailTemplate($page);
		$et_obj->fetch();
		
		$this->template_html  = $et_obj->contents;
	}
	
	function get_search($id){
		$sql = "SELECT * 
				FROM ".Config::get('SAVED_SEARCH_TABLE')." 
				WHERE id='".$id."'";
		$result = $this->dao->query($sql);
		$row = $result->fetch();
		$this->search_id = $id;
		if(empty($this->subscriber_id)){
			$this->search_sql = stripslashes($row['sql']);
		}
	}
	
	function get_search_name(){
		$sql = "SELECT name 
				FROM ".Config::get('SAVED_SEARCH_TABLE')." 
				WHERE id='".$this->search_id."'";
		$result = $this->dao->query($sql);
		$row = $result->fetch();
		if ( $row == false ) {
				return 'N/A';
		} 
		return stripslashes($row['name']);
	}
	
	function get_from($id){
		$sql = "SELECT * 
				FROM ".Config::get('USER_TABLE')." 
				WHERE id='".$id."'";
		$result = $this->dao->query($sql);
		$row = $result->fetch();
		$this->From = stripslashes($row['email']);
		$this->FromName = stripslashes($row['first_name'].' '.$row['last_name']);
		$this->member_id = $id;
	}
	
	function get_to(){
		if(empty($this->subscriber_id)){
			return $this->get_search_name().' Search';
		} else {
			$sql = "SELECT first_name, last_name, email
					FROM ".Config::get('CONTACT_TABLE')."
					WHERE id='".$this->subscriber_id."'";
			$result = $this->dao->query($sql);
			$row = $this->dao->fetch();
			if ( $row == false ) {
				return 'N/A';
			} 
			return $row['first_name'].' '.$row['last_name'].' &lt;'.$row['email'].'&gt;';
		}
	}
		
	function fetch(){
		if(empty($this->subscriber_id)){
			$sql = "SELECT sql 
					FROM ".Config::get('SAVED_SEARCH_TABLE')." 
					WHERE id='".$this->search_id."'";
			$result_sql = $this->dao->query($sql);
			if ( $result_sql->size() < 1 ) {
				return false;
			} else {
				$row = $result_sql->fetch();
			}
			$result = $this->dao->query($row['sql']);
			
		} else {
			$sql = "SELECT id, first_name, last_name, email
					FROM ".Config::get('CONTACT_TABLE')."
					WHERE id='".$this->subscriber_id."'";
			$result = $this->dao->query($sql);
		}
		if ( $result->size() > 0 ) {
			return $result;
		} 
		return false;
		
	}
	
	function replace_content($row){
		$html = $this->template_html;
		$html = str_replace("[NAME]", $row['first_name'], $html);
		$html = str_replace("[FULLNAME]", $row['first_name'].' '.$row['last_name'], $html);
		$html = str_replace("[CONTENT]", $this->html_body, $html);
		$html = str_replace("[EMAIL]", $row['email'], $html);
		$html = str_replace("[DATE]", date ("l, F jS, Y"), $html);
		$html = str_replace("[SERVER_NAME]", $_SERVER['SERVER_NAME'], $html);
		$html .= '<img src="http://'.$_SERVER['HTTP_HOST'].'../../email/save.php?id='.$row['id'].'&amp;message='.$this->id.'" width="1" height="1" border="0">';
		$unsubscribe = '<table width="550" border="0" align="center" cellpadding="0" cellspacing="0">
   				 	<tr><td align="center"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><br />
					You are receiving this information as a subscriber of Applied Communications.<br />
					If you do not want to receive additional information, please <a href="http://'.$_SERVER['HTTP_HOST'].'/email/unsubscribe/index.php?unsubscribe_email='.$row['email'].'">click here</a> to unsubscribe.<br />
					Copyright &copy; Applied Communications Ltd. All rights reserved.</font><br /></td></tr></table>';
		$html = str_replace("[UNSUBSCRIBE]", $unsubscribe, $html);
		$html = stripslashes($html);
		$this->Body = $html;
		return $html;
	}

	
	function preview($fake_data){
		$this->get_template($this->template_page);
		// Get From Information
		$this->get_from($this->member_id);
		// Get Search Information
		$this->get_search($this->search_id);
		$html = $this->replace_content($fake_data);
		return $html;
	}
	
	function set_email_body($new_value){
		$this->html_body  = stripslashes($new_value);
		$this->text_body  = stripslashes(strip_tags($new_value));
	}
	
	function set_subject($new_value){
		$this->Subject  = stripslashes(strip_tags($new_value));
	}

	function isError(){
		return ( !empty($this->error) ) ? true : false;
	}
		



	/**
	* validate_email function
	* @access public
	*/
	function validate_email($value){
		return $this->validate("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $value);
	}
		
	/**
	* validate_name function
	* @access public
	*/
	function validate_name($value){
		return $this->validate("^[A-Za-z�-��-��-� '\-\.]{1,22}$", $value);
	}
	
	/**
	* validate function
	* @access public
	*/
	function validate($expression, $value){
		if (!eregi($expression, $value)) {
			return false;
		} else {
			return true;
		}
	}
	
	function match_properties($array){ 
		$properties = get_object_vars($this);
		foreach($properties as $key=>$value) {
			if( isset($array[$key]) ) {
				$function = 'set_'.$key;
				if( method_exists($this, $function) ) {
					$this->$function( $array[$key] );
				} else {
					$this->{$key} = $array[$key];
				}
			}
		}  
	}

	function reset(){
		$properties = get_object_vars($this);
		foreach($properties as $key=>$value) {
			$this->$key = NULL;
		}  
		$parent = get_class($this);
		$this->$parent();
	}
	
	function getContactName(){
		$sql = "SELECT first_name, last_name 
				FROM ".Config::get('CONTACT_TABLE')."
				WHERE id='".$this->subscriber_id."'";
		$result = $this->dao->query( $sql );
		$row = $this->dao->fetch();
		if ( $row == false ) {
			return false;
		} 
		return $row['first_name'].' '.$row['last_name'];
	}
	
	function exists_email(){
		$result = array_unique($this->emails);
		$result = array_diff_assoc($this->emails, $result);
		$result = array_filter( $result);
		if(!empty($result)){
			return $result;
		}
		return false;
	}
	
	function is_valid(){ return true; }
	
	function save($status = ''){
		if( $this->track ){ $status = 'sent'; }
		if( empty($status) ){
			$status = 'prepared';
		} else {
			$status = $status;
		}
		if( empty($this->id) ){
			// INSERT
			$sql = "INSERT INTO ".Config::get('MESSAGE_TABLE')."
					SET template_page='".mysql_escape_string($this->template_page)."',
						subject='".mysql_escape_string($this->Subject)."',
						email_body='".mysql_escape_string($this->html_body)."',
						modified=NOW(),
						created=NOW(),
						status='".$status."',
						member_id='".$this->member_id."',
						search_id='".$this->search_id."'";
			$result = $this->dao->query($sql);
			if($result->isError()){
				return false;
			}
			$id = $result->insertID();
			$this->id = $id;
		} else {
			// UPDATE
			$sql = "UPDATE  ".Config::get('MESSAGE_TABLE')."
					SET template_page='".mysql_escape_string($this->template_page)."',
						subject='".mysql_escape_string($this->Subject)."',
						email_body='".mysql_escape_string($this->html_body)."',
						modified=NOW(),
						status='".$status."',
						member_id='".$this->member_id."',
						search_id='".$this->search_id."'
					WHERE id='".$this->id."'";
			$result = $this->dao->query($sql);
			if($result->isError()){
				return false;
			}
		}
		$this->load();
		return true;
	}
	
}
?>