<?php
/*
 * Template :: Template Manager Class. Version 1
*/

class EmailTemplate{
	//Property
	var $folder;
	var $path;
	var $contents;
	var $full_path;
	var $basename;
	// Constructor
	function EmailTemplate($path = ''){
		if(!empty($path)){
			$this->pathinfo($path);
		}
	}
	
	function pathinfo($path){
		$this->path = $path;
		$path_parts = pathinfo($path );
		$this->folder = $path_parts["dirname"].'/';
		$this->basename = $path_parts["basename"];
		$this->full_path = $_SERVER['DOCUMENT_ROOT'].$this->path;
	}
	
	// Public Methods
	
	function exists($path){
		if( empty($path) ) { return false; }
		if( file_exists($path) ){
			return true;
		}
		return false;
	}
	
	function delete($path=''){
		$path = (empty($path)) ? $this->full_path : $path ;
		if( empty($path) ) { return false; }
		@umask(0000); 
		@chmod($path, 0777);
		
		if(@unlink($path)){
			return true;
		} 
		return false;
	}
	
	function fetch($path=''){	
		$path = (empty($path)) ? $this->full_path : $path ;
		if($this->contents = file_get_contents($path)){
			return true;
		}
		return false;
	}
	
	function fetchAll($folder, $project_id){	
	// TODO CHECK FOR PROJECT ID TEMPLATES
		//echo($folder);
		// Validate folder as a directory
		if (is_dir($folder)) {
			// Initiate array for directory listing
			$dirlist = array();
			$d = dir($folder);
			while (false !== ($entry = $d->read())) {
			  if( $entry != '.' && $entry != '..'){
			   $dirlist[] = $entry;
			   
			   }
			}
			$d->close();
			if(empty($dirlist)){ return false; }
			asort($dirlist);
			return $dirlist;
		} else {
			return false;
		}
	}
	
	
    /**
    * Returns true if string valid, false if not
    * @return boolean
    * @access public
    */
    function isValid () {
        if ( count ($this->errors) > 0 ) {
            return false;
        } else {
            return true;
        }
    }
} // ends Template Class
?>