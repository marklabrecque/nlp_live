<?php
// Global CRM Configuration Vars
Config::set('CONTACT_TABLE','crm');
Config::set('PROJECT_TABLE','project'); 
Config::set('MESSAGE_TABLE','message'); 
Config::set('SAVED_SEARCH_TABLE','saved_search'); 
Config::set('USER_TABLE','member'); 
Config::set('GROUP_TABLE','collection'); 
Config::set('USER2GROUP_TABLE','member2collection'); 
Config::set('CRM2MESSAGE_TABLE','crm2message'); 
Config::set('EMAIL_TEMPLATES_FOLDER','/inc/templates/'); 
?>