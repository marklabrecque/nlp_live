<?php
/*
*    Written by Chris Geisler - dean@apartmentlocators.com
*
*    This script is distributed under the GPL License
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    http://www.gnu.org/licenses/gpl.txt
*
*/

class progress_bar {
	var $name;		// The name of the html element representing the progress bar.
	var $percent;   // The current percent value of the progress bar.
	var $width;     // The maximum width of the progress bar.

	/*Function progress_bar - The progress bar constructor function
	parameters:
	$name the name of the html element representing the progress bar.
	$percent the initial percent value of the progress bar.
	$width the initial width of the progress bar.
	$auto_create if set to TRUE the create() function will be called upon construction of the progress bar
	*/
	function progress_bar($name = 'pbar',$percent = 1,$width = 100,$auto_create = TRUE) {
		$this->name = $name;

		$this->percent = $percent;

		$this->width = $width;
		if($auto_create) {
			$this->create();
		}
  	}

	/*Function create() - Dispalys the progress bar as an html
	element. (Warning: do not call this function twice or if $auto_create is set to TRUE)

	parameters:
	$name sets the name of the html element
	(This function becomes usless after the create(); function is called.)*/
	function create() {
		?>
 <div align="center"> 
         <table  border="0" cellspacing="0" cellpadding="0"> 
            <tr> 
                 <td style="font-size: 12px; ">Progress:</td> 
                 <td name="<?php echo('cell_' . $this->name); ?>" style="width:<?php echo($this->width); ?>px; margin: 0px; padding: 0px; line-height: 0px; text-align:left; background-color: #EFEFEF; border:1px solid #333333; border-bottom-color: #999999;border-right-color: #999999; height: 8px; "><img name="<?php echo($this->name)?>" border="0" src="<?php echo(Config::get('ROOT')); ?>img/fill.gif" width="<?php echo(($this->percent * .01) * $this->width);?>" style="margin: 0px; padding: 0px; line-height: 0px; border: 0px;" height="8"></td> 
             </tr> 
        </table></div> 
<?php
	}

	/*Function set_name() - Sets the $percent of the object based
 	on current tasks done and the total tasks to finish.

	parameters:
	$name sets the name of the html element
	(This function becomes usless after the create(); function is called.)*/
	function set_name($name) {
		$this->name = $name;
	}

	/*Function set_percent() - Sets the $percent of the progress bar
 	using a pre-calculated percent.

	parameters:
	$percent the pre-calculated percent*/
	function set_percent($percent) {
		$this->percent = $percent;
		echo('<script>document.images.' . $this->name . '.width = ' . ($this->percent / 100) * $this->width . '</script>');
	}

 	/*Function set_percent_adv() - Sets the $percent of the object based
 	on current tasks done and the total tasks to finish.

	parameters:
	$cur_amount the curent number of tasks completed in a script
	$max_amount the number of tasks to complete in a script*/
	function set_percent_adv($cur_amount,$max_amount) {
		$this->percent = ($cur_amount / $max_amount) * 100;
		echo('<script>document.images.' . $this->name . '.width = ' . ($this->percent / 100) * $this->width . '</script>');
	}

	/*Function set_width() - Sets the maximum $width of the progress bar.

	parameters:
	$cur_amount the curent number of tasks completed in a script
	$max_amount the number of tasks to complete in a script*/
	function set_width($width) {
		$this->width = $width;
	}
}

?> 
