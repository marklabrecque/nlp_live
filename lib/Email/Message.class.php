<?php
require_once('config.inc.php');
/*
 * Message :: Message Manager Class. Version 1
*/

class Message {
	//Property
	var $dao;  // Data Access Object
	var $form_id;
	// used to allow functions to be called by form submits
	var $db_action;
	
	// Constructor
	function Message(&$dao){
	 	$this->dao = &$dao; 
		$this->form_id = 'message_id';
		$this->db_action = 'do';
		$this->do_action();

		switch(@$_GET['view']) {
			case 'distribute':	
				if($_POST[$this->db_action] == 'insert'){
					$result = $this->insert();
					$message_id = $result->insertID();
					$_POST['id'] = $message_id;
				} else {
					$this->update();
					$message_id = $_GET[$this->form_id ];
				}
					
				$this->distribute_confirm($message_id);
			break;
		}		
	}
	
	function do_action(){
		if ( isset($_POST[$this->db_action]) ) {
			$action = $_POST[$this->db_action];
			if( method_exists($this, $action) ) {
				$this->$action();			
			}
		}	
	}
	
	function get_results(){
		$this->dao->getMessage();
	}
	
	function delete(){
		$this->dao->delete("Record deleted for message: ".stripslashes($_POST['subject']));			
	}
	
	function copy(){
		$result = $this->dao->selectById($_POST['id']);
		$my_copy = array();
		$row = $result->fetch();
		
		$copy_keys = array('template_page', 'subject', 'email_body', 'search_id', 'member_id' );
		foreach( $row as $key=>$value){
			if(in_array($key, $copy_keys)) {
				$my_copy[$key] = $value;
			}
		}
		
		$this->dao->request = $my_copy; 
		$this->dao->insert();
		$insert_id = $this->dao->result->insertID();
		$this->dao->addMessage('Copied message with subject:'.$_POST['subject']. '.  You may <a href="edit/index.php?id='.$insert_id.'">edit message</a> to resend now.');
		$this->dao->request = $_POST;
	}
	

} // ends Ad Class
?> 
