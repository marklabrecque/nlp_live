 <?php
/**
* @package JPLIB
* @version $Id: DataCommandController.class.php,v 0.2 2006/04/05 11:06:11 joel Exp $
*/

/**
* Data Command Controller Class
* Controls the type of command being commited on the database and returns output
* @package JPLIB
* @author   Joel Pittet <joel@joelpittet.com>
* @version  v 0.1
* @access public
*/

/* Usage

$dcc = new DataCommandController($db, 'event');

// Setup DataCaommandController
// $dcc->action = 'do'; // Default
// $dcc->request = $_POST; // Default
$dcc->messages = array(
    'insert'=>"Added event dated '{event_date}' with the title '{title}'",
    'update'=>"Saved event dated '{event_date}' with the title '{title}'",
    'delete'=>"Deleted event dated '{event_date}' with the title '{title}'",
);
// Text areas or places where users will add wierd characters
$dcc->cleanupColumns = array('description');

// Execute Command from action
$dcc->execute();

// Get populated message
$result_message = $dcc->result_message;


*/

class DataCommandController{
    // Create Data Access Object
    var $dao;
    
    var $result_message = '';
    
    var $messages = array(
        'insert'=>"Added record #{id}'",
        'update'=>"Saved record #{id}'",
        'delete'=>"Deleted record #{id}'",
    );
    
    var $action = 'do';
    
    var $cleanupColumns = array();
    
    var $request;
	
	var $resultRow = array();
    
    // Hold current command
    var $command;
    
    // Constructor
    function DataCommandController(&$db, $table=''){
        if( empty($table) ){ 
            $this->dao = & $db;
        } else {
            $this->dao = & $db->create_dao( $table );
        }
        
        
        // Check if anything needs to be done
        $this->request = $_POST;
    }
    
    function execute(){
        if(isset($this->request[$this->action]) 
            && in_array($this->request[$this->action], array_keys($this->messages) ) ){
            // Store the command being executed
            $this->command = $this->request[$this->action];
            // Set initial value for the row
            $this->resultRow = false;
            
            if( $this->command == 'insert'  ){
                $this->cleanColumns();
                $this->dao->insert();
                $this->resultRow = $this->dao->selectById( $this->dao->insertID() );
            }
            
            // If it is not add check to make sure an 
            // primary key is set in the post array
            if( isset( $this->request[$this->dao->primaryKey]) 
                && !empty( $this->request[$this->dao->primaryKey]) ){
                // Grab result
                $this->resultRow = $this->dao->selectById( $this->request[$this->dao->primaryKey]);
                
                if($this->resultRow != false){
                
                    if( $this->command == 'update' ){
                        $this->cleanColumns();
                        $this->dao->update();
						// Grab result
              			$this->resultRow = $this->dao->selectById( $this->request[$this->dao->primaryKey]);
                
                    }
                    
                    if( $this->command == 'delete' ){
						// Grab result
              			$this->resultRow = $this->dao->selectById( $this->request[$this->dao->primaryKey]);
                        $this->dao->delete();
                    }
                }
            }
            // Checks selected row exists to replace tags
            if($this->resultRow != false){
                $this->replaceTagsWithResults();
            } else {
                $this->result_message = 'No records were affected by your action';
            }
        }
    }
    
    // Find tags that match the row's column names 
    // and replace tag with the rows data
    function replaceTagsWithResults(){
        $message = $this->messages[$this->command];
        foreach($this->resultRow as $key=>$value){
            $message = str_replace('{'.$key.'}', $value, $message);
        }
        $this->result_message =  $message;
    }
    
    function cleanColumns(){
        foreach($this->cleanupColumns as $value){
            $this->dao->request[$value] = $this->cleanCharacters($this->dao->request[$value]);
        }
    }
    
    // Cleans up some special characters 
    // from word and other programs
    function cleanCharacters($text){
       $badwordchars = array( chr(145), chr(146), chr(147), chr(148), chr(151) );
       $fixedwordchars = array( "'", "'", '&quot;', '&quot;', '&mdash;' );
       return str_replace($badwordchars, $fixedwordchars,$text);
    }
} // end class

?> 