<?php

// Create connection to the Database (file: connection.inc.php )
require_once( 'Database/MySQL.class.php');	
$db = new MySQL(
	'localhost', 
	'username', 
	'password', 
	'database' 
);


// Include connection.inc.php into a file with a form. (file: form.php)  
require_once( 'connection.inc.php');	
// 

//TEST DB TABLE STRUCTURE DUMP
/*
CREATE TABLE `menu` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `name` varchar(30) NOT NULL default '',
  `title` tinytext NOT NULL,
  `location` varchar(50) NOT NULL default '',
  `active` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;
*/

// Create a Data Access Object for the menu table
$menuDAO =& $db->create_dao('menu');

// Check post back with a hidden field named do
if(isset($_POST['do'])){
	// Check the value of the posted 'do'
	switch($_POST['do']){
		// Automatic
		case 'delete':
			// Delete matches the POST['primaryKey'] to delete a row from the table 
			// Where primaryKey is your tables primaryKey 
			$menuDAO->delete();	
			$result = 'You have deleted the menu ID# '.$menuDAO->request['id'];
		break;
		case 'insert':
			// Insert automatic does is matches form field names with 
			// column names and builds a SQL query string for you primaryKey
			// If the primaryKey is autoincrement then you don't have to add it.
			$menuDAO->insert();	
			// Gets the recently added menu ID
			$result = 'You have added a new menu ID# '.$menuDAO->insertID();
		break;
		case 'update':
			// Update automatic does is matches form field names with 
			// column names and builds a SQL query string for you
			$menuDAO->update();	
			$result = 'You have updated menu ID# '.$menuDAO->request['id'];
		break;
		// Manual 
		case 'manualInsert':
			// Request is default the $_POST  array, you can overwrite this for manual entry
			// See how the key/value pairs represent column/value in the database
			$menuDAO->request = array('parent_id'=>0, 'location'='index.php', 'name'='home', 'title'='Your Homepage');
			$menuDAO->insert();	
			$$result = 'You have manually added a new menu ID# '.$menuDAO->insertID();
		break;
	}
} 

/**
* Other things you can do with the DAO
*/

// Manual Select from a database
$sql = "SELECT * FROM menu";
// Query manual sql statement and create Result Object
$result = $menuDAO->query($sql);
// If more than 0 rows
if($result->size() > 0) {
	// Loop through fetched rows
	while($row = $result->fetch() ){
		// Display array of row values
		print_r($row);
	}
}

// Auto build select all
$result = $menuDAO->selectAll();

// Auto select all by ID
$result = $menuDAO->selectById('1231');


// Auto build select
// All params are optional in order ('Columns','Tables','Where Clause','Order BY','Limit')
$result = $menuDAO->select('column1, column2', 'table1, table2', 'column1=2 AND column2=3', 'column2', '5');



// Checking for duplicates, if inserted already, if missing etc...
// second parameter is optional if there is a value in request['location'] 
// ie a postback or manual change see manual insert above
if( $menuDAO->exists( 'location', 'index.php' ) ){ 
	// Yup we have one of those 
} else {
	// Nope we don't have of those
}

// Getting the values of an enum column
$enumColumnValues = $menuDAO->getEnum( 'enumColumn' );// returns an array of enum values for a column

// Find out what the primary key is of your table
echo( $menuDAO->getPrimaryKey() );

// Get an array of all the column names
print_r( $menuDAO->getColumns() );

// Return an array of all primary keys if more than one...
print_r( $menuDAO->getPrimaryKeys() );

// Get defaults of all the columns
print_r( $menuDAO->getDefaults() );

// Get default of a specific field
echo( $menuDAO->getDefault('active') );

// Return the MySQL field info of field
echo( $menuDAO->getFieldInfo('active') );





?>
