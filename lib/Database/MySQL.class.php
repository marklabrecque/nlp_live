<?php
/**
* @package JPLIB
* @version $Id: MySQL.class.php,v 1.4.9 2006/03/15 4:17:11 joel Exp $
*/
/**
* MySQL Database Connection Class
* @access public
* @package JPLIB
* Original modified from SPLIB by  Harry Fuecks
* @package JPLIB
*/
class MySQL {
    /**
    * MySQL server hostname
    * @access private
    * @var string
    */
    var $host;
    
    /**
    * MySQL username
    * @access private
    * @var string
    */
    var $dbUser;
    
    /**
    * MySQL user's password
    * @access private
    * @var string
    */
    var $dbPass;
    
    /**
    * Name of database to use
    * @access private
    * @var string
    */
    var $dbName;
    
    /**
    * MySQL Resource link identifier stored here
    * @access private
    * @var string
    */
    var $dbConn;
    
    /**
    * Stores error messages for connection errors
    * @access private
    * @var string
    */
    var $connectError;
    
    /**
    * MySQL constructor
    * @param string host (MySQL server hostname)
    * @param string dbUser (MySQL User Name)
    * @param string dbPass (MySQL User Password)
    * @param string dbName (Database to select)
    * @access public
    */
    function MySQL ($host,$dbUser,$dbPass,$dbName) {
        $this->host=$host;
        $this->dbUser=$dbUser;
        $this->dbPass=$dbPass;
        $this->dbName=$dbName;
        $this->connectToDb();
    }
    
    /**
    * Establishes connection to MySQL and selects a database
    * @return void
    * @access private
    */
    function connectToDb () {
        // Make connection to MySQL server
        if (!$this->dbConn = @mysql_connect($this->host,
                                  $this->dbUser,
                                  $this->dbPass)) {
        trigger_error('Could not connect to server');
        $this->connectError=true;
        // Select database
        } else if ( !@mysql_select_db($this->dbName,$this->dbConn) ) {
            trigger_error('Could not select database');
            $this->connectError=true;
        }
    }
    
    /**
    * Checks for MySQL errors
    * @return boolean
    * @access public
    */
    function isError () {
        if ( $this->connectError )
            return true;
        $error=mysql_error ($this->dbConn);
        $return = ( empty ($error) ) ? false : true;
        return $return;
    
    }
    
    /**
    * Returns an instance of MySQLResult to fetch rows with
    * @param $sql string the database query to run
    * @return MySQLResult
    * @access public
    */
    function & query($sql) {
        if (!$queryResource=mysql_query($sql,$this->dbConn))
        trigger_error ('Query failed: '.mysql_error($this->dbConn).
                       ' SQL: '.$sql); 
        $result = new MySQLResult($this,$queryResource);
        return $result;
        }
    
    function create_dao($table_name ) {
        return new DataAccess($this, $table_name);
    }
    
}
    
/**
* MySQLResult Data Fetching Class
* @access public
* @package JPLIB
*/
class MySQLResult {
    /**
    * Instance of MySQL providing database connection
    * @access private
    * @var MySQL
    */
    var $mysql;
    
    /**
    * Query resource
    * @access private
    * @var resource
    */
    var $query;
    
    var $row;
    var $rows_per_page;
    
    /**
    * MySQLResult constructor
    * @param object mysql   (instance of MySQL class)
    * @param resource query (MySQL query resource)
    * @access public
    */
    function MySQLResult(& $mysql,$query) {
        $this->row = 0;
        $this->mysql=& $mysql;
        $this->query=$query;
    }
    
    /**
    * Fetches a row from the result
    * @return array
    * @access public
    */
    function fetch () {
    
        if(!$this->query) {
            return false;
        }
        
        if(isset($this->rows_per_page)) {
            if ( $this->row >= $this->rows_per_page ) { 
                return false; 
            }
            $this->row++;
        }
        if ( $row = mysql_fetch_array($this->query,MYSQL_ASSOC) ) {
            foreach($row as $key=>$value) {
                $row[$key] = stripslashes($value);
            }
            return $row;
        } else if ( $this->size() > 0 ) {
            mysql_data_seek($this->query,0);
            return false;
        } else {
            return false;
        }
    }
    
    /**
    * Returns the number of rows selected
    * @return int
    * @access public
    */
    function size () {
        if(!$this->isError() && $this->query) {
            return mysql_num_rows($this->query);
        }
    }
    
    /**
    * Returns the ID of the last row inserted
    * @return int
    * @access public
    */
    function insertID () {
        return mysql_insert_id($this->mysql->dbConn);
    }
    
    /**
    * Checks for MySQL errors
    * @return boolean
    * @access public
    */
    function isError () {
        return $this->mysql->isError();
    }
}
    
/**
* DataAccess Data Access Class
* @access public
* @package JPLIB
*/
class DataAccess {
    // Properties
    var $message;   // array
    var $fields;  // array for field names
	var $fieldInfo; // array for field information gathered from show Columns
	
    var $table;  
    
    var $db;   // MySQL Object
    var $result; // MySQLResult Object
    var $sql; // Sql Query

    var $match_values = array();  //array
    var $request = array();
	
    var $primaryKey;
    var $primaryKeys;
    // Accessors and Mutators
    function setMessage($new_value) { $this->message[] = $new_value;    }
    function getMessage() { return $this->message;    }
    
    function setTable($new_value) { $this->table = $new_value;    }
    function getTable() { return $this->table;    }
    
    /**
    * DataAccess constructor
    * @param string table name
    * @access public
    */
    function DataAccess ( &$db, $table="" ) {
        $this->match_values = array();
        $this->db = &$db; 
        $this->request = $_POST;
        $this->table = $table; 
        $this->getColumns();
        $this->getPrimaryKeys();
    } // Ends Constructor Method
    
    // Core Functions
    /**
    * query
    * Queries the database for information and sets errors if nessasary
    * @param string sql   (SQL query string)
    * @access public
    */
    function query($sql) {
        $this->sql = $sql;
        $this->result = $this->db->query($sql);
        return $this->result;
    } // ends query method
    
    /**
    * update
    * Updates the database based on the array in this class
    * @param string message   (User Action string)
    * @access public
    */
    function update() {
		$primary_key = $this->getPrimaryKey();
		$id = $this->request[$primary_key]; 
        // Initialize values from form for update of database
       	if( $this->match() && isset($id) ) {
			$updateFields = array();
			foreach($this->match_values as $key=>$value) {
				$updateFields[] = $key."=".$value;
			}
			$sql = "UPDATE ".$this->table." 
					SET ".join(',',$updateFields)." 
					WHERE ".$primary_key." ='".$id."'";
			$this->query($sql );
		}
    }
    
    /**
    * insert
    * Inserts into the database based on the array in this class
    * @param string message   (User Action string)
    * @access public
    */
    function insert() {
        // Initialize values from form for insert into database 
		if( $this->match() ) {
			$matchedColumns = array_keys($this->match_values);
			$sql = "INSERT INTO ".$this->table." (".join(',', $matchedColumns).") 
					VALUES (".join(',',$this->match_values).")";
			$this->query($sql );
		}
    }
    
    /**
    * Returns the ID of the last row inserted
    * @return int
    * @access public
    */
    function insertID () {
        return $this->result->insertID();
    }
    
    /**
    * delete
    * @param string message   (User Action string)
    * Deletes a record from database based on the array in this class
    * @access public
    */
    function delete( $where = "") {
        
		if(empty($where)){
			$primary_key = $this->getPrimaryKey();
			$where = $primary_key."='".$this->request[$primary_key]."'";
		} 
		
        $sql = "DELETE FROM ".$this->table." WHERE ".$where;
				
        $this->query($sql);
    }
    
    // Private function for insert and update
    /**
    * match
    * Matches array values with table field values
    * @access public
    */
    function match() { 
    	$this->match_values = array();
        $has_match = false;
        foreach($this->fields as $field_name) {
            if( isset($this->request[$field_name]) && !is_array($this->request[$field_name]) ) {
                $value = addslashes( trim($this->request[$field_name]) );
                $value = ( substr($value, -2) != '()' ) ? "'".$value."'" : $value;
                $this->match_values[$field_name] = $value;
				$has_match = true;
            }
        }  
		return $has_match;
    }
    
    /**
    * selectById
    * @param int id   (primary key id of a record)
    * Selects a single record from the current table
    * @access public
    */
    function selectById($id='') {
        $primaryKey = $this->getPrimaryKey();
        if($id == '') {
            $id = $this->request[$primaryKey];
        }
        $sql = "SELECT * 
                FROM ".$this->table." 
                WHERE ".$primaryKey."='".$id."'";
        $result = $this->query($sql);
		if($result->size() > 0) {
		  	return $result->fetch();
		}
        // For simple select by id call this functino and fetch and don't use the returned value
      	return false;
    }
    
    /**
    * selectAll
    * Selects all records from the current table
    * @access public
    */
    function selectAll() {
		if( !isset($this->fields) || empty($this->fields) ) { $this->getColumns(); }
		// Preload the fields so that the query will run faster
		$fields = join(', ', $this->fields);
        $sql = "SELECT ".$fields." FROM ".$this->table;
        $result = $this->query($sql);
        // For simple select all call this function and fetch and don't use the returned value
        return $result;
    }
    
    /**
    * select
    * Selects all records from the current table
    * @access public
    */
    function select($aFields='*', $aTable='', $aWhere='', $aOrder = '', $aLimit='') {
        // Set field to choose from
        $fields = ( is_array($aFields) )? join($aFields, ', ') : $aFields;
        if( !empty($aTable)  ) {
            $table = ( is_array($aTable) )? join($aTable, ', ') : $aTable;
        } else {
            $table = $this->table;
        }    
        $where = ( !empty($aWhere) )? " WHERE ".$aWhere." ":'';
        // Set order
        $order = ( !empty($aOrder) ) ? $aOrder : $this->primaryKey; 
        // If need to return a certain amount
        $limit = ( !empty($limit) && is_numeric($limit) )? " LIMIT ".$limit." ":'';
        // Build Select Query
        $sql = "SELECT ".$fields." 
                FROM ".$table.$where." 
                ORDER BY ".$order.$limit;
        // Get Result
        $result = $this->query($sql);
        return $result;
    }
    
    /**
    * fetch
    * Wrapper for MysqlResult Fetch function to get records
    * @access public
    */
    function fetch() {
        return $this->result->fetch();
    } 
    
    /**
    * getColumns
    * @param  N/A
    * @return Array
    * Grabs all the field names from a table
    */
    function getColumns() {
        if( isset($this->fields) ) { unset($this->fields);    }
        $sql = "SHOW COLUMNS FROM ".$this->table;
		$this->fields = array();
        $this->query( $sql );
        while($row = $this->fetch() ) {
			$field_name = trim($row["Field"]);
		   	$this->fieldInfo[$field_name] = $row;
            $this->fields[] = $field_name;
        }
        return $this->fields;
    }
    
    /**
    * exists
    * @param  $value, $field
    * @return boolean
    * checks specific fields in a table
    * to see if the value already exists
    */
    function exists( $field, $value=NULL ) {
        if($value == NULL) {
            $value = $this->request[$field];
        }
        $sql = "SELECT * 
                FROM ".$this->table." 
                WHERE ".$field."='".$value."'";
        $this->query( $sql );
        if ( $this->fetch() == false ) {
            return false;
        } 
        return true;
    } // ends exists method
    
    /**
    * addMessage
    * @param  $message
    * @return N/A
    * Adds an error to the error message array
    * to see if the value already exists
    */
    function addMessage( $message = "" ) {
        if( !empty($message) ) { $this->setMessage( $message );    }
    } // ends addMessage function 
    
    /**
    * enum_values
    * @param  $table, $field
    * @return Array
    * Finds all the enumerated values for a 
    * specific field
    */
    function getEnum( $field ) {
        $row = $this->getFieldInfo($field);
        return( explode("','", preg_replace("/.*\('(.*)'\)/", "\\1", $row["Type"])) );
    } // ends enum_values method
    
    // For old scripts
    function get_enumValues( $field ) {
        return $this->getEnum( $field );
    }
    
    /**
    * getPrimaryKey
    * @return mixed
    * gets the field name of the first primary key value of a specified field
    */
    function getPrimaryKey( ) {
        if(isset($this->primaryKey) && !empty($this->primaryKey) ) {
            return $this->primaryKey;
        }
		if( !isset($this->fields) || empty($this->fields) ) { $this->getColumns(); }
		
		$fieldInfo = $this->fieldInfo;
        foreach ( $fieldInfo as $row) {
            if($row['Key']=='PRI') {
                $this->primaryKey = $row["Field"];
                return $row["Field"];
            }
        }
    }
    
    // For old scripts
    function get_primaryKey(  ) {
        return $this->getPrimaryKey( );
    }
    
    
    /**
    * getPrimaryKeys
    * @return mixed
    * gets the default value of a specified field
    */
    function getPrimaryKeys() {
        if( isset($this->primaryKeys) && !empty($this->primaryKeys) ) {
            return $this->primaryKeys;
        }
		if( !isset($this->fields) || empty($this->fields) ) { $this->getColumns(); }

        $keys = array();
		$fieldInfo = $this->fieldInfo;
		if( !is_array($fieldInfo) ){ return false; }
        foreach ( $fieldInfo as $row) {
            if($row['Key']=='PRI') {
                $keys[] = $row["Field"];
            }
        }
        $this->primaryKeys = $keys;
        $this->primaryKey = $keys[0];
        if( empty($this->primaryKeys) ) { return false; }
        return $keys;
    }
    
    /**
    * getDefaults
    * @return mix
    * gets the array of default values
    */
    function getDefaults() {
        $defaults = array();
		if( !isset($this->fields) || empty($this->fields) ) { $this->getColumns(); }
        $fields = $this->fields;
        foreach($fields as $field_name) {
            $defaults[$field_name] = $this->getDefault( $field_name ); 
        }
        return $defaults;
    }
    
    
    // For old scripts
    function get_defaults( ) {
        return $this->getDefaults();
    }


    /**
    * getDefault
    * @param  $field
    * @return mix
    * gets the default value of a specified field
    */
    function getDefault( $field ) {
        $row = $this->getFieldInfo($field);
        return $row["Default"];
    } 
    
    // For old scripts
    function get_default( $field ) {
        return $this->getDefault( $field );
    }
    
    /**
    * getType
    * @param  $field
    * @return mix
    * gets the type value of a specified field
    */
    function getType( $field ) {
        $row = $this->getFieldInfo($field);
        return $row["Type"];
    } 
    
    /**
    * getFieldInfo
    * @param  $field
    * @return mix
    * gets the type value of a specified field
    * @access private
    */
    function getFieldInfo( $field ) {
		if( !isset($this->fields) || empty($this->fields) ) { $this->getColumns(); }
        return $this->fieldInfo[$field];
    } 
    
    // For old scripts
    function display_messages() {
        return $this->message;
    }
} // End of DataAccess Class
?>