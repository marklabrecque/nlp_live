<?php
/**
* @package JPLIB
* @version $Id: PagedResultSet.php,v 2.2.2 2006/02/20 9:42:11 joel Exp $
*/
/**
* PagedResultSet Paged MySQL Result Set Class
* @access public
* @package JPLIB
*/
class PagedResultSet {
    // Properties
    var $results;            // will be used to store the actual MySQLResult object.
    var $rowsPerPage;        // will store the Numberber of records to be displayed per page.
    /**
    * Private
    * 
    */
    var $pageNumber;        // will keep track of which page of the result set is to be displayed.
	var $pageNumber_var;

    var $totalPages;
    var $startPage;
    var $totalRows;
    var $getvars;
    var $output;
    
    // Accessors & Mutators
    function getRowsPerPage() { return $this->rowsPerPage; }
    function setRowsPerPage($rowsPerPage) { $this->rowsPerPage = $rowsPerPage; }
    
    function getpageNumber() { return $this->pageNumber; }
    function setpageNumber($pageNumber){
		$this->pageNumber = $pageNumber;
		
		// Set starting point
		if($this->totalRows <= 0) {
			return false;
		}
		
		
		if( !empty($this->rowsPerPage) ){
			$seek_result = @mysql_data_seek($this->results->query,($pageNumber-1) * $this->getRowsPerPage());
		} else {
			$seek_result = @mysql_data_seek($this->results->query, 1);
		}
		return $seek_result;
    }
    
    // Constructor
    function PagedResultSet( &$results, $rows_per_page=NULL ) {
		$this->pageNumber_var = 'pageNumber';
  		$this->startPage = 1;
        $this->results =& $results ;
		$this->results->rows_per_page = $rows_per_page;
		$this->totalRows = $this->results->size();
        $this->setRowsPerPage( $rows_per_page );
		
		// Determine page valid number
		$pageNumber = ( isset($_GET[$this->pageNumber_var]) ) ? (int)$_GET[$this->pageNumber_var] : 1;
        if ($pageNumber < 1) { $pageNumber = 1; }
        if ($pageNumber > $this->getNumberPages()) {
            $pageNumber = $this->getNumberPages();
        }
        $this->setpageNumber($pageNumber);
    }

    function getNumberPages() {
        if ( $this->totalRows ) {
            if( !empty($this->rowsPerPage)){
				$this->totalPages = ceil($this->totalRows/$this->rowsPerPage);
            } else {
                $this->totalPages = 1;
            }
        } else {
            $this->totalRows = 0;
            $this->totalPages = 0;
        }
        return $this->totalPages;
    }

    function isLastPage() { return ($this->getpageNumber() >= $this->getNumberPages()); }
    function isFirstPage() { return ($this->getpageNumber() <= 1); }
	
	function pages($numEachSide=5){
    	$end_page = $this->totalPages;
        $next = $this->pageNumber + $numEachSide;
        $previous = $this->pageNumber - $numEachSide;
		
        if ( $previous < $this->startPage ){ 
			$next = $next + abs($this->startPage-$previous); 
		}
		
        if ( $next > $end_page ){ 
			$previous = $previous - abs($end_page-$next); 
		}
		
        if ( $previous < $this->startPage ){ 
			$previous = $this->startPage; 
		}
		
        if ( $next > $end_page ){ 
			$next = $end_page; 
		}
		
		$pages = array();
        for ($i=$previous; $i<=$next; $i++) {
            $pages[] = $i;
        }    
        
        return $pages;
	}
	
    function previousJump($number){
		if ( $this->pageNumber-$number >= $this->startPage ) {
			return $this->pageNumber-$number;
		}
		return false;
	}
	function first(){ return $this->startPage; }
	function last(){ return $this->totalPages; }
	
	function nextJump($number){
		if ( $this->pageNumber+$number <=  $this->totalPages ) {
			return $this->pageNumber+$number;
		}
		return false;
	}
	
	function previous(){
		if ($this->pageNumber > 1) {
			return $this->pageNumber-1;
		}
		return false;
	}
	
	function next(){
		if ( $this->pageNumber < $this->totalPages ) {
			return $this->pageNumber+1;
		}
		return false;
	}
	
	// Deprecated Functions
	/*
	STOP USING THESE
	*/
    function previous_output( $queryvars ){
        $previous='<div style="float: left; width:33%; text-align: left;">&nbsp;';
        if ($this->pageNumber > 1) {
            if ( $this->pageNumber-10 >= $this->startPage ) {
                // Write Previous 10 Link If Possible
                $previous .= " <a href=\"?".$this->pageNumber_var."=".( $this->pageNumber-10 ).
                        "&".$queryvars."\"><font size=\"3\">&laquo;</font>PREV 10</a> | ";
            }
            // Write Previous Link
             $previous .= "<a href=\"?".$this->pageNumber_var."=".($this->pageNumber-1).
                    "&".$queryvars."\"><font size=\"3\">&laquo;</font>PREV</a>";
        }
        $previous .= "</div>\n";
        return $previous;
    }
    
    function pages_output($previous, $next, $queryvars){
        $pages='<div style="float: left; text-align:center; width:34%;">&nbsp;';   
        $pages.="|";
        for ($i=$previous; $i<=$next; $i++) {
            $pages .=( $i == $this->pageNumber )?" ".$i."&nbsp;|":"<a href=\"?".$this->pageNumber_var."=".$i."&".$queryvars."\"> ".$i." </a>|";
        }    
        
        $pages .= "</div>\n";
        return $pages;
	}
    
    function next_output($queryvars){
        $next='<div style="float: right; width:33%; text-align: right;">&nbsp;';

        if ( $this->pageNumber < $this->totalPages ) {
            // Write Next Link
            $next .= " <a href=\"?".$this->pageNumber_var."=".($this->pageNumber+1).
                    "&".$queryvars."\">NEXT<font size=\"3\">&raquo;</font></a> ";
            if ( $this->pageNumber+10 <=  $this->totalPages ) {
                // If Possible write Next 10
                $next .= " | <a href=\"?".$this->pageNumber_var."=".($this->pageNumber+10 ).
                    "&".$queryvars."\"> NEXT 10<font size=\"3\">&raquo;</font></a> ";
            }
        }
        $next .= "</div>";
        return $next;
    }
    
	function getPageNav( $queryvars="" ) {
        $end_page     = $this->totalPages;
        $next         = $this->pageNumber + 5;
        $previous     = $this->pageNumber - 5;
        if ( $previous < $this->startPage ){ $next = $next+abs($this->startPage-$previous); }
        if ( $next > $end_page ){ $previous = $previous-abs($end_page-$next); }
        if ( $previous < $this->startPage ){ $previous = $this->startPage; }
        if ( $next > $end_page ){ $next = $end_page; }
        // Start Paged Navigation Div tag
        $this->output.='<div class="pagedNav">';
           // LEFT
        $this->output .= $this->previous_output( $queryvars );
        // MIDDLE
        $this->output .= $this->pages_output( $previous, $next, $queryvars );
        // RIGHT
        $this->output .= $this->next_output( $queryvars );
        // END
        $this->output .= '<div class="spacer">&nbsp;</div>';
        $this->output .= '</div>';
        return $this->output;
        //return $nav;
    }
    
}
?>