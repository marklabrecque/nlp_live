<?php
/**
* @package SPLIB
* @version $Id: ValidateEmail.php,v 1.3 2003/10/19 21:43:01 harry Exp $
*/
/**
* Include the base validator class
*/
require_once 'Validator.class.php';
/**
* Validates an email address
* <code>
* $validator = new ValidEmail('jbloggs@yahoo.com');
* if ( ! $validator->isValid() ) {
*   while ( $error = $validator->fetch() ) {
*       echo ( $error.'<br />' );
*   }
* }
* </code>
* @access public
* @package SPLIB
*/
class ValidateFolder extends Validator {
    /**
    * Validates an email address
    * Note that the regular expression used here will not allow certain validate
    * email addresses, such as someone@195.11.25.34:25
    * @param string email address to validate
    * @access protected
    * @return void
    */
    function validate($folderInfo) {
		$base_folder = $folderInfo[0];
        $folder = $folderInfo[1];
		$full_path = $_SERVER['DOCUMENT_ROOT'].$base_folder.$folder;
      	clearstatcache();
	   if (empty( $folder )) {
            $this->setError('Folder name is empty');
			return false;
        }
		if (!file_exists($full_path)) {
			 $this->setError('The folder "'.$base_folder.$folder.'" does not exist');
			 return false;
		}
		if (!is_dir($full_path)) {
			 $this->setError('The folder "'.$base_folder.$folder.'" is not a directory');
			 return false;
		}
		if ( !is_writable($full_path) ) {
		 	$this->setError('Folder is not writable.');
		}
// Check permissions

    }
}
?>