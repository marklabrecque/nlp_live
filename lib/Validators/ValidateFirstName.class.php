<?php
/**
* @package SPLIB
* @version $Id: ValidateEmail.php,v 1.3 2003/10/19 21:43:01 harry Exp $
*/
/**
* Include the base validator class
*/
require_once 'Validator.class.php';
/**
* Validates an email address
* <code>
* $validator = new ValidEmail('jbloggs@yahoo.com');
* if ( ! $validator->isValid() ) {
*   while ( $error = $validator->fetch() ) {
*       echo ( $error.'<br />' );
*   }
* }
* </code>
* @access public
* @package SPLIB
*/
class ValidateFirstName extends Validator {
    /**
    * Validates an email address
    * Note that the regular expression used here will not allow certain validate
    * email addresses, such as someone@195.11.25.34:25
    * @param string email address to validate
    * @access protected
    * @return void
    */
    function validate($email) {
        if (strlen($email)<2){
            $this->setError('Enter a first name');
        }
    }
}
?>