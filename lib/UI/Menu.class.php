<?php
/**
 * @package JPLIB
 * @version $Id: Menu.class.php,v 1.3.4 2008/09/21 6:57:11 joel Exp $
 */
/**
 * Menu Class
 * @access public
 * @package JPLIB
 * No need for a database! But still accomodates one
 */

class MenuData {

	private $menu_class;
	private $ids = array();
	

	
	function MenuData (&$menu_class){
		$this->menu_class = $menu_class;
	}
	
	function generateId(){
		if( empty($this->ids) ) return 1;
		// grab last id to start
		$start_id = end($this->ids);
		
		while( $this->idExists($start_id) ){
			$start_id++;
		}
		//echo '<br />'.$start_id;
		return $start_id;
		
	}
	
	function idExists($id) {
		//if(isset($id) && !empty($id))exit('id:'.$id. print_r($this->ids, true). (int)in_array( $id, $this->ids));
		if( in_array( $id, $this->ids) ) return true;
		return false;
	}
	
	function add($url="#", $name="", $parent_id=0, $id = 0,  $class=""){
		if( $id == 0 || !is_numeric($id) || $this->idExists($id) ) {
			$id = $this->generateId();
			
		}
		array_push($this->ids, $id);
		array_push($this->menu_class->menuData, array(   
			'id'=>$id, 
			'url'=>$url, 
			'name'=>$name, 
			'parent_id'=>$parent_id, 
			'class'=>$class )
		);
		
		return $id;
	}
}


class Menu {

	var $basefolder = '';
	var $currentClass = 'current';
	var $itemPrefix = 'm-';
	var $menuData = array();
	var $location;
	var $parents = array();
	
	var $ulLevelAttributes = array();
	
	// Constructor
	function Menu( $location, $basefolder='' ) {

		$this->location = $location;
		
		$this->basefolder = $basefolder;
		
		
		$this->getParents($this->getCurrentParent());



		
	}
	
	function hasChildren( $id ){
		foreach( $this->menuData as $menu_row ) {
			if( $menu_row['parent_id'] == $id )return true;
		}
		return false;
	}
	
	function getCurrentParent() {
		
		foreach( $this->menuData as $menu_row ) {
			
			if(	$this->basefolder.$menu_row['url'] == $this->location ) {
				return $menu_row['parent_id'] ;
			}
		}
	}
	
	function getParents($parent_id) {
		if(is_numeric($parent_id) && $parent_id > 0) {
			$this->parents[] = $parent_id;
		}
		foreach( $this->menuData as $menu_row ) {
			if(	$menu_row['id'] == $parent_id ) {
				if(is_numeric($menu_row['parent_id']) && $menu_row['parent_id'] > 0) {
					$this->getParents($menu_row['parent_id']);
				}
				return;
			}
		}
	
	}
	

	function countChildren( $parent_id ) {
		$count = 0;
		foreach( $this->menuData as $menu_row ) {
			if( $menu_row['parent_id'] == $parent_id ) $count++;
		}
		return $count;
	}
	
	// This function could be better if I can remove the html
	function build( $parent = 0, $level_limit = -1, $level = 0 ){
	

		// Start by setup of the un ordered list
		$ulAttr = (isset($this->ulLevelAttributes[$level]) && !empty($this->ulLevelAttributes[$level])) ?
		 	str_pad($this->ulLevelAttributes[$level], strlen($this->ulLevelAttributes[$level])+1, ' ', STR_PAD_LEFT) : '';
		echo "\n<ul" . $ulAttr .">";
		
		$level++;
		$i = 0;
		
		
		// Loop through elements checking the following:
		// On correct level?
		// Is page current?
		// Is page a parent?
		
		$numberOfChildren = $this->countChildren( $parent );
		
		foreach( $this->menuData as $menu_id => $menu_row ) {
		
			$class = '';
			$classes = '';

			// Check Level
			if( $menu_row['parent_id'] == $parent ) {

				
				// Check current page
				
				
				$classes = $menu_row['class'];
				if(	$this->basefolder.$menu_row['url'] == $this->location || in_array($menu_row['id'], $this->parents) ) $classes .= ' current ';
				if($i == 0) $classes .= ' first ';
				if($i == $numberOfChildren-1) $classes .= ' last ';
				
				if( !empty($classes) ){ $class = ' class="'.trim($classes).'"'; }
				
				// Clean up URL
				$link = str_replace('index.php','', $this->basefolder.$menu_row['url']);
				$id = $this->itemPrefix.str_replace(' ', '-', strtolower($menu_row['name']));

				
				
				echo ('<li id="'.$id.'"'.$class.'><a href="'.$link.'" title="'.$menu_row['name'].'">'.$menu_row['name'].'</a>'); 
				// Is page a parent?
				if( $this->hasChildren($menu_row['id']) ){
					// Recursive call
					
					if($level_limit == -1 || $level < $level_limit)
						$this->build(  $menu_row['id'], $level_limit, $level);
				}
				echo '</li>'."\n"; 
				$i++;
				
			}
			
		}
		echo '</ul>'; 
		
	}
	
	
}
	
?>