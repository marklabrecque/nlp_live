<?php 
require_once( 'Image.class.php'); 
require_once( 'ImageResize.class.php'); 
require_once( CLASSES_FOLDER.'Database/PagedResultSet.class.php'); 
class PhotoGallery {
	var $dao;
	var $sizes;
	var $web_root;
	var $web_url;
	var $form_id;
	var $db_action;
	
	// Constructor
	function PhotoGallery( & $db, $display_links=0){
		$this->dao = & $db->create_dao('photogallery');
		$this->form_id = 'image_id';
		$this->db_action = 'sql_type';
		// key is folder name
		// first and second are max width and max height
		// Last is jpg quality
		$this->sizes = array('/thumbs/'=>array(94, 94, 60),
							 '/'=>array(400, 400, 80)
							 );
		$this->web_root = IMG_FOLDER.'/slideshow/photos';
		$this->web_url = ROOT.'img/slideshow/photos'; 
		if($display_links == 1){
			$this->display_links();
		}else{
			$this->do_action();
			
			if ( !empty($_GET['photo_id']) ) {
				$this->form($_GET['photo_id']);
			} else {
				$this->display();
			}
		}
	}// Ends constructor
			
	
	
	function do_action(){
		if ( isset($_POST[$this->db_action]) ) {
			$action = $_POST[$this->db_action];
			$this->$action();			
			$this->dao->display_messages();
		}	
	}
	
	// Public Methods
	
	function display_add(){
		?>
		<form action="<?php echo($_SERVER['PHP_SELF']); ?>" method="post" name="add_photo" enctype="multipart/form-data"> 
			<table  border="0" cellspacing="1" cellpadding="2"> 
				<tr> 
					<td align="right">Photo: </td> 
					<td><input name="photo" type="file" size="20" maxlength="1000000"> 
						<input type="hidden" name="MAX_FILE_SIZE" value="2440000" /> </td> 
				</tr> 
				<tr> 
					<td>&nbsp;</td> 
					<td><input type="hidden" name="<?php echo($this->db_action); ?>" value="insert" /> 
						<input type="submit" class="button" value="Add Photo" /></td> 
				</tr> 
			</table> 
		</form> 
		<?php
	}
	
	
	function display() { 
		// Use page result to display photos
		$this->display_add(); ?> 
		<?php 
		$result = $this->dao->select_all();
		$pageResult = new PagedResultSet($result ,12);
		//$sort_arr = $subscriber_obj->get_sort();
		$sort_by = 'ASC';
		$where_by = '1';
		$order_by = 'date';
		if($pageResult->getNumberPages() > 1) {
			echo('<p><strong>Total Pages:</strong> '.$pageResult->getNumberPages().'</p>');
			$nav = $pageResult->getPageNav('order='.$order_by.'&amp;sort='.$sort_by.'&amp;'.$where_by);
			$nav = '<div style="width: 100%;">'.$nav.'</div>';
			echo($nav);
		} else if($pageResult->getNumberPages() < 1) { 
			echo('<p>There are no customers</p>');
		} 
		while ( $row = $result->fetch() ) {
			foreach($row as $key=>$value){
				$row[$key] = stripslashes($value);
			}
			extract($row);
			
			/* subscribers */
			?> <div class="float"><table width="100%"  border="0" cellpadding="0" class="row1" cellspacing="0"  style="padding: 0px; margin: 0px 0px 5px;">
                <tr valign="top">
                    <td><a href="<?php echo($_SERVER['PHP_SELF']); ?>?photo_id=<?php echo($row['id']); ?>" >
					<img src="/admin/img/edit.gif" alt="edit" name="edit<?php echo($row['id']); ?>" width="62" height="17" border="0"  /></a></td>
                    <td align="right">
					<form style="margin: 0px; padding: 0px; display:inline; " action="<?php echo($_SERVER['PHP_SELF']); ?>" method="post" name="delete_photo<?php echo($row['id']); ?>" onSubmit="return confirmsubmit('Are you sure you want to delete &quot;<?php echo(addslashes($photo)); ?>&quot;');">
					<input type="hidden" name="id" value="<?php echo($row['id']); ?>" />
					<input type="hidden" name="photo" value="<?php echo($photo); ?>" />
					<input type="hidden" name="<?php echo($this->db_action); ?>" value="delete" />
					<input name="delete" type="image" width="22" height="17" value="delete" src="/admin/img/delete.gif" style="margin: 0px;" />
					</form></td>
                </tr>
            </table>
			<div align="center"><a href="<?php echo($_SERVER['PHP_SELF']); ?>?photo_id=<?php echo($row['id']); ?>">
			<img src="<?php echo( $this->web_url.'/thumbs/'.$row['id'].'.jpg' ); ?>" style="  border: 1px solid   #003399;" alt=""></a></div>
		</div>
		<?php
		} 
		?><div class="spacer">&nbsp;</div><?php
		if( isset($nav) ) { echo( $nav ); } 
	}
	
	function form_button($name, $link='') {
		$link = ($link == '') ? $_SERVER['PHP_SELF'] : $link;
		echo('<form action="'.$link.'" method="post" style="display:inline"> 
			<input type="submit" class="button" value="'.stripslashes($name).'"> 
			</form>');
	}
	
	/****************************************************************
	 * Function:    form
	 * Parameters:  $photo_id
	 * Return Type: N/A
	 * Description: Displays a Form depending on get vars
	 ****************************************************************/
	function form($photo_id ){
		$row['id'] = $photo_id;
		$sql = "SELECT * FROM ".$this->dao->getTable()." WHERE id='".$row['id']."'";
		$result = mysql_query($sql);
		if ( mysql_num_rows($result) > 0 ){
			$row = mysql_fetch_assoc( $result );
			// init for field variables for update form
			foreach($row as $key=>$value){
				$row[$key] = stripslashes($value);
			}
			extract($row);
		}
		$this->form_button('Back To Lists');
		?> 
 
<br /> <hr size="1" noshade>

<div style="width: 350px; margin: 10px 0px;"> 
    <table border="0" cellspacing="0" cellpadding="0"> 
        <tr> 
            <td><form action="<?php echo($_SERVER['PHP_SELF']); ?>?photo_id=<?php echo($row['id']); ?>" method="post" name="update_article" enctype="multipart/form-data"> 
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0"> 
                        <tr><td class="header_row">Change Photo:</td></tr> 
                        <tr><td class="row1"><input name="photo" type="file" size="24" maxlength="1000000" />
							<input type="hidden" name="MAX_FILE_SIZE" value="2440000" />
							</td>
						</tr> 
                        <tr><td class="row2" align="right"> <input type="hidden"  name="id" value="<?php echo($row['id']); ?>" /> 
                                <input type="hidden" name="<?php echo($this->db_action); ?>" value="update"> 
                                <input name="update" type="submit" value="update">
							</td> 
                        </tr> 
                    </table> 
                </form></td> 
            <td valign="top"> <?php if(!empty($photo)){ ?> 
                <p style="padding: 0px 0px 0px 20px; "><img src="<?php echo( $this->web_url.'/thumbs/'.$photo ); ?>" style="  border: 1px solid   #003399;" alt=""></p> 
                <?php } ?> </td> 
        </tr> 
    </table> 
</div> 
<br /> 
<?php  
	}	
	
	function display_links(){
		$result = $this->dao->select_all();
		$pageResult = new PagedResultSet($result ,12);
		//$sort_arr = $subscriber_obj->get_sort();
		$sort_by = 'ASC';
		$where_by = '1';
		$order_by = 'date';
		if($pageResult->getNumberPages() > 1) {
			echo('<p><strong>Total Pages:</strong> '.$pageResult->getNumberPages().'</p>');
			$nav = $pageResult->getPageNav('order='.$order_by.'&amp;sort='.$sort_by.'&amp;'.$where_by);
			$nav = '<div style="width: 100%;">'.$nav.'</div>';
			echo($nav);
		} else if($pageResult->getNumberPages() < 1) { 
			echo('<p>There are no customers</p>');
		} 
		while ( $row = $result->fetch() ) {
			extract($row);
			echo('<div class="float"><p>');
			$image_size = getimagesize($this->web_root.$photo);
			echo('<a href="javascript:void(0);" onclick="photo_open(\''.ROOT.'inc/php/photo_display.inc.php?photo='.$this->web_url.'/'.$photo.'\',\''.$image_size[0].'\',\''.$image_size[1].'\');">');
			echo('<img src="'.$this->web_url.'/thumbs/'.$photo.'" alt="" style="border: 1px solid #FFFFFF; "  /></p>');
			echo('</a>');
			echo('</p></div>');
		}
		?><div class="spacer">&nbsp;</div><?php
		if( isset($nav) ) { echo( $nav ); } 
	}

	
	/****************************************************************
	 * Function:    delete_photos
	 * Parameters:  id of photo, field name of image, message for dialog
	 * Return Type: N/A
	 * Description: Deletes photos from the filesystem
	 ****************************************************************/
	function delete_photos($id){
		$sql = "SELECT photo FROM ".$this->dao->getTable()." WHERE id ='".$id."'";
		$result = mysql_query( $sql ) or die (mysql_error().$sql);
		$row = mysql_fetch_assoc( $result );
		$image = $row['photo'];
		foreach($this->sizes as $folder=>$value){
			$this->delete_file($this->web_root.$folder.$image);
		}
		mysql_free_result($result);
	}
	
	/****************************************************************
	 * Function:    delete_file
	 * Parameters:  $file:String  // Relative
	 * Return Type: Boolean
	 * Description: Deletes files
	 ****************************************************************/
	function delete_file($file){
		if ( @file_exists($file) ) {
			if( @unlink($file) ){
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	
	function insert() {
		if($filename = $this->check_file( 'photo' ) ) {
			if($this->resize_image( 'photo' ) ){
				$_POST['photo'] = $filename;
				$this->dao->insert();			
			}	
		}
	}
	
	function update() {
		if($filename = $this->check_file( 'photo' ) ) {
			//*** delete old photos ***
			$this->delete_photos( $row['id']);
			$_POST['photo'] = $filename;
			$this->resize_image( 'photo');
			$this->dao->update();
		}
	}
	
	function delete(){
		extract($_POST);
		$this->delete_photos( $row['id']);
		$this->dao->delete(  "Record deleted successfully for photo: ".stripslashes($photo) );
	}
	
	function resize($photo, $id){
			$img_obj = new Image($photos_uploaded['tmp_name'][$i]);
		$sql = "INSERT INTO gallery_photos (
		   filename,
		   caption
		 ) VALUES (
		   '0',
		   '" . $captions[$i] . "'
		 )";
		$result = $db->query($sql);
		// New Id generated
		$new_id = $result->insertID(); 
		$sql = "INSERT INTO gallery_category2photo (
		   category_id,
		   photo_id
		 ) VALUES (
		   '" . $_POST['category_id'] . "',  
		    '" . $new_id . "'
		 )";
		$result = $db->query($sql);
		
		// Get the filetype of the uploaded file
		$filetype = $photos_uploaded['type'][$i];  
		
		// Get the extension for the new name
		$extension = $known_types[$filetype];  
		
		// Generate a new name
		$filename = $new_id.'.'.$extension;  
		
		$img_obj->save($_SERVER['DOCUMENT_ROOT'].'/images/'.$filename);
		// Scale and save image 
		$img_obj->scale(94, 94);
		$img_obj->save($_SERVER['DOCUMENT_ROOT'].'/images/thumbs/'.$filename);
		
		// let�s update the filename now
		$sql = "UPDATE gallery_photos 
				SET filename = '".$filename."' 
				WHERE id = '".$new_id."'";
		$db->query( $sql );
	
	}
	
	function resize_image($form_file_name = "photo"){
		if( is_dir($this->web_root) ){
			if( !is_writable($this->web_root) ){
				umask(0011);
				chmod($this->web_root, 0766 );
			}
			$image = new ImageResize($form_file_name);
			foreach($this->sizes as $folder=>$size_properties ){
				$image->setDestination_folder($this->web_root.$folder);
				$image->setMaxWidth($size_properties[0]);	
				$image->setMaxHeight($size_properties[1]);
				$image->setJpgQuality($size_properties[2]);
				$image->resizer_main();
			}
			
			return $imageObj1->getDestination_photoName();
		} else{
			$this->dao->add_message( "No folder available to put image in:<br />".$this->web_root );
			return false;
		}
	}
	
	/****************************************************************
	 * Function:    check_file
	 * Parameters:  $form_file_name:String
	 * Return Type: Boolean
	 * Description: checks an uploaded file
	 ****************************************************************/
	function check_file($form_file_name = 'photo'){
		if( isset($_FILES[$form_file_name]['name']) && !empty($_FILES[$form_file_name]['name']) ){
			$photo = $_FILES[$form_file_name]['name'];
		} else {
			$this->dao->add_message( "Invalid File Name" );
			return false;
		}		
		if( !isset($_FILES[$form_file_name]['tmp_name']) || $_FILES[$form_file_name]["tmp_name"] == "none") {
			echo($_FILES[$form_file_name]['tmp_name']);
			$this->dao->add_message( "Your file was not recieved.<br />The file was not the valid format" );
			return false;
		}
		if( !$_FILES[$form_file_name]['error'] == 0 ) {
			$this->dao->add_message( "An error occured in the file upload" );
			return false;
		}
		if( $this->dao->is_duplicate( $form_file_name, $photo ) ){ 
			$this->dao->add_message( "The filename was already in the database" );
			return false;
		}		
		if ( $_FILES[$form_file_name]['size'] <= 0 ){ 
			$this->dao->add_message( "The file size was incorrect" );
			return false;
		} 
		return $photo;
	}


}// End of PhotoGallery Class

?>