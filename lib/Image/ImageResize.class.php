<?PHP
/****** Image Resize  *******/
// version:		v.3.1.0
// author: 		Joel Pittet
// email: 		joel@joelpittet.com
// company: 	Applied Communications
// date: 		March 5, 2003
// licence: 	free to use under the following conditions
// contributors:  

//********** CONDITIONS *********//
// Please leave above comments in when copying this code and if improved on 
// send it back to my email with your name so I can add your contribution to the internet 
//*******************************//
#############################################################################################

class ImageResize {
	// Properities
	var $uploaded_fieldName; 		// String
	var $uploaded_photoName;		// String
	var $destination_photoName;		// String
	var $uploaded_photoTempName;	// String
	var $created_imageResource;		// Resource
	var $original_imageHeight;		// int
	var $original_imageWidth;		// int  
	var $resized_imageHeight;		// int  
	var $resized_imageWidth;		// int  
	var $maxHeight;					// String(*) or int  
	var $maxWidth;					// String(*) or int 
	var $destination_folder;		// String
	var $destination_file_prefix;	// String
	var $use_imagecreatetruecolor; 	// boolean
	var $use_imagecopyresampled;	// boolean
	var $jpgQuality;				// int

	// Accessors & Mutators
	function setUploaded_fieldName( $uploaded_fieldName ) 				{ $this->uploaded_fieldName = $uploaded_fieldName; }
	function getUploaded_fieldName() 									{ return $this->uploaded_fieldName; }
	
	function setUploaded_photoName( $uploaded_photoName ) 				{ $this->uploaded_photoName = $uploaded_photoName; }
	function getUploaded_photoName() 									{ return $this->uploaded_photoName; }
	
	function setDestination_photoName( $destination_photoName ) 		{ $this->destination_photoName = $destination_photoName; }
	function getDestination_photoName() 								{ return $this->destination_photoName; }
	
	function setCreated_imageResource( $created_imageResource ) 		{ $this->created_imageResource = $created_imageResource; }
	function getCreated_imageResource() 								{ return $this->created_imageResource; }
	
	function setUploaded_photoTempName( $uploaded_photoTempName ) 		{ $this->uploaded_photoTempName = $uploaded_photoTempName; }
	function getUploaded_photoTempName() 								{ return $this->uploaded_photoTempName; }
	
	function setOriginal_imageHeight( $original_imageHeight ) 			{ $this->original_imageHeight = $original_imageHeight; }
	function getOriginal_imageHeight() 									{ return $this->original_imageHeight; }
	
	function setOriginal_imageWidth( $original_imageWidth )				{ $this->original_imageWidth = $original_imageWidth; }
	function getOriginal_imageWidth() 									{ return $this->original_imageWidth; }
	
	function setResized_imageHeight( $resized_imageHeight )				{ $this->resized_imageHeight = $resized_imageHeight; }
	function getResized_imageHeight() 									{ return $this->resized_imageHeight; }
	
	function setResized_imageWidth( $resized_imageWidth ) 				{ $this->resized_imageWidth = $resized_imageWidth; }
	function getResized_imageWidth() 									{ return $this->resized_imageWidth; }
	
	function setMaxHeight( $maxHeight ) 								{ $this->maxHeight = $maxHeight; }
	function getMaxHeight() 											{ return $this->maxHeight; }
	
	function setMaxWidth( $maxWidth ) 									{ $this->maxWidth = $maxWidth; }
	function getMaxWidth() 												{ return $this->maxWidth; }
	
	function setDestination_folder( $destination_folder ) 				{ $this->destination_folder = $destination_folder; }
	function getDestination_folder() 									{ return $this->destination_folder; }
	
	function setDestination_file_prefix( $destination_file_prefix ) 	{ $this->destination_file_prefix = $destination_file_prefix; }
	function getDestination_file_prefix() 								{ return $this->destination_file_prefix; }
	
	function setUse_imagecreatetruecolor( $use_imagecreatetruecolor ) 	{ $this->use_imagecreatetruecolor = $use_imagecreatetruecolor; }
	function getUse_imagecreatetruecolor() 								{ return $this->use_imagecreatetruecolor; }
	
	function setUse_imagecopyresampled( $use_imagecopyresampled ) 		{ $this->use_imagecopyresampled = $use_imagecopyresampled; }
	function getUse_imagecopyresampled() 								{ return $this->use_imagecopyresampled; }
	
	function setJpgQuality( $jpgQuality ) 								{ $this->jpgQuality = $jpgQuality; }
	function getJpgQuality() 											{ return $this->jpgQuality; }
	
	// Constructor Method
	function ImageResize( $fieldName ) {		
		$this->setUploaded_fieldName( $fieldName );	
		$this->setMaxHeight('*');											// Default to not resize.  Thumbnails are nice at 75 or 100 pixels	
		$this->setMaxWidth('*');											// Default to not resize  Thumbnails are nice at 75 or 100 pixels	
		$this->setDestination_folder('img/');							// A folder relative to the root with no ending or beginning slashes	
		$this->setDestination_file_prefix('');								// Suggestion for thumbnails is 'thumb_'  if the a thumbnail is created in the same folder	
		$this->setUse_imagecreatetruecolor(1);								// Set this to 0 when creating this object if server is running older than 4.3 for PHP with no GD 2.0	
		$this->setUse_imagecopyresampled(1); 								// Set this to 0 when creating this object if server is running older than 4.3 for PHP with no GD 2.0	
		$this->setJpgQuality(90);  											// best results on a resampled image
		
		global $_FILES;	
		$this->setUploaded_photoName	( $_FILES [ $this->getUploaded_fieldName() ]["name"] );			// sets the uploaded photos name
		$this->setUploaded_photoTempName( $_FILES [ $this->getUploaded_fieldName() ]["tmp_name"] );	// sets the uploaded photos tenp name
	}
	
	// Methods
	#############################################################################################
	function resizer_main(){
		if( trim( $this->getUploaded_photoTempName() ) == "" || trim( $this->getUploaded_photoTempName() ) =="none" ) return false;
		$this->image_from_upload();
		$this->calculate_sizes();
		$this->image_get_resized();
		$this->make_filename();
		imagejpeg($this->getCreated_imageResource(), $this->getDestination_folder().$this->getDestination_photoName(), $this->getJpgQuality() );
		return $this->dislpay_resultsTable();
	}

	function image_from_upload() {
		$image_size =  getimagesize( $this->getUploaded_photoTempName() ); 
		####### Now create original image from uploaded file. Be carefull! GIF is often not supported, as far as I remember from GD 1.6
		switch( $image_size[2] ){
			case 1:	$img = ImageCreateFromGif	( $this->getUploaded_photoTempName() ); 
			break;
			case 2: $img = ImageCreateFromJpeg	( $this->getUploaded_photoTempName() ); 
			break;
			case 3: $img = ImageCreateFromPng	( $this->getUploaded_photoTempName() ); 
			break;
			case 4: $img = ImageCreateFromSwf	( $this->getUploaded_photoTempName() ); 
			break;
			default: die("<br><font color=\"red\"><b>Sorry, this image type is not supported yet.</b></font><br>");
		} //case
		$this->setOriginal_imageHeight( $image_size[1] );
		$this->setOriginal_imageWidth( $image_size[0] );
		$this->setCreated_imageResource( $img );
	}

	function calculate_sizes() {
		## Get Multipliers
		if( $this->getOriginal_imageWidth() == $this->getMaxWidth() 
			&& $this->getOriginal_imageHeight() == $this->getMaxHeight() ){
			$this->setResized_imageWidth( $this->getOriginal_imageWidth() );
			$this->setResized_imageHeight( $this->getOriginal_imageHeight() );
		} else {
			if ( ($this->getMaxWidth() == "*") || ($this->getMaxHeight() == "*") ) {
				if($this->getMaxWidth() == "*" && $this->getMaxHeight() == "*") {
					$mlt=1;
				} else if($this->getMaxWidth() == "*") {
				  	$mlt = $this->getMaxHeight() / $this->getOriginal_imageHeight();
				} else if($this->getMaxHeight() == "*") {
					$mlt = $this->getMaxWidth() / $this->getOriginal_imageWidth();
				}
			} else {
				$mlt_w = $this->getMaxWidth() / $this->getOriginal_imageWidth();
				$mlt_h = $this->getMaxHeight() / $this->getOriginal_imageHeight();				
				$mlt = $mlt_w < $mlt_h ? $mlt_w:$mlt_h;
			}
			#### Calculate new dimensions
			$img_new_height =  round( $this->getOriginal_imageHeight() * $mlt);
			$img_new_width =  round( $this->getOriginal_imageWidth() * $mlt);
			$this->setResized_imageHeight( $img_new_height );
			$this->setResized_imageWidth( $img_new_width );
		}
	}

	function image_get_resized(){
		if( $this->getUse_imagecreatetruecolor() && function_exists("imagecreatetruecolor") ){
			$img_resized = imagecreatetruecolor( $this->getResized_imageWidth(), $this->getResized_imageHeight() ) or die("<br><font color=\"red\"><b>Failed to create destination image.</b></font><br>"); 
		} else {
			$img_resized = imagecreate( $this->getResized_imageWidth(), $this->getResized_imageHeight() ) or die("<br><font color=\"red\"><b>Failed to create destination image.</b></font><br>"); 
		}
		if( $this->getUse_imagecreatetruecolor() && function_exists("imagecopyresampled") ){
			imagecopyresampled( $img_resized, $this->getCreated_imageResource(), 0, 0, 0, 0, $this->getResized_imageWidth() , $this->getResized_imageHeight(), $this->getOriginal_imageWidth(), $this->getOriginal_imageHeight() ) or die("<br><font color=\"red\"><b>Failed to resize @ ImageCopyResampled()</b></font><br>"); 
		} else {
			imagecopyresized( $img_resized, $this->getCreated_imageResource(), 0, 0, 0, 0, $this->getResized_imageWidth() , $this->getResized_imageHeight(), $this->getOriginal_imageWidth(), $this->getOriginal_imageHeight() ) or die("<br><font color=\"red\"><b>Failed to resize @ ImageCopyResized()</b></font><br>"); 
		}
		$this->setCreated_imageResource( $img_resized );
	}

	function make_filename(){
		$file_name 	= trim($this->getUploaded_photoName());  
		$pos 		= strrpos( $file_name, '.' );
		$ext 		= substr( $file_name, $pos );
		$name 		= substr( $file_name, 0, $pos );
		$name 		= str_replace(" ", "_", $name);		
		$name 		= str_replace(".", "-", $name);
		$name 		= strtolower($name);		
		$file_name 	= $this->getDestination_file_prefix().$name.$ext;
		$this->setDestination_photoName( $file_name );
	}
	
	function dislpay_resultsTable() {
		$folder 		= $this->getDestination_folder();
		$photoName 		= $folder.$this->getDestination_photoName();
		$image_size 	=  getimagesize( $photoName ); // to be included in properties eventually
		$this->getResized_imageHeight();
		$this->getResized_imageWidth();
		$fileSize 		= filesize( $photoName );
		$rowStart 		= "<tr><td width=\"47%\"><div align=\"right\">";
		$middleCells 	= " </div></td><td width=\"53%\">";
		$html  = "<tr><td colspan=\"2\"><div align=\"center\"><img src=\"".$photoName."\" ".$image_size[3]."></div></td></tr>";
		$html .= $rowStart."Dimentions allowed:".$middleCells.$this->getMaxWidth()." x ".$this->getMaxHeight()."</td></tr>";
		$html .= $rowStart."Dimentions created:".$middleCells.$this->getResized_imageWidth()." x ".$this->getResized_imageHeight()."</td></tr>";
		$html .= $rowStart."File size:".$middleCells.$fileSize."</td></tr>";
		$html .= $rowStart."JPG Quality:".$middleCells.$this->getJpgQuality()."</td></tr>";
		return $html;
	}
//end of ImageResize Class
}
?>