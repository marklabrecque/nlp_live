<?php

/**
* @package JPLIB
* @version $Id: Image.class.php, v0.0.2 2004/06/22 10:26:11 joel Exp $
*/

/**
* Image Manipulation Class
* @access public
* @package EBLIB
*/
class Image {
	/**
	* prefix of image name 
	* @access private
	* @var string
	*/
	var $prefix;
	
	/**
	* postfix of image name 
	* @access private
	* @var string
	*/
	var $postfix;
	
	/**
	* image name 
	* @access private
	* @var string
	*/
	var $name;
	
	/**
	* temporary image file
	* @access private
	* @var string
	*/
	var $tmp_name;
	
	/**
	* image resource
	* @access private
	* @var string
	*/
	var $resource;
	
	/**
	* getimagesize array
	* @access private
	* @var mixed
	*/
	var $dimensions;
	
	/**
	* should the image be randomly generated
	* @access private
	* @var boolean
	*/
	var $is_name_random;
	
	/**
	* destination folder for image
	* @access private
	* @var string
	*/
	var $folder;
	
	/**
	* size of image in bytes
	* @access private
	* @var int
	*/
	var $filesize;
	
	/**
	* width of resized image
	* @access private
	* @var int
	*/
	var $width;
	
	/**
	* height of resized image
	* @access private
	* @var int
	*/
	var $height;
	
	/**
	* quality of resized image
	* @access private
	* @var int
	*/
	var $quality;
	
	/**
	* allows or disallows overwritting of files
	* @access private
	* @var boolean
	*/
	var $is_overwrite;
	
	/**
	* field name of image
	* @access private
	* @var int
	*/
	var $field_name;
	
	/**
	* is there an error
	* @access private
	* @var boolean
	*/
	var $error;
	
	/**
	* fieldname of image
	* @access private
	* @var mixed
	*/
	var $error_message;
	
	
	/**
	* Image constructor
	* @param string host (MySQL server hostname)
	* @access public
	*/
	function Image($field_name=''){
		$this->field_name = $field_name;
		if( !empty($_FILES[ $this->field_name ]) ) {
			$this->name = $_FILES [ $this->field_name ]["name"];
			$this->tmp_name = $_FILES [ $this->field_name ]["tmp_name"];
			$this->error = 0;
			$this->check_upload_error();
			$this->set_dimensions();
			$this->set_resource();
			$this->is_overwrite = 0;
			$this->filesize = $_FILES [ $this->field_name ]["size"];
			$this->quality = 90;
		} else {
			 $this->set_error( 'You must upload a file to manipulate');
		}
	}
	
	/**
	* Goes through the errors on upload
	* @return void
	* @access public
	*/
	function check_upload_error(){
	   switch ($_FILES[ $this->field_name ]['error']) {       
		   // They must specify a document on a new node
		   case 4:
			   $this->set_error( 'You must upload a file to resize');
			   break;   
		   /// Now check the error messages
		   case 1:
		   case 2:   
			    $this->set_error( 'The image you have attempted to upload is too large.');
			   break;                       
		   case 3:   
			    $this->set_error( 'An error occured while trying to recieve the file. Please try again.');
			   break;                                       
	   }
	}
	
	/**
	* Sets the error message and turns the error flag on
	* @return void
	* @access public
	*/
	function set_error($message){
	 	$this->error_message[] = $message;
		$this->error = 1;
	}
	
	/**
	* Creates a file name based on this objects 
	* prefix, postfix, name and is_random_name
	* @return void
	* @access private
	*/
	function create_filename($new_name=''){
		// get original name
		$file_name = trim($this->name); 
		$pos = strrpos( $file_name, '.' );
		$ext = substr( $file_name, $pos );
		$name = substr( $file_name, 0, $pos );
		if($new_name != '') {
			$name = $new_name;
		}
		
		if($this->is_name_random == 1){
			$this->make_random_filename(8);
			$file_name = $this->name;
		} else {
			// Replace all spaces with underscores 
			$file_name = str_replace(" ", "_", $name);		
		}
		if(!empty($this->prefix)){
			$file_name 	= $this->prefix.$file_name;
		}
		if(!empty($this->postfix)){
			$file_name 	= $file_name.$this->postfix;
		}
		$this->name = $file_name.$ext;
	}
	
	/**
	* used by make_filename to generate a random 
	* name for a file if required
	* @param $nlen int lentho of filename
	* @return void
	* @access private
	*/
	function make_random_filename($nlen){
		srand ((double)microtime()*1000000);
		$rndfl = rand($nlen/2, $nlen);
		$ltr = range('a', 'z');
		$num = range('0', '9');
		// Merge ranges and shuffle them
		$num_ltr = array_merge($num, $ltr);
		shuffle($num_ltr);
		$size = count($num_ltr);
		// From shuffled letters randomly choose 
		// one and concatinate to string
		$newnm = '';
		for ($i=1; $i<=$rndfl; $i++) {
			$rndltr = rand(1, $size);
			$newnm .= $num_ltr[$rndltr];
		}
		$this->name = $newnm;
	}
	
	/**
	* Rotates an image
	* @param $degrees int degrees of rotation
	* @return void
	* @access public
	*/
	function rotate( $degrees=0 ){
		if($this->error == 1) { return false; }
		if(!function_exists('imagerotate')){
			$this->set_error('Image rotate function doesn\'t exist. Please contact your administrator.');
			return false; 
		}
		switch($degrees){
			case 90:
				$degrees = 90;
				break;
			case 180:
				$degrees = 180;
				break;
			case 270:
			case -90:
				$degrees = 270;
				break;
			default: 
				$degrees = 0;
		}
	  	$this->resource = imagerotate($this->resource,$degrees,0);
	}
	
	/**
	* Adds an image ontop of this objects 
	* image as a watermark
	* @param $image string image name and location
	* @param $x int position of image on top of this objects image in x pixels
	* @param $y int position of image on top of this objects image in y pixels
	* @return void
	* @access public
	*/
	function add_watermark($image, $x=0, $y=0){}
	
	/**
	* Gathers information from getimagesize function on image
	* @return void
	* @access private
	*/
	function set_dimensions(){
		if($this->error == 1) { return false; }
		$this->dimensions =  getimagesize( $this->tmp_name );
	}
	
	/**
	* Uses the temp file from the form field to get 
	* the image resouce to work on.
	* @return void
	* @access private
	*/
	function set_resource(){
		if($this->error == 1) { return false; }
		// Now create original image from uploaded file. 
		// Be carefull! GIF is often not supported, as far as I remember from GD 1.6
		switch( $this->dimensions[2] ){
			case 1:	$img = ImageCreateFromGif	( $this->tmp_name ); 
			break;
			case 2: $img = ImageCreateFromJpeg	( $this->tmp_name ); 
			break;
			case 3: $img = ImageCreateFromPng	( $this->tmp_name ); 
			break;
			case 4: $img = ImageCreateFromSwf	( $this->tmp_name ); 
			break;
			default: 
				$this->set_error('Sorry, this image type is not supported yet.');
				return 0;
		} //case
		$this->resource = $img ;
	}
	
	/**
	* Checks for Image errors
	* @return boolean
	* @access public
	*/
	function is_error(){
		return $this->error;
	}
	
	/**
	* Caculates the new image size dependant 
	* on scaling
	* @return void
	* @access public
	*/
	function scale($width=0, $height=0){
		if($this->error == 1) { return false; }
		if($width != 0 ) {  $this->width = $width; }
		if($height != 0 ) {  $this->height = $height; }
		## Get Multipliers
		if( $this->dimensions[0] == $this->width && $this->dimensions[1] == $this->height ){
			$this->height = $this->dimensions[1];
			$this->width = $this->dimensions[0];
		} else {
			if ( ($this->width == '') || ($this->height == '') ) {
				if($this->width == '' && $this->height == '') {
					$mlt=1;
				} else if($this->width == '') {
				  	$mlt = $this->height / $this->dimensions[1];
				} else if($this->height == '') {
					$mlt = $this->width / $this->dimensions[0];
				}
			} else {
				$mlt_w = $this->width / $this->dimensions[0];
				$mlt_h = $this->height / $this->dimensions[1];				
				$mlt = $mlt_w < $mlt_h ? $mlt_w:$mlt_h;
			}
			#### Calculate new dimensions
			$new_height =  round( $this->dimensions[1] * $mlt);
			$new_width =  round( $this->dimensions[0] * $mlt);
			$this->height = $new_height;
			$this->width = $new_width;
		}
		$this->resize();
	}
	
	function scale_crop($width=0, $height=0) {
		if($this->error == 1) { return false; }
		if($width != 0 ) {  $this->width = $width; }
		if($height != 0 ) {  $this->height = $height; }
		// Final width and Final hight declairations
		$fw = $this->width;
		$fh = $this->height;
	   	$tempw = $fw;
	   	$temph = floor((($this->dimensions[1]*$fw)/$this->dimensions[0]));
	  
	   	if($temph < $fh) {
		   	$tempw = floor((($this->dimensions[0]*$fh)/$this->dimensions[1]));
		   	$temph = $fh;
	   	}
          
		$this->width = $tempw;
		$this->height = $temph;
		$this->resize();
        // Calculate offsets
		if($temph > $fh) {
			$offsety = floor(($temph/2)-($fh/2));
			$offsetx = 0;  
		} else {
			$offsety = 0;
			$offsetx = floor(($tempw/2)-($fw/2));
		}
		$this->width = $fw;
		$this->height = $fh;
		$this->resize(0, 0, $offsetx, $offsety, $fw, $fh, $fw, $fh);
	}
	
	/**
	* Resizes the image to specified caculated sizes
	* @return void
	* @access private
	*/
	function resize( $dstX = 0, $dstY = 0, $srcX = 0, $srcY = 0, $srcW = 0, $srcH = 0){
		// if srcW or srcH are 0 then grab original values from dimentsions array
		if($srcW == 0) {$srcW = $this->dimensions[0]; }
		if($srcH == 0) {$srcH = $this->dimensions[1]; }
		$img_resized = @imagecreatetruecolor( $this->width, $this->height );
		if( $img_resized == false ){
			$img_resized = imagecreate( $this->width, $this->height );
			// or die("<br><font color=\"red\"><b>Failed to create destination image.</b></font><br>"); 
		}
			// or die("<br><font color=\"red\"><b>Failed to create destination image.</b></font><br>"); 
		if( @imagecopyresampled( $img_resized, $this->resource, 0, 0, 0, 0, $this->width , $this->height, $srcW, $srcH ) == false ){
			imagecopyresized( $img_resized, $this->resource, 0, 0, 0, 0, $this->width , $this->height, $srcW, $srcH );
			// or die("<br><font color=\"red\"><b>Failed to resize @ ImageCopyResized()</b></font><br>"); 
		}
		$this->resource = $img_resized;
	}
	
	
	
	/**
	* save the image file to a folder
	* @return void
	* @access public
	*/
	function save($new_name=''){
		if($this->error == 1) { return false; }
		if(!empty($new_name) ) { 
			$this->create_filename($new_name); 
		} else {
			$this->create_filename(); 
		} 
		if( $this->is_overwrite == 0 && @file_exists($this->folder.$this->name) ) {
			$this->set_error('Filename already exists.');
			return false;
		}
		imagejpeg($this->resource, $this->folder.$this->name, $this->quality );
	}


} // end class

?>