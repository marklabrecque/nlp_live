<?php
/**
* @package JPLIB
* @version $Id: Navigation.class.php,v 1.10 2004/02/20 19:59:15 joel pittet Exp $
*/
/**
* Navigation Class
* Interfaces with a table
* @package JPLIB
* @access public
* @todo PHP < 4.3.0 compatibility
*   +------------------------------------------------------------------------------+ 
*       Project Name : Navigation Class                                                              
*   +------------------------------------------------------------------------------+ 
*       Copyright Notice(s)                                                         
*   +------------------------------------------------------------------------------+ 
*       Disclaimer Notice(s)                                                          
*       ex: This code is freely given to you and given "AS IS", SO if it damages      
*       your computer, formats your HDs, or burns your house I am not the one to 
*       blame.                                                                     
*       Moreover, don't forget to include my copyright notices and name.               
*   +------------------------------------------------------------------------------+ 
*		Conditions
* 		Please leave above comments in when copying this code and if improved on 
* 		send it back to my email with your name so I can add your contribution to
*	 	the internet 
*   +------------------------------------------------------------------------------+ 
*       Author(s): 	Joel Pittet (Bind727)  
*  		Email: 		joel@joelpittet.com 
*		Company: 	Applied Communications                                    
*   +------------------------------------------------------------------------------+ 
*		Version:		v.1.3
*		Date Modified: 	Feburary 20, 2004
*		Contributor(s):  
*   +------------------------------------------------------------------------------+ 
*    	Changes:
*		Feb 10, 2004 | Names Of files and folders can be used with this
*
*   +------------------------------------------------------------------------------+ 
*/

class Navigation {
  	// class attributes
	var $navigation;
	var $current_page;
	
	// class accesors and mutators
	function SetNavigation($new_value) 	{ $this->navigation = $new_value; }
	function SetCurrent_page($newcontent) 	{ $this->current_page 	= $newcontent; }
	
	// Constructor
	function Navigation(){
		$nav_array = array(  
							'Home' 		=> '/', 
							'Projects' 		=> '/projects/', 
							'Portfolio' 	=> '/portfolio/', 
							'Resume' 		=> '/resume/', 
							'Code Tips' 		=> '/tips/',
							//'Links' 		=> '/links/',
							'Contact' 		=> '/contact/' ) ;
		$this->SetNavigation($nav_array);
		$this->DisplayMenu();
	}



	// Display Admin Navigation Menu
	function DisplayMenu() {
		// Check current page
		if( empty($this->current_page) ){
			$this->current_page = $_SERVER['PHP_SELF'];
		} 
		$array_length  = count($this->navigation);
		for( $i = 0; $i < $array_length; $i++ ){
			$url = current($this->navigation);
			$index = $i + 1;
			$name = key($this->navigation);
			$this -> DisplayButton($name, $url, $index);
			next($this->navigation);
		
		} 
	}
	
	
	function DisplayButton($name, $url, $i) {
		
		$current_page  = ( $this->current_page != $url.'index.php')?'':' id="currentpage"';
		$onfocus = ' onfocus="if(this.blur){this.blur();}"';
		echo('<li'.$current_page.'><a href="'.$url.'" id="nav'.$i.'">'.$name.'</a></li>');
	}
}
?>