<?php
$browser_title = "Services";
ob_start(); ?>
	<h2>Promotional Offer</h2>
	<p>Book a family photo session by November 30, 2010 and receive 25 printed holiday cards free</p>
<?php 
$sidebar = ob_get_contents();
ob_end_clean();

ob_start();
?>
	<h1><?php echo $browser_title ?></h1>
	<p>All sessions have a session fee in order to cover the photographer’s time spent creating life lasting memories with you. All session fees are based on 5 people or less, however bigger groups are welcomed. Please <a href="/contact/">e-mail</a> for a pricing quote.</p>
	<p>Basic Session fee $50 (within Calgary, AB);<br /> $75 (outside of Calgary, AB)<br />
	<span>This covers up to a 1 hour session at one location</span></p>

	<p>Due to the wide variety of session types and individual needs, all packages are customized to fit each individual client. Below are some examples of package types. Please <a href="/contact/">e-mail</a> us with any questions, or for a session quote.</p>

	<div class="package">
		<h3>Maternity &amp; Newborn</h3>
		<span class="price">($400.00)</span>
		<img src="/img/services-maternity-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>2 sessions <br />(up to a maximum of 5 people)</li>
			<li>Photo Editing</li>
			<li>Brag Book including a 4x6 proof set</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>Framed 11x14 Customized Wall </li>
			<li>Panel 3 Occasion Cards, included on Photo Disc</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Family - Basic</h3>
		<span class="price">($275.00)</span>
		<img src="/img/services-family-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 session <br />(up to a maximum of 5 people)</li>
			<li>Photo Editing</li>
			<li>Brag Book including a 4x6 proof set</li>
			<li>Photo Disc of all edited photos, including color changes</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Graduation</h3>
		<span class="price">($850.00)</span>
		<img src="/img/services-graduation-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 four hour Special Occasion Session <br />(up to 4 graduates, dates, and family)</li>
			<li>Photo Editing</li>
			<li>4 Brag Books including 4x6 proof set</li>
			<li>4 Photo Discs of all edited photos, 
			including color changes</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Model/Headshots</h3>
		<span class="price">($425.00)</span>
		<img src="/img/services-model-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>8x10 Portfolio</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Wedding - Basic</h3>
		<span class="price">($1000.00)</span>
		<img src="/img/services-wedding-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 eight hour <br />(full day) Special Occasion Session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>8x10 Portfolio</li>
			<li>Framed 11x14 Customized Wall Panel</li>
			<li>2 Occasion Cards, included on Photo Disc</li>
		</ul>
	</div><!-- /package -->
	<div class="package">
		<h3>Special Event</h3>
		<span class="price">($650.00)</span>
		<img src="/img/services-special-hero.jpg" width="260" height="116" alt="" />
		<ul>
			<li>1 four hour Special Occasion Session</li>
			<li>Photo Editing</li>
			<li>Photo Disc of all edited photos, including color changes</li>
			<li>Framed 11x14 Customized Wall Panel</li>
		</ul>
	</div><!-- /package -->
	
<?php
$content = ob_get_contents();
ob_end_clean();


include_once('../templates/two-column.php');
?>
