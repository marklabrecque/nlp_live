<!doctype html>
<html lang="en">
<head>
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<style type="text/css">
		* { font-family:Roboto; box-sizing:border-box; }
		body { margin:0; padding:0; }
		header { width:100%; border-bottom:1px solid #cecece; padding:0 25px; background:#ddd; height:75px; }
		header h1 { font-size:24px; font-weight:bold; line-height:28px; margin:0; padding:10px 0 0 0; }
		#main { margin:0 auto; margin:30px 50px 0 50px; }
		td { border-right:1px solid #cecece; border-bottom:1px solid #cecece; padding:15px; }
		#no-results { padding:30px; border:1px solid #000; }
	</style>
</head>
<body>
	<header>
		<h1>Email Backups for Nicole Lynn Photography</h1>
	</header>
	<section id="main">
		<?php
		include(dirname(__FILE__) . '/db.php');
		if($results->num_rows == 0) {
			echo '<div id="no-results">No results could be found.</div>';
		}
		else {		
			echo "<table><thead><tr><td>Name</td><td>Email</td><td>Phone</td><td>Requested Date</td><td>Session Type</td><td>Message</td></tr></thead>";			
			while($entry = mysqli_fetch_assoc($results)) {
				echo "<tr>";
					echo "<td>" . $entry['name'] . "</td>";
					echo "<td>" . $entry['email'] . "</td>";
					echo "<td>" . $entry['phone'] . "</td>";
					echo "<td>" . $entry['request_date'] . "</td>";
					echo "<td>" . $entry['session_type'] . "</td>";
					echo "<td>" . $entry['message'] . "</td>";				
				echo "</tr>";
			}
		}
		?>
	</section>
	<footer>

	</footer>
</body>
</html>