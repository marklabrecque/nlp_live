<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | Welcome</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('#container .scrollable').scrollable({
				circular: true,
				next: '',
				prev: ''
			}).autoscroll(4000).navigator();
			$('#testimonials .scrollable').scrollable({
				circular: true,
			});
			$('#testimonials .arrows a').click(function() {
				return false;
			})
			var width = $(window).width();
			$('.mask.left').width((width - 900) / 2);
		});
	</script>
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
	<style type="text/css">
		* { font-family:'Raleway', sans-serif; }
		body { background:url(img/wood-bg.jpg); }
		header { width:100%; height:130px; background:#fff; border-bottom:1px solid #444; }
		header .wrapper { width:1049px; margin:0 auto; }
		header .title { float:left; width:500px; margin:15px 0 0 0; color:#444; }
		header .title .logo { float:left; width:100px; margin-right:20px;}
		header .header { float:left; width:340px; font-size:20px; margin:27px 0 0 0; padding:0; text-decoration:none; color:#c0e1d6; font-family:Arial, sans-serif; text-transform:uppercase; font-weight:normal; }
		header .sub-header { float:left; font-family:Arial, sans-serif; color:#713d25; font-weight:normal; }
		header h2 { float:left; width:180px; margin:0; padding:0; }
		header h1 a { text-decoration:none; color:#713d25; font-family:Arial, sans-serif; }
		#nav { width:536px; float:right; margin:50px 0 0 0; padding:0; }
		#nav li { list-style-type:none; float:left; margin-right:10px; }
		#nav li a { text-decoration:none; color:#444; padding:3px 7px; }
		#nav li a.active { border:1px solid #713d25; }
		#nav li a.active:hover { border:1px solid #c0e1d6; }
		#nav li a:hover { color:#fff; background:#c0e1d6; }
		#container { width:900px; margin:0 auto; }
		#container .content { margin-top:100px; }
		#container .content .flourish { margin-left:-125px; }
		.scrollable { position:relative; width:900px; height:600px; overflow:hidden; margin:-45px 0 0 14px; }
		.scrollable .items { position:absolute; width:20000em; }
		.scrollable .items div { float:left; width:900px; }
		.scrollable .items img { max-width:100%; height:auto; }
		#container .navi { width:80px; margin:0 auto; margin-top:10px; }
		#container .navi a { float:left; display:block; width:10px; height:10px; background:#713d25; margin:0 2px; cursor:pointer; border:1px solid #713d25; }
		#container .navi a.active { background:#fff; }
		#container .content .column h3 { padding-top:110px; font-size:24px; font-weight:normal; }
		#testimonials { float:left; width:100%; height:270px; margin-top:50px; background:#eee; }
		#testimonials .inner { position:relative; width:900px; margin:0 auto; }
		#testimonials .scrollable { width:400px; margin-top:0; overflow:hidden; }
		#testimonials .scrollable .items div { width:400px; margin-right:20px; }
		#testimonials .scrollable .items div p { padding-bottom:0; }
		#testimonials .scrollable .items div .client { width:100%; font-family:"Dancing Script", cursive; margin:0; padding:0; text-align:right; }
		#testimonials .inner .arrows { position:absolute; left:0; top:0; height:270px; width:400px; }
		#testimonials .inner .arrows .browse { position:absolute; display:block; width:25px; height:25px; top:50%; margin-top:-12px; color:#444; text-decoration:none; }
		#testimonials .inner .arrows .browse.left { left:-30px; }
		#testimonials .inner .arrows .browse.right { right:-50px; }
		footer { width:100%; height:100px; }
	</style>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="#" class="active">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="/weddings/gallery">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/weddings/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div class="content">
			<div class="scrollable">
				<div class="items">
					<div><img src="/weddings/img/homepage-features/001.jpg" alt="Wedding dress" /></div>
					<div><img src="/weddings/img/homepage-features/002.jpg" alt="Wedding hands" /></div>
					<div><img src="/weddings/img/homepage-features/003.jpg" alt="Wedding couple" /></div>
					<div><img src="/weddings/img/homepage-features/004.jpg" alt="Wedding couple" /></div>
					<div><img src="/weddings/img/homepage-features/005.jpg" alt="Wedding couple" /></div>
				</div>
			</div>
			<div class="navi"></div>
		</div>
	</div>
	<footer>

	</footer>
</body>
</html>
