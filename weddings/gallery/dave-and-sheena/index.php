<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | Gallery</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="/weddings/gallery/galleries.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script src="/weddings/gallery/functions.js" type="text/javascript"></script>
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="/weddings/gallery" class="active">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div id="gallery">
			<div class="top">
				<div class="dropdown">
					<span class="current">Dave &amp; Sheena</span>
					<i class="icon-angle-down"></i>
					<ul class="options">
						<li><a href="../aaron-and-melissa">Aaron &amp; Melissa</a></li>
						<li><a href="../ashley-and-andrew">Ashley &amp; Andrew</a></li>
						<li><a href="../curt-and-natalie">Curt &amp; Natalie</a></li>
						<li><a href="../dan-and-isobel">Dan &amp; Isobel</a></li>
						<li><a class="active" href="#">Dave &amp; Sheena</a></li>
						<li><a href="../isobel-and-dan">Isobel &amp; Dan</a></li>
						<li><a href="../jana-lyn-and-wayne">Jana-Lyn &amp; Wayne</a></li>
						<li><a href="../jen-and-nathan">Jen &amp; Nathan</a></li>
						<li><a href="../jen-and-trevor">Jen &amp; Trevor</a></li>
						<li><a href="../jess-and-andy">Jess &amp; Andy</a></li>
						<li><a href="../lisa-and-dennis">Lisa &amp; Dennis</a></li>
						<li class="last"><a href="../trevor-and-jen">Trevor &amp; Jen</a></li>
					</ul>
				</div>
				<a href="#" id="switch">Slideshow: <span>Off</span></a>
			</div>
			<div id="no-slideshow">
				<img id="jumbotron" src="/weddings/img/galleries/dave-and-sheena/1.jpg" />
				<div class="scrollable">
					<div class="items">
						<div>
							<img class="active" src="/weddings/img/galleries/dave-and-sheena/thumbs/1.jpg" />
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/2.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/3.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/4.jpg" />
							<img class="portrait last" src="/weddings/img/galleries/dave-and-sheena/thumbs/5.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/6.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/7.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/8.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/9.jpg" />
							<img class="last" src="/weddings/img/galleries/dave-and-sheena/thumbs/10.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/11.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/12.jpg" />
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/13.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/14.jpg" />
							<img class="last" src="/weddings/img/galleries/dave-and-sheena/thumbs/15.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/16.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/17.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/18.jpg" />
							<img class="portrait" src="/weddings/img/galleries/dave-and-sheena/thumbs/19.jpg" />
							<img src="/weddings/img/galleries/dave-and-sheena/thumbs/20.jpg" />
						</div>
					</div>
				</div>
				<div class="arrows">
					<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
					<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
				</div>
			</div>
			<div id="slideshow" class="active">
				<div class="scrollable">
					<div class="items">
						<div><img src="/weddings/img/galleries/dave-and-sheena/1.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/2.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/3.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/4.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/5.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/6.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/7.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/8.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/9.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/10.jpg" /></div>
						<div><img  class="portrait" src="/weddings/img/galleries/dave-and-sheena/11.jpg" /></div>
						<div><img src="/weddings/img/galleries/dave-and-sheena/12.jpg" /></div>
                        <div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/13.jpg" /></div>
                        <div><img src="/weddings/img/galleries/dave-and-sheena/14.jpg" /></div>
                        <div><img src="/weddings/img/galleries/dave-and-sheena/15.jpg" /></div>
                        <div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/16.jpg" /></div>
                        <div><img src="/weddings/img/galleries/dave-and-sheena/17.jpg" /></div>
                        <div><img src="/weddings/img/galleries/dave-and-sheena/18.jpg" /></div>
                        <div><img class="portrait" src="/weddings/img/galleries/dave-and-sheena/19.jpg" /></div>
                        <div><img src="/weddings/img/galleries/dave-and-sheena/20.jpg" /></div>
					</div>
				</div>
				<div class="arrows">
					<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
					<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
