$(document).ready(function() {
	$('#no-slideshow .scrollable').scrollable({ circular: true });
	$('#slideshow .scrollable').scrollable({ circular: true, }).autoscroll({
		'interval': 5000,
		'autopause' : false
	});
	$('.scrollable .items img').click(function() {
		if ($(this).hasClass("active")) { return; }
		var thumbFilename = $(this).attr('src');
		console.log(thumbFilename);
		var jumboFilename = thumbFilename.replace("thumbs/", "");
		var portraitOrientation = false;
		if($(this).hasClass('portrait')) {
			portraitOrientation = true;
		}
		$('#jumbotron').fadeTo('fast', 0, function() {
			if(portraitOrientation) { $('#jumbotron').addClass('portrait'); }
			else { $('#jumbotron.portrait').removeClass('portrait'); }
			$('#jumbotron').attr('src', jumboFilename);
			$('#jumbotron').fadeTo('fast', 1);
			$('.items img.active').removeClass('active');
			$(this).addClass('active');
		});
	});
	$('#container .dropdown .options li a.active').click(function() {
		//$('.dropdown .options').hide();
		return false;
	});
	$('#masonry').masonry({
		'gutter' : 25,
		'columnWidth' : 250
	});
	$('.dropdown').click(function() {
		$('.dropdown .options').show();
	});
	$('.arrows .browse').click(function() { return false; });
	$('#switch').click(function() {
		$('#no-slideshow').fadeOut('fast');
		if($('#no-slideshow').hasClass('active')) {
			$('#no-slideshow').fadeOut('fast', function() {
				$('#slideshow').fadeIn('slow').addClass('active');
			}).removeClass('active');
			$('#switch span').html('On');
		} else {
			$('#slideshow').fadeOut('fast', function() {
				$('#no-slideshow').fadeIn('slow').addClass('active');
			}).removeClass('active');
			$('#switch span').html('Off');
		}
		$('#slideshow').toggle();
		$('#no-slideshow').toggle();
		return false;
	});
});		