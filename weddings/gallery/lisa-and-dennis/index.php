<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | Gallery</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="/weddings/gallery/galleries.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script src="/weddings/gallery/functions.js" type="text/javascript"></script>
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="/weddings/gallery" class="active">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div id="gallery">
			<div class="top">
				<div class="dropdown">
					<span class="current">Lisa &amp; Dennis</span>
					<i class="icon-angle-down"></i>
					<ul class="options">
						<li><a href="../aaron-and-melissa">Aaron &amp; Melissa</a></li>
						<li><a href="../ashley-and-andrew">Ashley &amp; Andrew</a></li>
						<li><a href="../curt-and-natalie">Curt &amp; Natalie</a></li>
						<li><a href="../dan-and-isobel">Dan &amp; Isobel</a></li>
						<li><a href="../dave-and-sheena">Dave &amp; Sheena</a></li>
						<li><a href="../isobel-and-dan">Isobel &amp; Dan</a></li>
						<li><a href="../jana-lyn-and-wayne">Jana-Lyn &amp; Wayne</a></li>
						<li><a href="../jen-and-nathan">Jen &amp; Nathan</a></li>
						<li><a href="../jen-and-trevor">Jen &amp; Trevor</a></li>
						<li><a href="../jess-and-andy">Jess &amp; Andy</a></li>
						<li><a class="active" href="#">Lisa &amp; Dennis</a></li>
						<li class="last"><a href="../trevor-and-jen">Trevor &amp; Jen</a></li>
					</ul>
				</div>
				<a href="#" id="switch">Slideshow: <span>Off</span></a>
			</div>
			<div id="no-slideshow" class="active">
				<img id="jumbotron" src="/weddings/img/galleries/lisa-and-dennis/2.jpg" />
				<div class="scrollable">
					<div class="items">
						<div>
							<img class="active" src="/weddings/img/galleries/lisa-and-dennis/thumbs/2.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/3.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/4.jpg" />
							<img class="portrait last" src="/weddings/img/galleries/lisa-and-dennis/thumbs/5.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/6.jpg" />
						</div>
						<div>
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/7.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/8.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/9.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/10.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/11.jpg" />
						</div>
						<div>
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/12.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/13.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/14.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/15.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/16.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/17.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/18.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/19.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/20.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/21.jpg" />
						</div>
						<div>
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/22.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/23.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/24.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/25.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/26.jpg" />
						</div>
						<div>
							<img class="portrait" width="156" src="/weddings/img/galleries/lisa-and-dennis/thumbs/27.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/28.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/29.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/30.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/31.jpg" />
						</div>
						<div>
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/32.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/33.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/34.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/35.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/36.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/38.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/39.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/40.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/41.jpg" />
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/42.jpg" />
						</div>
						<div>
							<img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/thumbs/43.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/44.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/45.jpg" />
							<img src="/weddings/img/galleries/lisa-and-dennis/thumbs/1.jpg" />
						</div>
					</div>
				</div>
				<div class="arrows">
					<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
					<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
				</div>
			</div>
			<div id="slideshow" class="active">
				<div class="scrollable">
					<div class="items">
						<div><img src="/weddings/img/galleries/lisa-and-dennis/2.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/3.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/4.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/5.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/6.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/7.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/8.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/9.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/10.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/11.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/12.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/13.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/14.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/15.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/16.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/17.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/18.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/19.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/20.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/21.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/22.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/23.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/24.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/25.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/26.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/27.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/28.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/29.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/30.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/31.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/32.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/33.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/34.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/35.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/36.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/37.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/38.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/39.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/40.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/41.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/42.jpg" /></div>
						<div><img class="portrait" src="/weddings/img/galleries/lisa-and-dennis/43.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/44.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/45.jpg" /></div>
						<div><img src="/weddings/img/galleries/lisa-and-dennis/1.jpg" /></div>
					</div>
				</div>
				<div class="arrows">
					<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
					<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
