<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | Gallery</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('.scrollable').scrollable({
				circular: true
			});
			$('.scrollable .items img').click(function() {
				if ($(this).hasClass("active")) { return; }
				var thumbFilename = $(this).attr('src');
				var jumboFilename = thumbFilename.replace("_thumb", "");
				var portraitOrientation = false;
				if($(this).hasClass('portrait')) {
					portraitOrientation = true;
				}
				$('#jumbotron').fadeTo('fast', 0, function() {
					if(portraitOrientation) { $('#jumbotron').addClass('portrait'); }
					else { $('#jumbotron.portrait').removeClass('portrait'); }
					$('#jumbotron').attr('src', jumboFilename);
					$('#jumbotron').fadeTo('fast', 1);
					$('.items img.active').removeClass('active');
					$(this).addClass('active');
				});
			});
			$('#masonry').masonry({
				'gutter' : 25,
				'columnWidth' : 250
			});
			$('.dropdown').click(function() {
				$('.dropdown .options').show();
				return false;
			});
			$('.dropdown .options li a').click(function() {
				var selection = $(this).html();
				$('.dropdown .current').html(selection);
				$('.dropdown .options').hide();
				$('.content .section').hide();
				selection = selection.toLowerCase();
				selection = selection.replace(' ', '_');
				$('.content .section#' + selection).show();
				return false;
			});
			$('.arrows .browse').click(function() { return false; });
		});
	</script>
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
	<style type="text/css">
		* { font-family:'Raleway', sans-serif; }
		body { background:url(../img/wood-bg.jpg); }
		header { width:100%; height:130px; background:#fff; border-bottom:1px solid #444; }
		header .wrapper { width:1049px; margin:0 auto; }
		header .title { float:left; width:500px; margin:15px 0 0 0; color:#444; }
		header .title .logo { float:left; width:100px; margin-right:20px;}
		header .header { float:left; width:340px; font-size:20px; margin:27px 0 0 0; padding:0; text-decoration:none; color:#c0e1d6; font-family:Arial, sans-serif; text-transform:uppercase; font-weight:normal; }
		header .sub-header { float:left; font-family:Arial, sans-serif; color:#713d25; font-weight:normal; }
		header h2 { float:left; width:180px; margin:0; padding:0; }
		header h1 a { text-decoration:none; color:#713d25; font-family:Arial, sans-serif; }
		#nav { width:536px; float:right; margin:50px 0 0 0; padding:0; }
		#nav li { list-style-type:none; float:left; margin-right:10px; }
		#nav li a { text-decoration:none; color:#444; padding:3px 7px; }
		#nav li a.active { border:1px solid #713d25; }
		#nav li a.active:hover { border:1px solid #c0e1d6; }
		#nav li a:hover { color:#fff; background:#c0e1d6; }
		#container { width:1049px; margin:0 auto; padding-top:50px; }
		#container .dropdown { position:relative; width:200px; padding: 12px 25px; background:#fff; border:1px solid #444; margin:0 0 25px 0; }
		#container .dropdown .current { color:#713d25; cursor:pointer; }
		#container .dropdown i { position:absolute; right:25px; color:#713d25; margin-top:2px; }
		#container .dropdown .options { display:none; padding:12px 25px; width:200px; position:absolute; top:-17px; left:-1px; background:#fff; border:1px solid #000; }
		#container .dropdown .options li { background:none; padding-left:0; margin-left:0; margin-bottom:5px; list-style-type:none; }
		#container .dropdown .options li.last { margin-bottom:0; }
		#container .dropdown .options li a { text-decoration:none; color:#713d25; }
		#gallery { margin:0 auto; padding:0; width:865px; }
		#gallery #jumbotron { margin:0 auto; max-height:533px; }
		#gallery #jumbotron.portrait { margin-left:222px; }
		#gallery .scrollable { position:relative; width:798px; height:105px; overflow:hidden; }
		#gallery .scrollable .items { position:absolute; width:20000em; }
		#gallery .scrollable .items > div { float:left; width:800px; padding-left:2px; }
		#gallery .scrollable .items div img { cursor:pointer; }
		#gallery .arrows { position:relative; width:100%; height:25px; }
		#gallery a.browse { position:absolute; display:block; width:15px; height:15px; text-decoration:none; top:-60px; }
		#gallery a.browse.disabled { display:none; }
		#gallery a.browse.prev { left:-25px; }
		#gallery a.browse.next { right:-25px; }
		#gallery a.browse i { font-size:15px; color:#444; }
		#gallery .back-link { float:right; display:block; font-size:15px; margin-right:20px; }
		#gallery .back-link a { text-decoration:none; color:#000; }
		#masonry { padding:30px; }
		#masonry .box { width:250px; height:250px; margin-bottom:25px; }
		#masonry .box .cover { width:250px; height:auto; }
		#masonry .box a { display:block; position:relative; width:100%; height:100%; color:#fff; }
		#masonry .box a img { max-width:250px; max-height:250px; }
		#masonry .box a .mask { display:none; position:absolute; top:0; left:0; width:100%; height:250px; background:rgba(0,0,0,0.4); text-align:center; }
		#masonry .box a:hover .mask { display:block; }
		#masonry .box a .mask .label { display:block; margin-top:120px; }

	</style>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="#" class="active">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div id="gallery">
			<h1>Gallery<span class="back-link"><a href="/gallery/">To Portraits Gallery</a></span></h1>
			<div id="masonry">
				<div class="box">
					<a href="jess-and-andy/">
						<img src="../img/jess-and-andy-cover.jpg" />
						<span class="mask"><span class="label">Jess &amp; Andy</span></span>
					</a>
				</div>
				<div class="box">
					<a href="lisa-and-dennis/">
						<img src="../img/lisa-and-dennis-cover.jpg" />
						<span class="mask"><span class="label">Lisa &amp; Dennis</span></span>
					</a>
				</div>
				<div class="box">
					<a href="jen-and-nathan/">
						<img src="../img/jen-and-nathan-cover.jpg" />
						<span class="mask"><span class="label">Jen &amp; Nathan</span></span>
					</a>
				</div>
				<div class="box">
					<a href="jen-and-trevor/">
						<img src="../img/jen-and-trevor-cover.jpg" />
						<span class="mask"><span class="label">Jen &amp; Trevor</span></span>
					</a>
				</div>
				<div class="box">
					<a href="isobel-and-dan">
						<img src="../img/isobel-and-dan-cover.jpg" />
						<span class="mask"><span class="label">Isobel &amp; Dan</span></span>
					</a>
				</div>
				<div class="box">
					<a href="aaron-and-melissa/">
						<img class="cover" src="../img/aaron-and-melissa-cover.jpg" />
						<span class="mask"><span class="label">Aaron &amp; Melissa</span></span>
					</a>
				</div>
				<div class="box">
					<a href="ashley-and-andrew/">
						<img src="../img/ashley-and-andrew-cover.jpg" />
						<span class="mask"><span class="label">Ashley &amp; Andrew</span></span>
					</a>
				</div>
				<div class="box">
					<a href="curt-and-natalie/">
						<img class="cover" src="../img/curt-and-natalie-cover.jpg" />
						<span class="mask"><span class="label">Curt &amp; Natalie</span></span>
					</a>
				</div>
				<div class="box">
					<a href="dan-and-isobel/">
						<img src="../img/dan-and-isobel-cover.jpg" />
						<span class="mask"><span class="label">Dan &amp; Isobel</span></span>
					</a>
				</div>
				<div class="box">
					<a href="trevor-and-jen/">
						<img src="../img/trevor-and-jen-cover.jpg" />
						<span class="mask"><span class="label">Trevor &amp; Jen</span></span>
					</a>
				</div>
				<div class="box">
					<a href="jana-lyn-and-wayne/">
						<img src="../img/jana-lyn-and-wayne-cover.jpg" />
						<span class="mask"><span class="label">Jana Lyn &amp; Wayne</span></span>
					</a>
				</div>
				<div class="box">
					<a href="dave-and-sheena/">
						<img src="../img/dave-and-sheena-cover.jpg" />
						<span class="mask"><span class="label">Dave &amp; Sheena</span></span>
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
