<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | About</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('.dropdown').click(function() {
				$('.dropdown .options').show();
				return false;
			});
			$('.dropdown .options li a').click(function() {
				var selection = $(this).html();
				$('.dropdown .current').html(selection);
				$('.dropdown .options').hide();
				$('.content .section').hide();
				selection = selection.toLowerCase();
				selection = selection.replace(" ", "-");
				console.log(selection);
				$('.content .section#' + selection).show();
				return false;
			});
		});
	</script>
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
	<style type="text/css">
		* { font-family:'Raleway', sans-serif; }
		body { background:url(../img/wood-bg.jpg); }
		header { width:100%; height:130px; background:#fff; border-bottom:1px solid #444; }
		header .wrapper { width:1049px; margin:0 auto; }
		header .title { float:left; width:500px; margin:15px 0 0 0; color:#444; }
		header .title .logo { float:left; width:100px; margin-right:20px;}
		header .header { float:left; width:340px; font-size:20px; margin:27px 0 0 0; padding:0; text-decoration:none; color:#c0e1d6; font-family:Arial, sans-serif; text-transform:uppercase; font-weight:normal; }
		header .sub-header { float:left; font-family:Arial, sans-serif; color:#713d25; font-weight:normal; }
		header h2 { float:left; width:180px; margin:0; padding:0; }
		header h1 a { text-decoration:none; color:#713d25; font-family:Arial, sans-serif; }
		#nav { width:536px; float:right; margin:50px 0 0 0; padding:0; }
		#nav li { list-style-type:none; float:left; margin-right:10px; }
		#nav li a { text-decoration:none; color:#444; padding:3px 7px; }
		#nav li a.active { border:1px solid #713d25; }
		#nav li a.active:hover { border:1px solid #c0e1d6; }
		#nav li a:hover { color:#fff; background:#c0e1d6; }
		#container { width:1049px; margin:0 auto; padding-top:50px; }
		#container .dropdown { position:relative; width:200px; padding: 12px 25px; background:#fff; border:1px solid #444; margin:0 0 25px 0; }
		#container .dropdown .current { color:#713d25; cursor:pointer; }
		#container .dropdown i { position:absolute; right:25px; color:#713d25; margin-top:2px; }
		#container .dropdown .options { display:none; padding:12px 25px; width:200px; position:absolute; top:-17px; left:-1px; background:#fff; border:1px solid #444; }
		#container .dropdown .options li { background:none; padding-left:0; margin-left:0; margin-bottom:5px; list-style-type:none; }
		#container .dropdown .options li.last { margin-bottom:0; }
		#container .dropdown .options li a { text-decoration:none; color:#713d25; }
		#container .dropdown .options li a:hover { color:#c0e1d6; }
		#container .content	.section { display:none; }
		#container .content	.section h1 { margin-bottom:30px; }
		#container .content	.section p { float:left; width:500px; margin-right:50px; color:#444; }
		#container .content .section img { float:left; margin-left:50px; }
		#container .content #company { float:left; width:949px; background:#fff; border:1px solid #444; padding:50px; }
		#container .content #our-team { float:left; width:949px; background:#fff; border:1px solid #444; padding:50px; }
		#container .content #company h1 { margin-top:0; }
		#container .content #our-team h1 { margin-top:0; }
		#container .content #company .body { padding-left:80px; }
		#container .content #company .body img { margin-top:20px; }
		#container .content .section .photographer { float:left; padding:0 0 50px 80px; border-top:1px solid #444; }
		#container .content .section .photographer h3 { float:left; width:480px; margin-top:50px; }
		#container .content .section .photo { margin-top:-40px; width:225px; height:auto; }
		#container .content .section .photographer.top { border-top:0; }
	</style>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="#" class="active">About</a></li>
				<li><a href="/weddings/gallery">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="http://nicolelynn.com/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div class="dropdown">
			<span class="current">Company</span>
			<i class="icon-angle-down"></i>
			<ul class="options">
				<li><a href="#">Company</a></li>
				<li class="last"><a href="#">Our Team</a></li>
			</ul>
		</div>
		<div class="content">
			<div id="company" class="section" style="display:block">
				<h1>About Nicole-Lynn Photography</h1>
				<div class="body">
					<p>Nicole-Lynn Photography is a Calgary based company with a focus on capturing the special memories of clients. Opening in 2010, the company and its photographers have continued to grow. Our range of experience includes everything from weddings to portraits, newborn &amp; maternity, sports, special event and commercial photography. Through consultations the photographers build a relationship with all of their clients. All of our processes from our first conversation and the photo shoot to the finished products we take pride in our quality and professionalism. Everyone is dedicated to ensuring that clients&rsquo; expectations can be surpassed and that their cherished moments will live on through the ages.</p>
					<img class="photo" src="/img/about-company-resized.jpg" alt="An example photo taken by Nicole-Lynn Photography" />
				</div>
			</div>
			<div id="our-team" class="section">
				<h1>About The Photographers</h1>
				<div class="photographer top">
					<h3>Nicole Elliott</h3>
					<p>Nicole Elliott is the head photographer and Owner of Nicole-Lynn Photography. She has a passion for capturing all of life&rsquo;s treasured memories on film and strives to see life through the eyes of her clients. Nicole&rsquo;s passion for photography has taken her from inside the studio into the lives of her clients, allowing her to develop an eye for creating unique and innovative portraits in her clients personal settings. With over 8 years of experience both in studio and on location photography, she brings a lot of creativity and knowledge to every session. Now completing a CMA designation, her strong business sense allows for her to contribute to the professionalism and structure of the company. Through every aspect of her life she loves to be surrounded by people and being inspired by the people she meets.</p>
					<img class="photo" src="/img/nicole-v2.jpg" alt="A picture of Nicole-Lynn Elliott" />
				</div>
				<div class="photographer">
					<h3>Becky Elliott</h3>
					<p>Becky Elliott is a photographer with Nicole-Lynn Photography. She has an eye for details and capturing candid moments. Becky&rsquo;s love of photography has blossomed over 28 years. She has always loved photography, being able to document the lives of family and friends, favorite places and special things in her garden. So many moments are fleeting and wonderful when captured by the camera. Through a transition from film to digital she combines all of her expertise to create beautiful portraits. Photography has always been a passion for Becky. Aside from photography, Becky has been a Registered Nurse for 28 years. Currently Becky also works as a Patient Care Manager at the Rocky View General Hospital.</p>
					<img class="photo" src="/img/becky-v2.jpg" alt="A picture of Becky" />
				</div>
				<div class="photographer">
					<h3>Kate Mallabone</h3>
					<p>Kate Mallabone is the Sales and Social Media Director for Nicole-Lynn Photography. From personal experience with Nicole-Lynn Photography as a client, Kate brings an exciting element of first-hand experience to the team. Kate&rsquo;s passion for people and children is incredible. Recently completing a Bachelor of Education with a minor in Early Childhood, she enjoys using her skills to assist with sessions and viewing meetings.</p>
					<img class="photo" src="/img/kate.jpg" alt="A picture of Kate" />
				</div>
			</div>
		</div>
	</div>
	<footer>

	</footer>
</body>
</html>
