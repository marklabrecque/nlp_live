<!doctype html>
<html lang="en">
<head>
	<title>Weddings by Nicole-Lynn Photography | Info</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>	
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<script type="text/javascript">
		$(document).ready(function() {
			$('.scrollable').scrollable({
				circular: true
			});
			$('.scrollable .items img').click(function() {
				if ($(this).hasClass("active")) { return; }
				var thumbFilename = $(this).attr('src');
				var jumboFilename = thumbFilename.replace("_thumb", "");
				var portraitOrientation = false;
				if($(this).hasClass('portrait')) {
					portraitOrientation = true;
				}
				$('#jumbotron').fadeTo('fast', 0, function() {
					if(portraitOrientation) { $('#jumbotron').addClass('portrait'); }
					else { $('#jumbotron.portrait').removeClass('portrait'); }
					$('#jumbotron').attr('src', jumboFilename);
					$('#jumbotron').fadeTo('fast', 1);
					$('.items img.active').removeClass('active');
					$(this).addClass('active');
				});
			});
			$('.dropdown').click(function() {
				$('.dropdown .options').show();
				return false;
			});
			$('.dropdown .options li a').click(function() {				
				var selection = $(this).html();
				$('.dropdown .current').html(selection);
				$('.dropdown .options').hide();
				$('.content .section').hide();
				selection = selection.toLowerCase();
				selection = selection.replace(' &amp; ', '-and-');
				selection = selection.replace(' ', '-');
				$('.content #' + selection).show();
				console.log(selection)
				return false;
			});
			$('.arrows .browse').click(function() { return false; });
			$('#masonry').masonry({
				columnWidth: 400,
				itemSelector: '.box',
				gutter: 25
			});
		});
	</script>
	<style type="text/css">
		* { font-family:'Raleway', sans-serif; }
		body { background:url(../img/wood-bg.jpg); }
		header { width:100%; height:130px; background:#fff; border-bottom:1px solid #444; }
		header .wrapper { width:1049px; margin:0 auto; }
		header .title { float:left; width:500px; margin:15px 0 0 0; color:#444; }
		header .title .logo { float:left; width:100px; margin-right:20px;}
		header .header { float:left; width:340px; font-size:20px; margin:27px 0 0 0; padding:0; text-decoration:none; color:#c0e1d6; font-family:Arial, sans-serif; text-transform:uppercase; font-weight:normal; }
		header .sub-header { float:left; font-family:Arial, sans-serif; color:#713d25; font-weight:normal; }
		header h2 { float:left; width:180px; margin:0; padding:0; }
		header h1 a { text-decoration:none; color:#713d25; font-family:Arial, sans-serif; }
		#nav { width:536px; float:right; margin:50px 0 0 0; padding:0; }
		#nav li { list-style-type:none; float:left; margin-right:10px; }
		#nav li a { text-decoration:none; color:#444; padding:3px 7px; }
		#nav li a.active { border:1px solid #713d25; }
		#nav li a.active:hover { border:1px solid #c0e1d6; }
		#nav li a:hover { color:#fff; background:#c0e1d6; }
		#container { width:1049px; margin:0 auto; padding-top:50px; }
		#container .dropdown { position:relative; width:250px; padding: 12px 25px; background:#fff; border:1px solid #000; margin:0 0 25px 0; }
		#container .dropdown .current { color:#713d25; cursor:pointer; }
		#container .dropdown i { position:absolute; right:25px; color:#713d25; margin-top:2px; }
		#container .dropdown .options { display:none; padding:12px 25px; width:250px; position:absolute; top:-17px; left:-1px; background:#fff; border:1px solid #444; }
		#container .dropdown .options li { background:none; padding-left:0; margin-left:0; margin-bottom:5px; list-style-type:none; }
		#container .dropdown .options li.last { margin-bottom:0; }
		#container .dropdown .options li a { text-decoration:none; color:#713d25; }
		#container .dropdown .options li a:hover { color:#c0e1d6; }
		#container .content #information { float:left; width:949px; background:#fff; border:1px solid #444; padding:50px; }
		#container .content #faq { display:none; float:left; width:949px; background:#fff; border:1px solid #444; padding:50px; }
		#container .content #publications-and-testimonials { display:none; float:left; width:100%; background:#fff; border:1px solid #444; padding:50px; }
		#container .content #publications-and-testimonials .testimonial { float:left; margin-bottom:25px; }
		#container .content #publications-and-testimonials .testimonial .author { display:block; float:right; }
		#container #masonry { width:100%; }
		#container #masonry .box { width:400px; margin-bottom:25px; border-bottom:1px solid #713d25; padding-top:25px;  }
		#container #masonry .box h3 { float:left; margin:0; padding:0 0 15px 0; }
		/*#container #masonry .box.left h3 { float:left; }*/
		/*#container #masonry .box.right h3 { float:right; }*/
		#container #masonry .box.left img { float:left; margin:0 15px 0 0; }
		#container #masonry .box.right img { float:right; margin:0 0 0 15px; }
		#container #masonry .box p { padding:0; margin:0 0 1em 0; }
		#container #masonry .box ul { margin:15px 0 1em 0; }
		/*#container #masonry .box ul.nudge { margin-top:15px; } */
		#container #masonry .box.left p { float:left; }
		#container #masonry .box .condensed { width:200px; }
		#container #masonry .box.left ul { float:left; }
		#container #masonry .box.right p { float:left; }
		#container #masonry .box.right ul { float:right; }
	</style>
</head>
<body>
	<?php $big_image = "150x150"; ?>
	<?php $small_image = "200x200"; ?>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
				<img class="logo" src="/img/landing-logo-2.png" />
				<span class="header">Weddings</span>
				<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="/weddings/gallery">Gallery</a></li>
				<li><a href="#" class="active">Info</a></li>
				<li><a href="/weddings/contact">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div class="dropdown">
			<span class="current">Information</span>
			<i class="icon-angle-down"></i>
			<ul class="options">
				<li><a class="info" href="#">Information</a></li>
				<li><a class="pub-test" href="#">Publications &amp; Testimonials</a></li>
				<li class="last"><a class="faq" href="#">FAQ</a></li>				
			</ul>
		</div>
		<div class="content">
			<div id="information" class="section">
				<div class="intro">
					<h3>Pricing</h3>
					<p>Weddings are charged based on individual couples. We have a wide range of services and feel that custom quotes are the best way for a couple to choose exactly what they want from their wedding photography. This includes the coverage required as well as the final products. All of our sessions include a complimentary engagement session &amp; 2 photographers for your wedding day. Both of these do not cost extra, but we feel are necessary to make your wedding photography the best that it can be.</p>
					<p>Wedding packages start at $1800. Some sample packages are below.</p>
				</div>
				<div id="masonry">
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Bronze Package</h3>
						<p class="condensed">The following elements from the engagement session are included in the Bronze Package:</p>
						<ul>
							<li>2 8x10 prints from the engagement session</li>
							<li>4 5x7 prints from the engagement session</li>
						</ul>
						<p>The following elements from the wedding photos are included in the Bronze Package:</p>
						<ul>
							<li>Online gallery of wedding photos that family and friends can also sign into to view the photos from the wedding and order any of them directly from Nicole-Lynn Photography</li>
							<li>$50 Print Credit from wedding photos</li>
							<li>20 page Wedding Album in an 11x14 size</li>
						</ul>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Silver Package</h3>
						<p class="condensed">The following elements from the engagement session are included in the Silver Package:</p><br />
						<ul>
							<li>2 8x10 prints from the engagement session</li>
							<li>4 5x7 prints from the engagement session</li>
							<li>Edited disc of photos from the engagement session</li>
						</ul>
						<p>The following elements from the wedding photos are included in the Silver Package</p>
						<ul>
							<li>Online gallery of wedding photos that family and friends can also sign into to view the photos from the wedding and order any of them directly from Nicole-Lynn Photography</li>
							<li>$50 Print Credit from wedding photos</li>
							<li>20 page Wedding Album in an 11x14 size</li>
							<li>Edited disc of photos from the wedding</li>
						</ul>
					</div>
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Diamond Package</h3>
						<p class="condensed">The following elements from the engagement session are included in the Platinum Package:</p>
						<br /><br />
						<ul>
							<li>2 8x10 prints from the engagement session</li>
							<li>4 5x7 prints from the engagement session</li>
							<li>Edited disc of photos from the engagement session</li>
							<li>20 pages Guest Signing Book made with engagement photos and personalization's</li>
						</ul>
						<p>The following elements from the wedding photos are included in the Platinum Package:</p>
						<ul>
							<li>Online gallery of wedding photos that family and friends can also sign into to view the photos from the wedding and order any of them directly from Nicole-Lynn Photography</li>
							<li>$50 Print Credit from wedding photos</li>
							<li>20 page Wedding Album in an 11x14 size</li>
							<li>Edited disc of photos from the wedding</li>
						</ul>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Platinum Package</h3>
						<p class="condensed">The following elements from the engagement session are included in the Diamond Package:</p>
						<ul>
							<li>2 8x10 prints from the engagement session</li>
							<li>4 5x7 prints from the engagement session</li>
							<li>Edited disc of photos from the engagement session</li>
							<li>20 pages Guest Signing Book made with engagement photos and personalization's</li>
						</ul>
						<p>The following elements from the wedding photos are included in the Diamond Package:</p>
						<ul>
							<li>Online gallery of wedding photos that family and friends can also sign into to view the photos from the wedding and order any of them directly from Nicole-Lynn Photography</li>
							<li>$100 Print Credit from wedding photos</li>
							<li>20 page Wedding Album in an 11x14 size</li>
							<li>2 Parent Albums of the Wedding Album. These are great gifts for the parents of the Bride and Groom and are smaller versions of the Wedding Album created for the couple.</li>
							<li>11x14 Custom Frame</li>
							<li>Edited disc of photos from the wedding</li>
						</ul>
					</div>
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<p class="condensed">Our photographers specialize in capturing and creating photo memories to last a lifetime. As we always try to ensure that your photos are as unique and personal as possible, we offer a wide range of products for your post-session selection.</p>
						<p>Below is a listing of our individual a la carte products. We do offer value packages as well that offer savings from purchasing a collection of a la carte items.</p>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Photographic Prints</h3>
						<p class="condensed">Our photographic prints are made to last a lifetime. Made from a unique process, all the prints are done by using a special chemical process to create real photographs and not inkjet prints.</p>
						<p>These prints are sized anywhere from 3.5x5 to 30x40. Pricing starts at $5</p>
					</div>
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Aluminum Prints</h3>
						<p class="condensed">Aluminum prints are a new way of displaying your photos. Printed directly onto aluminum, these prints have a unique metallic feature that only the aluminum properties can create. These prints are sized anywhere from 4x6 to 30x40. Pricing starts at $40</p>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />						
						<h3>Canvas &amp; Velvet Prints</h3>
						<p class="condensed">Our canvas and velvet prints come in a variety of finishes and sizes. Ranging from a stretcher frame to gallery wraps, there are numerous ways to display these wonderful natural fiber prints. Pricing starts at $110. Please contact us directly for specific pricing &amp; finishes.</p>
					</div>
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3 class="condensed">Custom Framing &amp; Design Service</h3>
						<p class="condensed">We offer a wide selection of custom framing options. Classic frames, modern frames, barn wood frames &amp; Organic Bloom frames are some of the options we offer. Our frames come complete with your photos and are ready to hang. Pricing starts at $40. We also offer a variety of collections that are a 10-25% savings off of a la carte pricing.</p><br />
						<p>We know deciding on color, style and size can be difficult. Why not let us help? We offer a complimentary service that can show you exactly what your framed print will look like on your wall! Simply contact us for details to get started.</p>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Photo Books &amp; Albums</h3>
						<p class="condensed">An excellent way to preserve your memories when you simply can't decide! Our custom made photo books &amp; albums are made specifically to fit the photos and the client. They are all made with archival quality. These beautiful keepsakes make great gifts and are an excellent way to show off your photos to family and friends. Pricing starts at $175.</p>
					</div>
					<div class="box left">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Custom Photo Cards</h3>
						<p class="condensed">Custom photo cards are another great way to use your photos. Created with personal messages and designs there are hundreds of possibilities. Cards come in a variety of styles &amp; shapes. In addition envelopes come in a variety of colors to match and custom address stickers to match your card's design can be created for the perfect finishing touch.</p>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Digital Products</h3>
						<p class="condensed">Our digital products are great for clients who live life in the digital world. Available from your session individual files as well as complete collections can be purchased. Individual files start at $10 and collections start at $150.</p>
					</div>
					<div class="box right">
						<img src="http://placehold.it/<?=$big_image?>" />
						<h3>Much More!</h3>
						<p class="condensed">We are always finding new and creative ways to display your photos. Creating custom phone cases, magnetic display wires, keepsake calendars, notepads, storyboards &amp; animal shaped collages are just a few of our options. Inquire about our current studio look-book for the latest options!</p>
					</div>
				</div> <!-- /#maosonry -->
			</div>
			<div id="faq" class="section">
				<ol>
					<li>
						<h4>How many weddings do you photograph a day?</h4>
						<p>We will only book one wedding per day. When you book with us you are getting our team, we don't have additional photographers who work under our name. Besides, it's your special day and we want to give you our undivided attention to make sure it's the best possible experience.</p>
					</li>
					<li>
						<h4>Is a deposit required?</h4>
						<p>In order to hold your date we require a $200 deposit. This deposit applies regardless of the package that you choose. Typically you submit your deposit with the signed wedding contract.</p>
					</li>
					<li>
						<h4>How far in advance should I book?</h4>
						<p>It is best to book a minimum of 6 months in advance. We can book as far in advance as you need. If your wedding is closer than 6 months don't hesitate to contact us, occasionally we may still have your date available.</p>
					</li>
					<li>
						<h4>Can you hold a date? When will my date be officially booked?</h4>
						<p>Unfortunately we can only hold a date once your deposit has been submitted. At the time of your deposit your date is officially booked and cannot be booked by any other client.</p>
					</li>
					<li>
						<h4>We noticed that you list four main packages. What if these aren't exactly what we want, can they be modified?</h4>
						<p>Absolutely! We list some packages that are most common; however we know that these might not be the right fit. We want your wedding photography to be exactly what you like and can build any custom package you would like. All you have to do is ask.</p>
					</li>
					<li>
						<h4>Do you shoot with a second photographer? Will we need to pay more for them?</h4>
						<p>I always shoot with a second photographer. She is my back-up, and I'm hers. We simply do not shoot alone. This is a personal preference of ours as we feel it's the best way to get entire coverage of your special day. As this is our preference we do not charge a premium for this. All of our shooting fees are for both of us.</p>
					</li>
					<li>
						<h4>What type of equipment do you use?</h4>
						<p>We both shoot with Canon equipment. Again this is a personal preference. We have used Canon equipment for the duration of our photography careers and have loved using it every minute.</p>
					</li>
					<li>
						<h4>Do you travel for weddings?</h4>
						<p>We certainly do! We love to travel. Please contact us directly for travel rates.</p>
					</li>
					<li>
						<h4>What is an engagement session and is it necessary?</h4>
						<p>An engagement session is a fun photo session just for the couple. These photos are great to use at the wedding reception, in a personalized guest book or on save the dates/invitations. It also gives you the opportunity to see how we work and try out different shots. It will give you and us a better idea of how to effectively work together on your wedding day. Since these sessions primarily benefit us, they are complimentary and are included in every wedding package.</p>
					</li>
					<li>
						<h4>When should formal photos be done?</h4>
						<p>This is entirely up to you. Most commonly they are taken before or after the ceremony, however in our consultation we can discuss all these details in order to ensure you get exactly what you want.</p>
					</li>
					<li>
						<h4>Do you follow a certain schedule for taking the photos?</h4>
						<p>The only schedule we follow is you schedule. We review this together a few weeks in advance of the wedding. We work entirely around what you would like to happen and stick to that plan.</p>
					</li>
					<li>
						<h4>When will we get to see our wedding photos?</h4>
						<p>Typically you will receive your proofs 6-8 weeks after your wedding. At this point you meet us for a viewing to go through your images and start your album design process.</p>
					</li>
					<li>
						<h4>What types of finished products do you offer? Are we able to have an album done?</h4>
						<p>We have many different types of finished products that include photographic prints, canvas prints, frames, digital files, thank you cards and more. Every package we offer includes the main wedding album for the bride &amp; groom. We feel this is the best way to preserve memories and tell the story of your day. We love adding on parent albums (mini versions of the wedding album) as a keepsake for the parents of the bride &amp; groom. For more information please look at our <a href="/weddings/info">product page</a> or contact us directly.</p>
					</li>
					<li>
						<h4>Will you use our photos as samples?</h4>
						<p>We love to use photos from every wedding that we photograph as samples of our work. Although we hold the copyright we still ask your permission and have you sign a model release.</p>
					</li>
					<li>
						<h4>I would like you to photograph my wedding, how do I start the process?</h4>
						<p>All you have to do is contact us. You can use this form, email us at <a href="mailto:contact@nicolelynn.com">contact@nicolelynn.com</a> or give us a call (403) 988-2204.</p>
					</li>
				</ol>
			</div>
			<div id="publications-and-testimonials" class="section">
				<div class="testimonial">
					<p>&ldquo;It was a truly wonderful experience. Nicole and her mom work as a team and do wonderful work. It added a personal touch to the event we held. It was a truly rewarding experience to have them do our wedding.&rdquo;<br /><span class="author">&mdash;Jen &amp; Trevor Karren</span></p>
				</div>
				<div class="testimonial">
					<p>&ldquo;My husband and I had the extreme pleasure of having Nikki and Becky photograph our engagement and wedding pictures and we could not be happier with the quality of the pictures. Nikki was extremely organized throughout the entire wedding preparation process and this quality along with her great sense of humor, creativity, and incredibly personable nature have me recommending her to anyone who is looking for a skilled photographer. My husband, who normally isn't a fan of having his pictures taken professionally, found the process to be very painless &ndash; in fact, we both found that having Nikki and Becky taking our pictures was fun and comfortable.&rdquo;<br /><span class="author">&mdash;Isobel &amp; Dan Jones</span></p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>