<!doctype html>
<html lang="en">
<?php
error_reporting(0);
require_once(dirname(dirname(dirname(__FILE__))).'/inc/process.php');
ob_start(); ?>
<!--<p><strong>Email:</strong><br/> 
	<a href="mailto:&#x63;&#x6F;&#x6E;&#x74;&#x61;&#x63;&#x74;&#x40;&#x6E;&#x69;&#x63;&#x6F;&#x6C;&#x65;&#x6C;&#x79;&#x6E;&#x6E;&#x2E;&#x63;&#x6F;&#x6D;">&#x63;&#x6F;&#x6E;&#x74;&#x61;&#x63;&#x74;&#x40;&#x6E;&#x69;&#x63;&#x6F;&#x6C;&#x65;&#x6C;&#x79;&#x6E;&#x6E;&#x2E;&#x63;&#x6F;&#x6D;</a>
</p>
<p><strong>Phone:<br/> </strong>403-988-2204</p> -->
<?php 
$sidebar = ob_get_contents();
ob_end_clean();


// Add some JavaScript
ob_start(); ?>
<!-- For Calendar-->
<link rel="stylesheet" href="/js/jquery-ui-1.8.9.custom/css/south-street/jquery-ui-1.8.9.custom.css" type="text/css" media="screen" />
<script src="/js/jquery-ui-1.8.9.custom/js/jquery-ui-1.8.9.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
	(function($){
		$(function(){
			//$('#date').datepicker();
			$('#saveForm').button();
			
		});
	})(jQuery);
</script>
<?php 
$js = ob_get_contents();
ob_end_clean();

ob_start();
?>
<head>
	<title>Contact | Weddings by Nicole-Lynn Photography</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway|Dancing+Script' rel='stylesheet' type='text/css'>
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/weddings/css/normalize.css">
	<link rel="stylesheet" type="text/css" href="/js/jquery-ui-1.8.9.custom/css/south-street/jquery-ui-1.8.9.custom.css">
	<link rel="stylesheet" type="text/css" href="/weddings/css/styles.css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script src="/js/jquery-ui-1.8.9.custom/js/jquery-ui-1.8.9.custom.min.js"></script>
	<script type="text/javascript" src="/weddings/js/custom.js"></script>
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->
	<!-- Google Analytics script -->
	<script type="text/javascript" src="/js/ga.js"></script>
	<style type="text/css">
		* { font-family:'Raleway', sans-serif; }
		p { margin-bottom:0; padding-bottom:10px; }
		body { background:url(../img/wood-bg.jpg); }
		header { width:100%; height:130px; background:#fff; border-bottom:1px solid #444; }
		header .wrapper { width:1049px; margin:0 auto; }
		header .title { float:left; width:500px; margin:15px 0 0 0; color:#444; }
		header .title .logo { float:left; width:100px; margin-right:20px;}
		header .header { float:left; width:340px; font-size:20px; margin:27px 0 0 0; padding:0; text-decoration:none; color:#c0e1d6; font-family:Arial, sans-serif; text-transform:uppercase; font-weight:normal; }
		header .sub-header { float:left; font-family:Arial, sans-serif; color:#713d25; font-weight:normal; }
		header h2 { float:left; width:180px; margin:0; padding:0; }
		header h1 a { text-decoration:none; color:#713d25; font-family:Arial, sans-serif; }
		#nav { width:536px; float:right; margin:50px 0 0 0; padding:0; }
		#nav li { list-style-type:none; float:left; margin-right:10px; }
		#nav li a { text-decoration:none; color:#444; padding:3px 7px; }
		#nav li a.active { border:1px solid #713d25; }
		#nav li a.active:hover { border:1px solid #c0e1d6; }
		#nav li a:hover { color:#fff; background:#c0e1d6; }
		#nav li a.active { border:1px solid #444;}
		#nav li a:hover { color:#fff; background:#444; }
		#container { width:1049px; margin:0 auto; padding:50px; }
		#container .content-wrapper { float:left; background:#fff; border:1px solid #444; padding:50px; color:#444; }
		#container .content-wrapper h1 { margin-top:0; padding-top:0; }
		.scrollable { position:relative; width:900px; height:600px; overflow:hidden; margin:-45px 0 0 14px; }
		.scrollable .items { position:absolute; width:20000em; }
		.scrollable .items div { float:left; width:900px; }
		.scrollable .items img { max-width:100%; height:auto; }
		#container .navi { width:48px; margin:0 auto; margin-top:10px; }
		#container .navi a { float:left; display:block; width:10px; height:10px; background:#000; margin:0 2px; cursor:pointer; border:1px solid #000; }
		#container .navi a.active { background:#fff; }
		#container .content .column h3 { padding-top:110px; font-size:24px; font-weight:normal; }
		#container #contact-form { float:left; background:#fff; }
		#container #contact-form .warning { margin-bottom:10px; text-transform:uppercase; font-size:12px; }
		#container #contact-form #human { display:none; }
		#container #contact-form ul { padding:0; }
		#container #contact-form ul li { float:left; list-style-type:none; width:100%; margin-bottom:25px; }
		#container #contact-form ul li label { float:left; display:block; width:200px; }
		#container #contact-form ul li input { float:left; }
		#container #contact-form #success { color:green; border:1px solid green; padding:25px; margin:25px 0; }
		footer { width:100%; height:100px; }
	</style>
</head>
<body>
	<header>
		<div class="wrapper">
			<div class="title">
				<a href="/">
					<img class="logo" src="/img/landing-logo-2.png" />
					<span class="header">Weddings</span>
					<span class="sub-header">by Nicole-Lynn Photography</span>
				</a>
			</div>
			<ul id="nav">
				<li><a href="/weddings/">Home</a></li>
				<li><a href="/weddings/about">About</a></li>
				<li><a href="/weddings/gallery">Gallery</a></li>
				<li><a href="/weddings/info">Info</a></li>
				<li><a href="#" class="active">Contact</a></li>
				<li><a href="http://nicolelynn2010.wordpress.com/">Blog</a></li>
				<li><a href="/weddings/photocart/">Client Access</a></li>
			</ul>
		</div>
	</header>
	<div id="container">
		<div class="content-wrapper">
			<h1>Contact</h1>
			<p>If you have any questions regarding sessions, pricing, or you would like to request a booking please do not hesitate to contact us. We will respond to any questions, comments, or booking requests within two business days. We would be happy to hear from you.</p>
			<form id="contact-form" name="contact-form" class="topLabel" enctype="multipart/form-data" method="post">
				<?php
				if(isset($_POST['name']) && ($_POST['name'] == '')) {
					echo '<div id="success"><p>Form sent! You\'ll be hearing from us soon. Thank you!</p></div>';
				}
				?>
				<p class="warning" style="margin-bottom: 10px">Fields marked <span class="required">*</span> are required.</p>
				<input id="human" name="human" type="text">
				<ul>
					<li>
						<label class="desc" for="name">Name <span class="required">*</span></label>
						<input id="name" name="name" type="text" class="field text medium" value="" size="30" maxlength="150" tabindex ="1" />
					</li>
					<li>
						<label class="desc" for="email">Email Address<span class="required">*</span></label>
						<input id="email" name="email" type="text" class="field text medium" value="" size="30" maxlength="150" tabindex ="2" />
					</li>
					<li>
						<label class="desc" for="phone">Phone Number</label>
						<input id="phone" name="phone" type="text" class="field text medium" value="" size="30" maxlength="12" tabindex ="3" />
					</li>
					<li>
						<label class="desc" for="date">Requested Session Date</label>
						<input id="date" name="date" type="text" class="field text medium" value="" size="30" maxlength="150" tabindex ="4" />
					</li>
					<li>
						<label class="desc" for="session">Session Type</label>
						<select id="session" class="select" name="session" tabindex="5">
							<option value="None Selected">-- None Selected --</option>
							<option value="Animal Portraits">Animal Portraits</option>
							<option value="Before &amp; After Baby">Before &amp; After Baby</option>
							<option value="Couples &amp; Engagement">Couples &amp; Engagement</option>
							<option value="Family">Family</option>
							<option value="Graduation &amp; Special Events">Graduation &amp; Special Events</option>
							<option value="Kids &amp; Teens">Kids &amp; Teens</option>
							<option value="Modeling &amp; Headshots">Modeling &amp; Headshots</option>
							<option value="Weddings">Weddings</option>
						</select>
					</li>
					<li>
						<label class="desc" for="message">Message <span class="required">*</span></label>
						<textarea name="message" id="message" rows="12" cols="52" tabindex="6"></textarea>
					</li>
					<li class="buttons">
						<input id="saveForm" class="btTxt submit" type="submit" name="submit" value="Submit" />
					</li>
				</ul>
			</form>
		</div>
	</div>
</body>
</html>
