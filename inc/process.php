<?php
	require_once(dirname(dirname(__FILE__)).'/lib/Email/class.phpmailer.php');
	require_once(dirname(dirname(__FILE__)).'/lib/Validators/ValidateFullName.class.php');
	require_once(dirname(dirname(__FILE__)).'/lib/Validators/ValidateEmail.class.php');

	function sanitizeVar( $var ){
		return filter_var( $var, FILTER_SANITIZE_STRING );
	}
	
	// Test to see if the human field was filled out, to avoid spam bots
	if ( isset( $_POST['submit'] ) && empty( $_POST['human'] ) ) {
	
		$formIsValid = true;
		// Create an array to store the errors
		$errors = array();
	
		$name = sanitizeVar( $_POST['name'] );
		$email = sanitizeVar( $_POST['email'] );
		$phone = sanitizeVar( $_POST['phone'] );
		$date = sanitizeVar( $_POST['date'] );
		$session = sanitizeVar( $_POST['session'] );
		$message = sanitizeVar( $_POST['message'] );
		
		// Collection of validators
		$validators = array();
		$validators[] = new ValidateFullName($_POST['name']);
		$validators[] = new ValidateEmail($_POST['email']);
	
		foreach( $validators as $validator ) {
			if( !$validator->isValid() ) {
				while( $error = $validator->fetch() ) {
					$errors[] = $error;
				}
				$formIsValid = false;
			}
		}

		if( !isset( $_POST['message'] ) || empty( $_POST['message'] ) ) {
			array_push( $errors, 'Your message cannot be empty.' );
			$formIsValid = false;
		}

		if( $formIsValid == true ) {
			$mail  = new PHPMailer(); // defaults to using php "mail()"

			// $body             = file_get_contents('contents.html');
			$body  = '<strong>From:</strong> ' . $name . "<br />";  
			$body .= '<strong>Email:</strong> ' . $email . "<br />";  
			$body .= "<strong>Phone:</strong> " . $phone . "<br />";
			$body .= "<strong>Date:</strong> " . $date . "<br />";
			$body .= "<strong>Session Type:</strong> " . $session . "<br />";
			$body .= "<strong>Message:</strong><br />" . $message . "<br /><br />";
			//TO DO: Determine if eregi_replace is needed. Currently is marked for deprecation.
			//$body  = eregi_replace("[\]",'',$body);

			$mail->AddReplyTo($email, $name);

			$mail->SetFrom('mail-bot@nicolelynn.com', 'Nicole-Lynn Photography Mail Robot');

			//testing email:
			//$mail->AddAddress("mark.labrecque@gmail.com", "Mark Labrecque");
			$mail->AddAddress("contact@nicolelynn.com", "Nicole-Lynn Elliott");

			$mail->Subject    = "Message from $name - Nicole-Lynn Photography";

			// $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

			// Databse backup
			include dirname(__FILE__)."/setup.php";
			$link = mysqli_connect( HOST, DBUSER, DBPASS, DBNAME) or die("Error " . mysqli_error($link));
			if ($link->connect_errno) {
			    echo "Failed to connect to MySQL: (" . $link->connect_errno . ") " . $link->connect_error;
			}
			if($link) {
				$name = mysqli_real_escape_string($link, $name);
				$email = mysqli_real_escape_string($link, $email);
				$phone = mysqli_real_escape_string($link, $phone);
				$date = mysqli_real_escape_string($link, $date);
				$session = mysqli_real_escape_string($link, $session);
				$message = mysqli_real_escape_string($link, $message);

				mysqli_query($link, 'INSERT INTO emails (name,email,phone,request_date,session_type,message) VALUES("' . $name . '", "' . $email . '", "' . $phone . '", "' . $date . '", "' . $session . '", "' . $message . '")');
			}
			else echo "$link failed";
			
			$mail->MsgHTML($body);

			if(!$mail->Send()) {
				array_push( $errors, "Your email was not delivered successfully, please send an email directly to " . $address . " and we'll resolve this issue." );
			  // echo "Mailer Error: " . $mail->ErrorInfo;
			} else {
			  $success = true;
			
				// Clear out the fields once the email is sent
				foreach( $_POST as $key => $value ) {
						$_POST[$key] = '';
				}
			}
		}
		
	}
?>