<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<meta name="description" content="" />
		<meta name="keywords" content="<?php
			$keywords = ( isset($keywords) ) ? $keywords:'';
			echo @$keywords;
		?>" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /> 
		
		
		<title>
			<?php			
			// Prepend the page title to the site title 
			if ( isset($browser_title) && !empty($browser_title) ) {
		 		echo $browser_title . ' - ';
			}
			?> 
			Nicole-Lynn Photography
		</title>
		
		<!-- Page specific CSS head content -->
 		<link href="/css/screen.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
		<!--[if lt IE 7]>
		<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
		<![endif]-->
		
		<?php echo @$css ?>
		
		<!-- Page specific JS head content -->
		<script type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>		
		<script type="text/javascript" src="/js/jreject/jquery.reject.min.js"></script>
		<script type="text/javascript" charset="utf-8">
			// Prompt IE6 Users to upgrade
			$(document).ready(function () {
				$.reject(); // Default Settings  
				return false;
			});
			
		</script>
		<?php echo @$js ?>
		
		<!-- Extra head content -->
		<?php echo @$head ?>
	</head>
	<body>
		<div id="container">
			<div id="header">
				
				<div id="logo">
					<a href="/"><img src="/img/logo.jpg" alt="Home" /></a>
				</div>	
				
				<?php
				require_once(dirname(dirname(__FILE__))."/lib/UI/Menu.class.php");

				$menu = new Menu( $_SERVER['PHP_SELF'] );

				$menuData = new MenuData($menu);
				$menuData->add('/index.php','Home');
				$about_id = $menuData->add('/about/index.php','About');
				$gallery_id = $menuData->add('/gallery/index.php','Gallery');
					$menuData->add('/gallery/new-beginnings/index.php','New Beginnings', $gallery_id);
					$menuData->add('/gallery/growing-up/index.php','Growing Up', $gallery_id);
					$menuData->add('/gallery/expanding-horizons/index.php','Expanding Horizons', $gallery_id);
					$menuData->add('/gallery/special-moments/index.php','Special Moments', $gallery_id);
					$menuData->add('/gallery/shared-lives/index.php','Shared Lives', $gallery_id);
					$menuData->add('/gallery/fashion/index.php','Fashion', $gallery_id);
					$menuData->add('/gallery/unconditional-love/index.php','Unconditional Love', $gallery_id);
				$info_id = $menuData->add('/info/index.php','Info');
				$contact_id = $menuData->add('/contact/index.php','Contact');
				$blog_id = $menuData->add('http://nicolelynn2010.wordpress.com/  ','Blog');
				$client_access_id = $menuData->add('/photocart/','Client Access');
				
				$menu->getParents( $menu->getCurrentParent() );
				
				?>
				<div id="menu">			
					<?php $menu->build(null, 1); ?>
				</div><!-- /menu -->
			</div><!-- /header -->
		
			<div id="content">
				<?php echo @$content ?>
			</div><!-- /content -->
			
		</div><!-- /container -->
		<div id="footer">
			<div id="footer-content">
				<p id="copyright">Copyright &copy; <?php echo date('Y') ?> <a href="/">Nicole-Lynn Photography</a></p>
				<?php echo @$footer ?>
			</div><!-- /footer-content -->
		</div> <!-- /footer -->
	</body>
</html>
