<?php

@$css .= '<link rel="stylesheet" href="/css/interior.css" type="text/css" media="all">';


// Head output
ob_start();

?>
<div id="sidebar-container"> 
	<?php if (isset($sidebar) && !empty($sidebar)): ?>
		<div id="sidebar">
			<?php echo $sidebar ?>
		</div>
	<?php endif ?>
		<div id="sidebar-footer">&nbsp;</div><!-- /sidebar-footer -->
</div>

	<div id="bodytext-container">
		<div id="bodytext">
			<?php echo @$content ?>
		</div>
		<div id="bodytext-footer">&nbsp;</div><!-- /bodytext-footer -->
	</div>


	
<?php
@$content = ob_get_contents();
ob_end_clean();


include_once('base.php')
?>
