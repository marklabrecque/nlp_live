<?php

// content output
ob_start();

?>
	
	<div id="bodytext-container" class="no-sidebar">
		<div id="bodytext">
			<?php echo @$content ?>
		</div>
	</div>
	
<?php
$content = ob_get_contents();
ob_end_clean();


include_once('base.php');
