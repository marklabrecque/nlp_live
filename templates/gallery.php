<?php ob_start(); ?>
<link rel="stylesheet" href="/css/gallery.css" type="text/css" media="all">
<link rel="stylesheet" href="/js/shadowbox/shadowbox.css" type="text/css" media="all">
<?php
@$css = ob_get_contents();
ob_end_clean();

ob_start();
?>
<script src="/js/shadowbox/shadowbox.js" type="text/javascript"></script>
<script type="text/javascript">
	Shadowbox.init();
</script>

<?php
@$js = ob_get_contents();
ob_end_clean();

ob_start(); ?>
<h2>Pick A Gallery</h2>
<?php
require_once(dirname(dirname(__FILE__))."/lib/UI/Menu.class.php");

$menu = new Menu( $_SERVER['PHP_SELF'] );

$menuData = new MenuData($menu);

$folders = array(
	'before-and-after-baby',
	'kids-and-teens',
	'family',
	'couples-and-engagement',
	'wedding',
	'graduation-and-special-events',
	'modeling-and-headshots',
	'animal-portraits',
);

foreach ($folders as $folder)
{	
	$gallery_name = ucwords(str_replace(' and ', ' &amp; ', str_replace('-', ' ', $folder)));
	$menuData->add('/gallery/'.$folder.'/index.php',$gallery_name);
}	// 
	// $menuData->add('/gallery/new-beginnings/index.php','New Beginnings');
	// $menuData->add('/gallery/growing-up/index.php','Growing Up');
	// $menuData->add('/gallery/expanding-horizons/index.php','Expanding Horizons');
	// $menuData->add('/gallery/special-moments/index.php','Special Moments');
	// $menuData->add('/gallery/shared-lives/index.php','Shared Lives');
	// $menuData->add('/gallery/fashion/index.php','Fashion');
	// $menuData->add('/gallery/unconditional-love/index.php','Unconditional Love');

$menu->getParents( $menu->getCurrentParent() );
?>
<div id="gallery-menu">
	<?php $menu->build(null, 1); ?>
</div><!-- /gallery-menu -->

<?php 
$sidebar = ob_get_contents();
ob_end_clean();



include_once('two-column.php')
?>
