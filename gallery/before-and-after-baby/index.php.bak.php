<?php
$folder = basename(dirname(__FILE__));
$browser_title = ucwords(str_replace(' and ', ' &amp; ', str_replace('-', ' ', $folder)));
ob_start();
include_once '../list.php';
$content = ob_get_contents();
ob_end_clean();
include_once('../../templates/gallery.php');