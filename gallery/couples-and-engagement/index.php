
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<title>Couples &amp; Engagements | Nicole-Lynn Photography</title>
		<!-- Page specific CSS head content -->
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		<link href="/css/screen.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
		<link href="/css/interior.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lt IE 7]>
		<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
		<![endif]-->
		<link rel="stylesheet" href="/css/interior.css" type="text/css" media="all">
		<!-- Page specific JS head content -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
		<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
		<script type="text/javascript" src="/js/custom/custom.js"></script>
		<!-- Google Analytics script -->
		<script type="text/javascript" src="/js/ga.js"></script>
	</head>
	<body class="gallery-page">
		<div id="container">
			<div id="header">
				<div id="logo">
					<a href="/"><img src="/img/logo.jpg" alt="Home" /></a>
				</div>
				<div id="menu">
					<ul>
						<li id="m-home" class="first"><a href="/home/" title="Home">Home</a></li>
						<li id="m-about"><a href="/about/" title="About">About</a></li>
						<li id="m-gallery" class="current"><a href="/gallery/" title="Gallery">Gallery</a></li>
						<li id="m-pricing"><a href="/info/" title="Info">Info</a></li>
						<li id="m-contact"><a href="/contact/" title="Contact">Contact</a></li>
						<li id="m-blog"><a href="http://nicolelynn2010.wordpress.com/  " title="Blog">Blog</a></li>
						<li id="m-client-access" class="last"><a href="/photocart/" title="Client Access">Client Access</a></li>
					</ul>
				</div><!-- /menu -->
			</div><!-- /header -->
			<div id="content">
				<div id="bodytext-container" class="secondary">
					<div id="bodytext">
						<div class="dropdown shadow">
							<span class="current">Couples &amp; Engagement</span>&nbsp;<i class="icon-angle-down"></i>
							<ul class="options">
								<li><a href="../animal-portraits">Animal Portraits</a></li>
								<li><a href="../before-and-after-baby">Before &amp; After Baby</a></li>
								<li><a href="#" class="active">Couples &amp; Engagement</a></li>
								<li><a href="../dance-and-sports">Dance &amp; Sports Photography</a></li>
								<li><a href="../family/">Family</a></li>
								<li><a href="../graduation-and-special-events/">Graduation &amp; Special Events</a></li>
								<li><a href="../kids-and-teens/">Kids &amp; Teens</a></li>
								<li><a href="../modeling-and-headshots/">Modeling &amp; Headshots</a></li>
								<li class="last"><a href="../../weddings/gallery">Wedding</a></li>
							</ul>
						</div>
						<a href="#" id="switch">Slideshow: <span>On</span></a>
						<div id="gallery">
							<div id="no-slideshow">
							<img id="jumbotron" src="/img/gallery/couples-and-engagement/1.jpg" />
								<div id="couples-and-engagement">
									<div class="wrapper">
										<div class="scrollable">
											<div class="items">
												<div>
													<img class="active wide" src="/img/gallery/couples-and-engagement/thumbs/1.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/2.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/3.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/4.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/5.jpg" />
												</div>
												<div>
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/6.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/7.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/8.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/9.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/10.jpg" />
												</div>
												<div>
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/11.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/12.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/13.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/14.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/15.jpg" />
												</div>
												<div>
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/16.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/17.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/18.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/19.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/20.jpg" />
												</div>
												<div>
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/21.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/22.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/23.jpg" />
													<img class="portrait" src="/img/gallery/couples-and-engagement/thumbs/24.jpg" />
													<img class="wide" src="/img/gallery/couples-and-engagement/thumbs/25.jpg" />
												</div>
											</div>
										</div>
										<div class="arrows">
											<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
											<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div id="slideshow" class="active">
								<div class="scrollable">
									<div class="items">
										<div><img class="active wide" src="/img/gallery/couples-and-engagement/1.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/2.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/3.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/4.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/5.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/6.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/7.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/8.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/9.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/10.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/11.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/12.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/13.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/14.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/15.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/16.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/17.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/18.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/19.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/20.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/21.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/22.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/23.jpg" /></div>
										<div><img class="portrait" src="/img/gallery/couples-and-engagement/24.jpg" /></div>
										<div><img class="wide" src="/img/gallery/couples-and-engagement/25.jpg" /></div>
									</div>
								</div>
								<div class="arrows">
									<a href="#" class="browse left prev"><i class="icon-chevron-sign-left"></i></a>
									<a href="#" class="browse right next"><i class="icon-chevron-sign-right"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div id="bodytext-footer">&nbsp;</div><!-- /bodytext-footer -->
				</div>
			</div><!-- /content -->
		</div><!-- /container -->
		<div id="footer">
			<div id="footer-content">
				<p id="copyright">Copyright &copy; 2013 <a href="/">Nicole-Lynn Photography</a></p>
			</div><!-- /footer-content -->
		</div> <!-- /footer -->
	</body>
</html>
