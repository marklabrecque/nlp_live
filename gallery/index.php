
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<title>Gallery | Nicole-Lynn Photography</title>
		<!-- Page specific CSS head content -->
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		<link href="/css/screen.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
		<link href="/css/interior.css" rel="stylesheet" type="text/css" media="all" />
		<!--[if lt IE 7]>
		<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
		<![endif]-->
		<link rel="stylesheet" href="/css/interior.css" type="text/css" media="all">
		<!-- Google Analytics script -->
		<script type="text/javascript" src="/js/ga.js"></script>
		<!-- Page specific JS head content -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
		<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
		<script type="text/javascript" src="/js/custom/custom.js"></script>
	</head>
	<body class="gallery-page">
		<div id="container">
			<div id="header">
				<div id="logo">
					<a href="/"><img src="/img/logo.jpg" alt="Home" /></a>
				</div>
				<div id="menu">
					<ul>
						<li id="m-home" class="first"><a href="/home/" title="Home">Home</a></li>
						<li id="m-about"><a href="/about/" title="About">About</a></li>
						<li id="m-gallery" class="current"><a href="#" title="Gallery">Gallery</a></li>
						<li id="m-pricing"><a href="/info/" title="Info">Info</a></li>
						<li id="m-contact"><a href="/contact/" title="Contact">Contact</a></li>
						<li id="m-blog"><a href="http://nicolelynn2010.wordpress.com/  " title="Blog">Blog</a></li>
						<li id="m-client-access" class="last"><a href="/photocart/" title="Client Access">Client Access</a></li>
					</ul>
				</div><!-- /menu -->
			</div><!-- /header -->
			<div id="content">
				<div id="bodytext-container" class="secondary">
					<div id="bodytext">
						<h1>Gallery</h1>
						<div id="masonry">
							<div class="box">
								<a href="before-and-after-baby/">
									<img src="../img/gallery/before-and-after-baby/cover.jpg" />
									<span class="mask"><span class="label">Before &amp; After Baby</span></span>
								</a>
							</div>
							<div class="box">
								<a href="kids-and-teens/">
									<img src="../img/gallery/kids-and-teens/cover.jpg" />
									<span class="mask"><span class="label">Kids &amp; Teens</span></span>
								</a>
							</div>
							<div class="box">
								<a href="family/">
									<img src="../img/gallery/family/cover.jpg" />
									<span class="mask"><span class="label">Family</span></span>
								</a>
							</div>
							<div class="box">
								<a href="couples-and-engagement/">
									<img src="../img/gallery/couples-and-engagement/cover.jpg" />
									<span class="mask"><span class="label">Couples and Engagement</span></span>
								</a>
							</div>
							<div class="box">
								<a href="../../weddings/gallery">
									<img src="/weddings/img/jess-and-andy-cover.jpg" />
									<span class="mask"><span class="label">Wedding</span></span>
								</a>
							</div>
							<div class="box">
								<a href="graduation-and-special-events/">
									<img src="../img/gallery/graduation-and-special-events/cover.jpg" />
									<span class="mask"><span class="label">Graduation &amp; Special Events</span></span>
								</a>
							</div>
							<div class="box">
								<a href="modeling-and-headshots/">
									<img src="../img/gallery/modeling-and-headshots/cover.jpg" />
									<span class="mask"><span class="label">Modeling &amp; Headshots</span></span>
								</a>
							</div>
							<div class="box">
								<a href="animal-portraits/">
									<img src="../img/gallery/animal-portraits/cover.jpg" />
									<span class="mask"><span class="label">Animal Portraits</span></span>
								</a>
							</div>
							<div class="box">
								<a href="dance-and-sports/">
									<img src="../img/gallery/dance-and-sports/cover.jpg" />
									<span class="mask"><span class="label">Dance &amp; Sports</span></span>
								</a>
							</div>
						</div>
					</div>
					<div id="bodytext-footer">&nbsp;</div><!-- /bodytext-footer -->
				</div>
			</div><!-- /content -->
		</div><!-- /container -->
		<div id="footer">
			<div id="footer-content">
				<p id="copyright">Copyright &copy; 2013 <a href="/">Nicole-Lynn Photography</a></p>
			</div><!-- /footer-content -->
		</div> <!-- /footer -->
	</body>
</html>
