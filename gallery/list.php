<h1><?php echo $browser_title ?></h1>

<ul id="thumbs">
<?php
$img_dir = '../../img/gallery/'.$folder.'/';

if ($handle = opendir( $img_dir . 'thumbs/' ))
{
	while (false !== ($file = readdir($handle)))
	{
		$images[] = $file;
		sort($images);
	}
	closedir($handle);
}
foreach ($images as $file) {
	$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
	if($ext == 'jpg')
	{
		echo "\t".'<li><a href="' . $img_dir . $file . '" rel="shadowbox[gallery]"><img src="' . $img_dir . 'thumbs/' . $file . '" /></a></li>';
	}
}
?>
</ul>
