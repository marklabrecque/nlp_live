/* custom.js Custom functions */
$(document).ready(function() {
	$('.dropdown').click(function() {
		$('.dropdown').removeClass('shadow');
		$('.dropdown .options').show();
	});
	$('#about-page .dropdown .options li a, .info-page .dropdown .options li a,').click(function() {
		if($(this).hasClass('active')) {
			$('.dropdown .options').hide();
			return false;
		} else {
			$('.dropdown .options li a.active').removeClass('active');
			var selection = $(this).html();		
			$('.dropdown .current').html(selection);
			$('.dropdown').addClass('shadow');
			$('.dropdown .options').hide();
			$('.section').hide();
			selection = selection.toLowerCase();
			selection = selection.replace(" &amp; ", "-and-");
			selection = selection.replace(" ", "-");
			$('#' + selection).show();
			$(this).addClass('active');
			return false;
		}
	});

	$('.gallery-page .dropdown .options li a').click(function() {
		if($(this).hasClass('active')) {
			$('.dropdown .options').hide();
			return false;
		}
	});

	$('.gallery-page #masonry').masonry({
		'gutter' : 25,
		'columnWidth' : 250
	});
	$('.info-page #masonry').masonry({
		'gutter' : 25,
		'columnWidth' : 427
	});

	$('.wrapper .browse').click(function() {
		return false;
	});

	// gallery functionality
	$('#no-slideshow .scrollable').scrollable({ circular: true });
	$('#slideshow .scrollable').scrollable({ circular: true }).autoscroll({
		'interval': 5000,
		'autopause' : false
	});
	$('.scrollable .items img').click(function() {
		if ($(this).hasClass("active")) { return; }
		var thumbFilename = $(this).attr('src');
		console.log(thumbFilename);
		var jumboFilename = thumbFilename.replace("thumbs/", "");
		var portraitOrientation = false;
		if($(this).hasClass('portrait')) {
			portraitOrientation = true;
		}
		$('#jumbotron').fadeTo('fast', 0, function() {
			if(portraitOrientation) { $('#jumbotron').addClass('portrait'); }
			else { $('#jumbotron.portrait').removeClass('portrait'); }
			$('#jumbotron').attr('src', jumboFilename);
			$('#jumbotron').fadeTo('fast', 1);
			$('.items img.active').removeClass('active');
			$(this).addClass('active');
		});
	});
	$('.arrows .browse').click(function() { return false; });
	
	// Slideshow autoplay toggle switch
	$('#switch').click(function() {
		var scroller = $('#slideshow').data('scrollable');
		$('#no-slideshow').fadeOut('fast');
		if($('#no-slideshow').hasClass('active')) {
			$('#no-slideshow').fadeOut('fast', function() {
				scroller.start();
				$('#slideshow').fadeIn('slow').addClass('active');
			}).removeClass('active');
			$('#switch span').html('On');
		} else {
			$('#slideshow').fadeOut('fast', function() {
				scroller.stop();
				$('#no-slideshow').fadeIn('slow').addClass('active');
			}).removeClass('active');
			$('#switch span').html('Off');
		}
		$('#slideshow').toggle();
		$('#no-slideshow').toggle();
		return false;
	});
});