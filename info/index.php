
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<title>Info -  Nicole-Lynn Photography</title>
		<!-- Page specific CSS head content -->
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
 		<link href="/css/screen.css" rel="stylesheet" type="text/css" media="all" />
		<link href="/css/print.css" rel="stylesheet" type="text/css" media="print" />
		<!--[if lt IE 7]>
		<link href="/css/ie6.css" rel="stylesheet" type="text/css" media="screen"   />
		<![endif]-->
		<link rel="stylesheet" href="/css/interior.css" type="text/css" media="all">
		<!-- Page specific JS head content -->
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
		<script type="text/javascript" src="/js/masonry/masonry.min.js"></script>
		<script type="text/javascript" src="/js/custom/custom.js"></script>
		<!-- Google Analytics script -->
		<script type="text/javascript" src="/js/ga.js"></script>
		<style type="text/css">
			#bodytext-container { width:100%; margin-left:0; }
			#bodytext-container .dropdown { float:none; }
			#info .intro p { width:100%; }
			#info .box { float:left; width:427px; margin-right:25px; }
			#info .box img { width:150px; height:150px; }
			#info .box .left { float:left; width:150px; margin-right:25px; }
			#info .box .right { float:left; width:252px; }
			#faq { display:none; }
			#masonry .box { height:auto; padding:0; }
		</style>
		<!-- Extra head content -->
	</head>
	<body class="info-page">
		<div id="container">
			<div id="header">
				<div id="logo">
					<a href="/"><img src="/img/logo.jpg" alt="Home" /></a>
				</div>
				<div id="menu">
					<ul>
						<li id="m-home" class="first"><a href="/home/" title="Home">Home</a></li>
						<li id="m-about"><a href="/about/" title="About">About</a></li>
						<li id="m-gallery"><a href="/gallery/" title="Gallery">Gallery</a></li>
						<li id="m-pricing" class="current"><a href="/info/" title="Info">Info</a></li>
						<li id="m-contact"><a href="/contact/" title="Contact">Contact</a></li>
						<li id="m-blog"><a href="http://nicolelynn2010.wordpress.com/  " title="Blog">Blog</a></li>
						<li id="m-client-access" class="last"><a href="/photocart/" title="Client Access">Client Access</a></li>
					</ul>
				</div><!-- /menu -->
			</div><!-- /header -->
			<div id="content">
				<div id="bodytext-container">
					<div id="bodytext">
						<h1>Info</h1>
						<div class="dropdown">
							<span class="current">Info</span>&nbsp;<i class="icon-angle-down"></i>
							<ul class="options">
								<li><a class="active" href="#">Info</a></li>
								<li class="last"><a class="faq" href="#">FAQ</a></li>
							</ul>
						</div>
						<div id="info" class="section">
							<div class="intro">
								<p>Photo sessions are charged a $75 session fee for sessions in the Calgary area. A travel fee may apply for sessions outside of this area, please contact us for more information. This fee covers the photographer’s time.</p>
								<p>Our photographers specialize in capturing and creating photo memories to last a lifetime. As we always try to ensure that your photos are as unique and personal as possible, we offer a wide range of products for your post-session selection.</p>
								<p>Below is a listing of our individual a la carte products. We do offer value packages as well that offer savings from purchasing a collection of a la carte items.</p>
							</div>
							<div id="masonry">
								<div class="box">
									<div class="left">
										<img src="/img/products/photographic-prints.jpg" />
									</div>
									<div class="right">
										<h2>Photographic Prints</h2>
										<p>Our photographic prints are made to last a lifetime. Made from a unique process, all the prints are done by using a special chemical process to create real photographs and not inkjet prints.</p>
										<p>These prints are sized anywhere from 3.5x5 to 30x40. Pricing starts at $5</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/aluminum-prints.jpg" />
									</div>
									<div class="right">
										<h2>Aluminum Prints</h2>
										<p>Aluminum prints are a new way of displaying your photos. Printed directly onto aluminum, these prints have a unique metallic feature that only the aluminum properties can create.</p>
										<p>These prints are sized anywhere from 4x6 to 30x40. Pricing starts at $40</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/canvas-velvet-prints.jpg" />
									</div>
									<div class="right">
										<h2>Canvas &amp; Velvet Prints</h2>
										<p>Our canvas and velvet prints come in a variety of finishes and sizes. Ranging from a stretcher frame to gallery wraps, there are numerous ways to display these wonderful natural fiber prints. Pricing starts at $110. Please contact us directly for specific pricing & finishes.</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/custom-framing-design-service.jpg" />
									</div>
									<div class="right">
										<h2>Custom Framing &amp; Design Service</h2>
										<p>We offer a wide selection of custom framing options. Classic frames, modern frames, barn wood frames & Organic Bloom frames are some of the options we offer. Our frames come complete with your photos and are ready to hang. Pricing starts at $40. We also offer a variety of collections that are a 10-25% savings off of a la carte pricing.</p>
										<p>We know deciding on color, style and size can be difficult. Why not let us help? We offer a complimentary service that can show you exactly what your framed print will look like on your wall! Simply contact us for details to get started.</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/photobooks-albums.jpg" />
									</div>
									<div class="right">
										<h2>Photo Books &amp; Albums</h2>
										<p>An excellent way to preserve your memories when you simply can’t decide! Our custom made photo books & albums are made specifically to fit the photos and the client. They are all made with archival quality. These beautiful keepsakes make great gifts and are an excellent way to show off your photos to family and friends. Pricing starts at $175.</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/custom-photo-cards.jpg" />
									</div>
									<div class="right">
										<h2>Custom Photo Cards</h2>
										<p>Custom photo cards are another great way to use your photos. Created with personal messages and designs there are hundreds of possibilities. Cards come in a variety of styles & shapes. In addition envelopes come in a variety of colors to match and custom address stickers to match your card’s design can be created for the perfect finishing touch.</p>
									</div>
								</div>
								<div class="box">
									<div class="left">
										<img src="/img/products/digital-products.jpg" />
									</div>
									<div class="right">
										<h2>Digital Products</h2>
										<p>Our digital products are great for clients who live life in the digital world. Available from your session individual files as well as complete collections can be purchased. Individual files start at $10 and collections start at $150.</p>
									</div>
								</div>
							</div>
							<div class="box">
								<div class="left">
									<img src="/img/products/much-more.jpg" />
								</div>
								<div class="right">
									<h2>Much More!</h2>
									<p>We are always finding new and creative ways to display your photos. Creating custom phone cases, magnetic display wires, keepsake calendars, notepads, storyboards & animal shaped collages are just a few of our options. Inquire about our current studio look-book for the latest options!</p>
								</div>
							</div>
						</div>
						<div id="faq" class="section">
							<div class="content">
								<ol>
									<li>
										<h4>What types of portraits do you take?</h4>
										<p>We take all types of portraits; pre-natal (maternity), newborn, family, senior, bridal, engagement, boudoir, model &amp; fashion, animal, sports &amp; dance, rodeo, special event &amp; commercial. We love portraits and working with people, so we are open to anything.
									</li>
									<li>
										<h4>Do you have a studio?</h4>
										<p>We do not currently have a permanent studio. Some of our studio work is done in our office, but most of our studio work is done on location. We have a mobile backdrop, lights and other equipment to turn any space into the studio we need.</p>
									</li>
									<li>
										<h4>What type of equipment do you use?</h4>
										<p>We both shoot with Canon equipment. This is a personal preference. We have used Canon equipment for the duration of our photography careers and have loved using it every minute.</p>
									</li>
									<li>
										<h4>What types of finished products do you offer? Are we able to have an album done?</h4>
										<p>We have many different types of finished products that include photographic prints, canvas prints, frames, digital files, custom cards and more. We also offer a few types of custom albums which are a great way to showcase your portraits. For more information please look at our product page or contact us directly.</p>
									</li>
									<li>
										<h4>I’d like to have a family/newborn session, but I’m afraid my kids won’t cooperate. What do I do?</h4>
										<p>There are a few things that we do. To start we try and interact with your children without our camera first this tends to warm them up a little better. We also work at their level. If they are comfortable on the floor, we shoot on the floor with them. Patience is also important. We work with many families and this is a common situation. Sessions take time and knowing &amp; planning for that ahead of time makes everyone more relaxed.</p>
									</li>
									<li>
										<h4>What should we wear?</h4>
										<p>Whatever you are comfortable in is what works best. If you’d like to coordinate that’s great, if you don’t that’s great too! One thing we caution clients on is busy patterns. Your portraits are about you and not what you are wearing, busy patterns can be distracting.</p>
									</li>
									<li>
										<h4>How long will it take to receive our photo proofs?</h4>
										<p>Depending on the session type and size proofs take roughly 3-4 weeks to prepare.</p>
									</li>
									<li>
										<h4>Will you use our photos as samples?</h4>
										<p>We love to use photos from every session that we photograph as samples of our work. Although we hold the copyright, we still ask your permission and have you sign a model release before using your photos</p>
									</li>
									<li>
										<h4>Do you have gift certificates available?</h4>
										<p>Absolutely! They are available in any denomination. Just contact us and we will be able to set one up right away.</p>
									</li>
									<li>
										<h4>I would like you to be our photographer, how do I start the process?</h4>
										<p>All you have to do is contact us. You can use <a href="/contact/">this form</a>, email us at <a href="mailto:contact@nicolelynn.com">contact@nicolelynn.com</a> or give us a call (403) 988-2204.</p>
									</li>
								</ol>
							</div>
						</div>
					</div>
					<div id="bodytext-footer">&nbsp;</div><!-- /bodytext-footer -->
				</div>
			</div>
		</div><!-- /content -->
	</div><!-- /container -->
	<div id="footer">
		<div id="footer-content">
			<p id="copyright">Copyright &copy; 2013 <a href="/">Nicole-Lynn Photography</a></p>
						</div><!-- /footer-content -->
	</div> <!-- /footer -->
</body>
</html>
