<?php
require_once(dirname(dirname(__FILE__)).'/inc/process.php');
$browser_title = "Contact";

$css = '<link rel="stylesheet" href="/css/form.css" type="text/css" media="all">';
ob_start(); ?>
<p><strong>Email:</strong><br/> 
	<a href="mailto:&#x63;&#x6F;&#x6E;&#x74;&#x61;&#x63;&#x74;&#x40;&#x6E;&#x69;&#x63;&#x6F;&#x6C;&#x65;&#x6C;&#x79;&#x6E;&#x6E;&#x2E;&#x63;&#x6F;&#x6D;">&#x63;&#x6F;&#x6E;&#x74;&#x61;&#x63;&#x74;&#x40;&#x6E;&#x69;&#x63;&#x6F;&#x6C;&#x65;&#x6C;&#x79;&#x6E;&#x6E;&#x2E;&#x63;&#x6F;&#x6D;</a>
</p>
<p><strong>Phone:<br/> </strong>403-988-2204</p>
<?php 
$sidebar = ob_get_contents();
ob_end_clean();


// Add some JavaScript
ob_start(); ?>
<!-- For Calendar-->
<link rel="stylesheet" href="/js/jquery-ui-1.8.9.custom/css/south-street/jquery-ui-1.8.9.custom.css" type="text/css" media="screen" />
<script src="/js/jquery-ui-1.8.9.custom/js/jquery-ui-1.8.9.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
	(function($){
		$(function(){
			$('#date').datepicker();
			$('#saveForm').button();
			
		});
	})(jQuery);
</script>
<?php 
$js = ob_get_contents();
ob_end_clean();

ob_start();
?>
	<h1><?php echo $browser_title ?></h1>
	<p>
		If you have any questions regarding sessions, pricing, or you would like to request a booking please do not hesitate to contact us. We will respond to any questions, comments, or booking requests within two business days. We would be happy to hear from you.
	</p>
	
	<?php	if(count(@$errors) > 0) { ?>
		<div id="errors">
			<p>We are unable to submit the form for the following reasons:</p>
			<ul>
				<?php foreach( $errors as $error ) { ?>
					<li><?php echo $error ?></li>
				<?php } ?>
			</ul>
			<p>Please correct these errors and submit the form again.</p>
		</div><!-- /errors -->
	<?php } ?>
	<?php if( @$success ) { ?>
		<div id="success">
			<p>Form sent! You'll be hearing from us soon. Thank you!</p>
		</div><!-- /success -->
	<?php } ?>
	<p style="margin-bottom: 10px">Fields marked <span class="required">*</span> are required.</p>

	<form id="contact-form" name="contact-form" class="topLabel" enctype="multipart/form-data" method="post">
		<input id="human" name="human" type="text">
		<ul>
			<li>
				<label class="desc" for="name">Name <span class="required">*</span></label>
				<div>
					<input id="name" name="name" type="text" class="field text medium" value="<?php echo @$_POST['name'] ?>" size="30" maxlength="150" tabindex ="1" />
				</div>
			</li>
			
			<li>
				<label class="desc" for="email">Email Address<span class="required">*</span></label>
				<div>
					<input id="email" name="email" type="text" class="field text medium" value="<?php echo @$_POST['email'] ?>" size="30" maxlength="150" tabindex ="2" /> 
				</div>
			</li>
			
			<li>
				<label class="desc" for="phone">Phone Number</label>
				<div>
					<input id="phone" name="phone" type="text" class="field text medium" value="<?php echo @$_POST['phone'] ?>" size="30" maxlength="12" tabindex ="3" /> 
				</div>
			</li>
			
			<li>
				<label class="desc" for="date">Requested Session Date</label>
				<div>
					<input id="date" name="date" type="text" class="field text medium" value="<?php echo @$_POST['date'] ?>" size="30" maxlength="150" tabindex ="4" /> 
				</div>
			</li>
			
			<li>
				<label class="desc" for="session">Session Type</label>
				<div>
					<select id="session" class="select" name="session" tabindex="5">
						
						<?php
						$options = array('Animal Portraits',
						'Before &amp; After Baby',
						'Couples &amp; Engagement',
						'Family',
						'Graduation &amp; Special Events',
						'Kids &amp; Teens',
						'Modeling &amp; Headshots',
						'Weddings',);
						
						?>
						
						<option value="None Selected"<?php if(@$_POST['session'] == 'None Selected') { echo ' selected="selected"'; }?>>-- None Selected --</option>
						<?php foreach ($options as $value): ?>
							<option value="<?php echo $value ?>"<?php if(@$_POST['session'] == $value) echo ' selected="selected"' ?>><?php echo $value ?></option>
						<?php endforeach ?>
						<!-- <option value="New Beginnings"<?php if(@$_POST['session'] == 'New Beginnings') { echo ' selected="selected"'; }?>>New Beginnings</option>
												<option value="Growing Up"<?php if(@$_POST['session'] == 'Growing Up') { echo ' selected="selected"'; }?>>Growing Up</option>
												<option value="Expanding Horizons"<?php if(@$_POST['session'] == 'Expanding Horizons') { echo ' selected="selected"'; }?>>Expanding Horizons</option>
												<option value="Special Moments"<?php if(@$_POST['session'] == 'Special Moments') { echo ' selected="selected"'; }?>>Special Moments</option>
												<option value="Shared Lives"<?php if(@$_POST['session'] == 'Shared Lives') { echo ' selected="selected"'; }?>>Shared Lives</option>
												<option value="Fashion"<?php if(@$_POST['session'] == 'Fashion') { echo ' selected="selected"'; }?>>Fashion</option>
												<option value="Unconditional Love"<?php if(@$_POST['session'] == 'Unconditional Love') { echo ' selected="selected"'; }?>>Unconditional Love</option> -->
						
					</select>
				</div>
			</li>

			<li>
				<label class="desc" for="message">Message <span class="required">*</span></label>
				<textarea name="message" id="message" rows="12" cols="52" tabindex="6"><?php echo @$_POST['message'] ?></textarea>
			</li>
			
			<li class="buttons">
				<input id="saveForm" class="btTxt submit" type="submit" name="submit" value="Submit" />
			</li>
		</ul>
	</form>
	
<?php
$content = ob_get_contents();
ob_end_clean();


include_once('../templates/two-column.php');
?>
